----------------------------------------------------------------------------  
--  Nombre del Proyecto    : UNIPROC 2020                                       
--  Institucion            : Universidad Nacional de Ingenieria                 
--  Facultad               : FEC  
--  Carrera                : Ing. Electronica                                              
--  Pais                   : Nicaragua                                          
--  Nombre del Autor       : Jason Ortiz                                
--  Nombre del Modulo      : Registro_de_Segmentacion_IDEX
--  Descripcion del Modulo : 
--      En este modulo se encuentra el 
----------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Registro_de_Segmentacion_IDEX is
    Port ( ---------------------------------------------------------
           ---------         Puertos de Entrada            ---------
           ---------------------------------------------------------  
           CLK                  : in  STD_LOGIC;
           RST                  : in  STD_LOGIC;      
           IDEX_L_Ctr_WB        : in  STD_LOGIC_VECTOR(1  downto 0); 
           IDEX_L_Ctr_MEM       : in  STD_LOGIC_VECTOR(1  downto 0);
           IDEX_L_Ctr_EX        : in  STD_LOGIC_VECTOR(2  downto 0);    
           IDEX_L_Funct3        : in  STD_LOGIC_VECTOR(2  downto 0);
           IDEX_L_Funct7        : in  STD_LOGIC_VECTOR(6  downto 0);
           IDEX_L_Data1         : in  STD_LOGIC_VECTOR(31 downto 0);
           IDEX_L_Data2         : in  STD_LOGIC_VECTOR(31 downto 0);           
           IDEX_L_Gen_Inm       : in  STD_LOGIC_VECTOR(31 downto 0);
           IDEX_L_Reg_Rs1       : in  STD_LOGIC_VECTOR(3  downto 0);
           IDEX_L_Reg_Rs2       : in  STD_LOGIC_VECTOR(3  downto 0);
           IDEX_L_Reg_Rd        : in  STD_LOGIC_VECTOR(3  downto 0);       
           ---------------------------------------------------------
           ---------      Puertos Especiales de Entrada    ---------
           ---------          - Interfaz Grafica -         ---------
           ---------------------------------------------------------  
           IDEX_L_PC            : in  STD_LOGIC_VECTOR(6  downto 0);
           IDEX_L_NOP           : in  STD_LOGIC;
           ---------------------------------------------------------
           ---------          Puertos de Salida            ---------
           ---------------------------------------------------------  
           IDEX_H_Ctr_WB        : out STD_LOGIC_VECTOR(1  downto 0);
           IDEX_H_Ctr_MEM       : out STD_LOGIC_VECTOR(1  downto 0);
           IDEX_H_ALU_Op        : out STD_LOGIC_VECTOR(1  downto 0);           
           IDEX_H_MUX_ALU_Sel   : out STD_LOGIC;
           IDEX_H_Funct3        : out STD_LOGIC_VECTOR(2  downto 0);
           IDEX_H_Funct7        : out STD_LOGIC_VECTOR(6  downto 0);
           IDEX_H_Data1         : out STD_LOGIC_VECTOR(31 downto 0);
           IDEX_H_Data2         : out STD_LOGIC_VECTOR(31 downto 0);
           IDEX_H_Gen_Inm       : out STD_LOGIC_VECTOR(31 downto 0);
           IDEX_H_Reg_Rs1       : out STD_LOGIC_VECTOR(3  downto 0);
           IDEX_H_Reg_Rs2       : out STD_LOGIC_VECTOR(3  downto 0);
           IDEX_H_Reg_Rd        : out STD_LOGIC_VECTOR(3  downto 0);                     
           ---------------------------------------------------------
           ---------        Puerto Especial de Salida      ---------
           ---------           - Interfaz Grafica -        ---------
           ---------------------------------------------------------           
           IDEX_H_PC            : out STD_LOGIC_VECTOR(6  downto 0);
           IDEX_H_NOP           : out STD_LOGIC);
end Registro_de_Segmentacion_IDEX;

architecture Behavioral of Registro_de_Segmentacion_IDEX is

    signal IDEX_PC_Aux            : std_logic_vector(6  downto 0);
    signal IDEX_NOP_Aux           : std_logic;
    
    signal IDEX_Ctr_WB_Aux        : std_logic_vector(1  downto 0);
    signal IDEX_Ctr_MEM_Aux       : std_logic_vector(1  downto 0);
    signal IDEX_ALU_Op_Aux        : std_logic_vector(1  downto 0);
    signal IDEX_MUX_ALU_Sel_Aux   : std_logic;
    signal IDEX_Funct3_Aux        : std_logic_vector(2  downto 0);
    signal IDEX_Funct7_Aux        : std_logic_vector(6  downto 0);
    signal IDEX_Data1_Aux         : std_logic_vector(31 downto 0);
    signal IDEX_Data2_Aux         : std_logic_vector(31 downto 0);
    signal IDEX_Gen_Inm_Aux       : std_logic_vector(31 downto 0);
    signal IDEX_Reg_Rs1_Aux       : std_logic_vector(3  downto 0);
    signal IDEX_Reg_Rs2_Aux       : std_logic_vector(3  downto 0);
    signal IDEX_Reg_Rd_Aux        : std_logic_vector(3  downto 0);

begin

    process(CLK, RST)
    begin
        --Escritura de Informacion
        if RST = '1' then      
            IDEX_Ctr_WB_Aux       <= (others => '0');
            IDEX_Ctr_MEM_Aux      <= (others => '0');
            IDEX_ALU_Op_Aux       <= (others => '0');
            IDEX_MUX_ALU_Sel_Aux  <= '0';
            IDEX_Funct3_Aux       <= (others => '0');
            IDEX_Funct7_Aux       <= (others => '0');
            IDEX_Data1_Aux        <= (others => '0');
            IDEX_Data2_Aux        <= (others => '0');
            IDEX_Gen_Inm_Aux      <= (others => '0');
            IDEX_Reg_Rs1_Aux      <= (others => '0');
            IDEX_Reg_Rs2_Aux      <= (others => '0');
            IDEX_Reg_Rd_Aux       <= (others => '0');
            
            IDEX_PC_Aux           <= (others => '0');  
            IDEX_NOP_Aux          <= '0';       
        elsif (falling_edge(CLK)) then
            IDEX_Ctr_WB_Aux       <= IDEX_L_Ctr_WB;
            IDEX_Ctr_MEM_Aux      <= IDEX_L_Ctr_MEM;
            IDEX_ALU_Op_Aux       <= IDEX_L_Ctr_EX(1 downto 0);
            IDEX_MUX_ALU_Sel_Aux  <= IDEX_L_Ctr_EX(2);
            IDEX_Funct3_Aux       <= IDEX_L_Funct3;
            IDEX_Funct7_Aux       <= IDEX_L_Funct7;
            IDEX_Data1_Aux        <= IDEX_L_Data1;
            IDEX_Data2_Aux        <= IDEX_L_Data2;
            IDEX_Gen_Inm_Aux      <= IDEX_L_Gen_Inm;
            IDEX_Reg_Rs1_Aux      <= IDEX_L_Reg_Rs1;
            IDEX_Reg_Rs2_Aux      <= IDEX_L_Reg_Rs2;
            IDEX_Reg_Rd_Aux       <= IDEX_L_Reg_Rd;
            
            IDEX_PC_Aux           <= IDEX_L_PC;
            IDEX_NOP_Aux          <= IDEX_L_NOP;
        end if;       
        
        --Lectura de Informacion
        if RST = '1' then     
            IDEX_H_Ctr_WB       <= (others => '0');
            IDEX_H_Ctr_MEM      <= (others => '0');
            IDEX_H_ALU_Op       <= (others => '0');
            IDEX_H_MUX_ALU_Sel  <= '0';
            IDEX_H_Funct3       <= (others => '0');
            IDEX_H_Funct7       <= (others => '0');
            IDEX_H_Data1        <= (others => '0');
            IDEX_H_Data2        <= (others => '0');
            IDEX_H_Gen_Inm      <= (others => '0');
            IDEX_H_Reg_Rs1      <= (others => '0');
            IDEX_H_Reg_Rs2      <= (others => '0');
            IDEX_H_Reg_Rd       <= (others => '0');

            IDEX_H_PC           <= (others => '0');
            IDEX_H_NOP          <= '0';
        elsif (rising_edge(CLK)) then
            IDEX_H_Ctr_WB       <= IDEX_Ctr_WB_Aux;
            IDEX_H_Ctr_MEM      <= IDEX_Ctr_MEM_Aux;
            IDEX_H_ALU_Op       <= IDEX_ALU_Op_Aux;  
            IDEX_H_MUX_ALU_Sel  <= IDEX_MUX_ALU_Sel_Aux;
            IDEX_H_Funct3       <= IDEX_Funct3_Aux;
            IDEX_H_Funct7       <= IDEX_Funct7_Aux;
            IDEX_H_Data1        <= IDEX_Data1_Aux;
            IDEX_H_Data2        <= IDEX_Data2_Aux;
            IDEX_H_Gen_Inm      <= IDEX_Gen_Inm_Aux;
            IDEX_H_Reg_Rs1      <= IDEX_Reg_Rs1_Aux;
            IDEX_H_Reg_Rs2      <= IDEX_Reg_Rs2_Aux;
            IDEX_H_Reg_Rd       <= IDEX_Reg_Rd_Aux;
            
            IDEX_H_PC           <= IDEX_PC_Aux;  
            IDEX_H_NOP          <= IDEX_NOP_Aux;              
        end if;
    end process;

end Behavioral;
