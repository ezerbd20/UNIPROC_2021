library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Registro_de_Segmentacion_IDEX_tb is
--  Port ( );
end Registro_de_Segmentacion_IDEX_tb;

architecture Behavioral of Registro_de_Segmentacion_IDEX_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Registro_de_Segmentacion_IDEX
    Port ( ---------------------------------------------------------
           ---------         Puertos de Entrada            ---------
           ---------------------------------------------------------  
           CLK                  : in  STD_LOGIC;
           RST                  : in  STD_LOGIC;      
           IDEX_L_Ctr_WB        : in  STD_LOGIC_VECTOR(1  downto 0); 
           IDEX_L_Ctr_MEM       : in  STD_LOGIC_VECTOR(1  downto 0);
           IDEX_L_Ctr_EX        : in  STD_LOGIC_VECTOR(2  downto 0);    
           IDEX_L_Funct3        : in  STD_LOGIC_VECTOR(2  downto 0);
           IDEX_L_Funct7        : in  STD_LOGIC_VECTOR(6  downto 0);
           IDEX_L_Data1         : in  STD_LOGIC_VECTOR(31 downto 0);
           IDEX_L_Data2         : in  STD_LOGIC_VECTOR(31 downto 0);           
           IDEX_L_Gen_Inm       : in  STD_LOGIC_VECTOR(31 downto 0);
           IDEX_L_Reg_Rs1       : in  STD_LOGIC_VECTOR(3  downto 0);
           IDEX_L_Reg_Rs2       : in  STD_LOGIC_VECTOR(3  downto 0);
           IDEX_L_Reg_Rd        : in  STD_LOGIC_VECTOR(3  downto 0);       
           ---------------------------------------------------------
           ---------      Puertos Especiales de Entrada    ---------
           ---------          - Interfaz Grafica -         ---------
           ---------------------------------------------------------  
           IDEX_L_PC            : in  STD_LOGIC_VECTOR(6  downto 0);
           IDEX_L_NOP           : in  STD_LOGIC;
           ---------------------------------------------------------
           ---------          Puertos de Salida            ---------
           ---------------------------------------------------------  
           IDEX_H_Ctr_WB        : out STD_LOGIC_VECTOR(1  downto 0);
           IDEX_H_Ctr_MEM       : out STD_LOGIC_VECTOR(1  downto 0);
           IDEX_H_ALU_Op        : out STD_LOGIC_VECTOR(1  downto 0);           
           IDEX_H_MUX_ALU_Sel   : out STD_LOGIC;
           IDEX_H_Funct3        : out STD_LOGIC_VECTOR(2  downto 0);
           IDEX_H_Funct7        : out STD_LOGIC_VECTOR(6  downto 0);
           IDEX_H_Data1         : out STD_LOGIC_VECTOR(31 downto 0);
           IDEX_H_Data2         : out STD_LOGIC_VECTOR(31 downto 0);
           IDEX_H_Gen_Inm       : out STD_LOGIC_VECTOR(31 downto 0);
           IDEX_H_Reg_Rs1       : out STD_LOGIC_VECTOR(3  downto 0);
           IDEX_H_Reg_Rs2       : out STD_LOGIC_VECTOR(3  downto 0);
           IDEX_H_Reg_Rd        : out STD_LOGIC_VECTOR(3  downto 0);                     
           ---------------------------------------------------------
           ---------        Puerto Especial de Salida      ---------
           ---------           - Interfaz Grafica -        ---------
           ---------------------------------------------------------           
           IDEX_H_PC            : out STD_LOGIC_VECTOR(6  downto 0);
           IDEX_H_NOP           : out STD_LOGIC);
    END COMPONENT;

    --Inputs
    signal CLK               : std_logic:='0';
    signal RST               : std_logic:='0';
    signal IDEX_L_Ctr_WB     : std_logic_vector(1  downto 0):=(others => '0');
    signal IDEX_L_Ctr_MEM    : std_logic_vector(1  downto 0):=(others => '0');
    signal IDEX_L_Ctr_EX     : std_logic_vector(2  downto 0):=(others => '0');
    signal IDEX_L_Funct3     : std_logic_vector(2  downto 0):=(others => '0');
    signal IDEX_L_Funct7     : std_logic_vector(6  downto 0):=(others => '0');
    signal IDEX_L_Data1      : std_logic_vector(31 downto 0):=(others => '0');
    signal IDEX_L_Data2      : std_logic_vector(31 downto 0):=(others => '0');
    signal IDEX_L_Gen_Inm    : std_logic_vector(31 downto 0):=(others => '0');
    signal IDEX_L_Reg_Rs1    : std_logic_vector(3  downto 0):=(others => '0');
    signal IDEX_L_Reg_Rs2    : std_logic_vector(3  downto 0):=(others => '0');
    signal IDEX_L_Reg_Rd     : std_logic_vector(3  downto 0):=(others => '0');
    signal IDEX_L_PC         : std_logic_vector(6  downto 0):=(others => '0');
    signal IDEX_L_NOP        : std_logic:='0';

    --Outputs
    signal IDEX_H_Ctr_WB      : std_logic_vector(1  downto 0);
    signal IDEX_H_Ctr_MEM     : std_logic_vector(1  downto 0);
    signal IDEX_H_ALU_Op      : std_logic_vector(1  downto 0);
    signal IDEX_H_MUX_ALU_Sel : std_logic;
    signal IDEX_H_Funct3      : std_logic_vector(2  downto 0);
    signal IDEX_H_Funct7      : std_logic_vector(6  downto 0);
    signal IDEX_H_Data1       : std_logic_vector(31 downto 0);
    signal IDEX_H_Data2       : std_logic_vector(31 downto 0);
    signal IDEX_H_Gen_Inm     : std_logic_vector(31 downto 0);
    signal IDEX_H_Reg_Rs1     : std_logic_vector(3  downto 0);
    signal IDEX_H_Reg_Rs2     : std_logic_vector(3  downto 0);
    signal IDEX_H_Reg_Rd      : std_logic_vector(3  downto 0);
    signal IDEX_H_PC          : std_logic_vector(6  downto 0);
    signal IDEX_H_NOP         : std_logic;
    
     --Clock period definitions
    constant clk_period : time := 10 ns;
    
begin

    --Instantiate the Unit Under Test (UUT)
    uut : Registro_de_Segmentacion_IDEX PORT MAP (
        CLK                 => CLK,
        RST                 => RST,
        IDEX_L_Ctr_WB       => IDEX_L_Ctr_WB,
        IDEX_L_Ctr_MEM      => IDEX_L_Ctr_MEM,
        IDEX_L_Ctr_EX       => IDEX_L_Ctr_EX,
        IDEX_L_Funct3       => IDEX_L_Funct3,
        IDEX_L_Funct7       => IDEX_L_Funct7,
        IDEX_L_Data1        => IDEX_L_Data1,
        IDEX_L_Data2        => IDEX_L_Data2,
        IDEX_L_Gen_Inm      => IDEX_L_Gen_Inm,
        IDEX_L_Reg_Rs1      => IDEX_L_Reg_Rs1,
        IDEX_L_Reg_Rs2      => IDEX_L_Reg_Rs2,
        IDEX_L_Reg_Rd       => IDEX_L_Reg_Rd,
        IDEX_L_PC           => IDEX_L_PC,
        IDEX_L_NOP          => IDEX_L_NOP,
        IDEX_H_Ctr_WB       => IDEX_H_Ctr_WB,
        IDEX_H_Ctr_MEM      => IDEX_H_Ctr_MEM,
        IDEX_H_ALU_Op       => IDEX_H_ALU_Op,
        IDEX_H_MUX_ALU_Sel  => IDEX_H_MUX_ALU_Sel,
        IDEX_H_Funct3       => IDEX_H_Funct3,
        IDEX_H_Funct7       => IDEX_H_Funct7,
        IDEX_H_Data1        => IDEX_H_Data1,
        IDEX_H_Data2        => IDEX_H_Data2,
        IDEX_H_Gen_Inm      => IDEX_H_Gen_Inm,
        IDEX_H_Reg_Rs1      => IDEX_H_Reg_Rs1,
        IDEX_H_Reg_Rs2      => IDEX_H_Reg_Rs2,
        IDEX_H_Reg_Rd       => IDEX_H_Reg_Rd,
        IDEX_H_PC           => IDEX_H_PC,        
        IDEX_H_NOP          => IDEX_H_NOP
    );
    
    --Clock process definitions
    clk_process : process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;
    
    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        RST             <= '0';
        IDEX_L_Ctr_WB   <= "10";
        IDEX_L_Ctr_MEM  <= "01";
        IDEX_L_Ctr_EX   <= "111";
        IDEX_L_Funct3   <= "010";
        IDEX_L_Funct7   <= "0100000";
        IDEX_L_Data1    <= x"FFFFFFFF";
        IDEX_L_Data2    <= x"CCCCCCCC";
        IDEX_L_Gen_Inm  <= x"AAAAAAAA";
        IDEX_L_Reg_Rs1  <= x"4";
        IDEX_L_Reg_Rs2  <= x"5";
        IDEX_L_Reg_Rd   <= x"7";
        IDEX_L_PC       <= "0100000";
        IDEX_L_NOP      <= '0';
        wait for 10 ns;
        RST             <= '0';
        IDEX_L_Ctr_WB   <= "11";
        IDEX_L_Ctr_MEM  <= "00";
        IDEX_L_Ctr_EX   <= "101";
        IDEX_L_Funct3   <= "110";
        IDEX_L_Funct7   <= "0100011";
        IDEX_L_Data1    <= x"11111111";
        IDEX_L_Data2    <= x"FFFF0000";
        IDEX_L_Gen_Inm  <= x"EEEEAAAA";
        IDEX_L_Reg_Rs1  <= x"9";
        IDEX_L_Reg_Rs2  <= x"B";
        IDEX_L_Reg_Rd   <= x"C";
        IDEX_L_PC       <= "0010100";
        IDEX_L_NOP      <= '0';
        wait for 10 ns;
        RST             <= '0';
        IDEX_L_Ctr_WB   <= "00";
        IDEX_L_Ctr_MEM  <= "01";
        IDEX_L_Ctr_EX   <= "001";
        IDEX_L_Funct3   <= "011";
        IDEX_L_Funct7   <= "0101111";
        IDEX_L_Data1    <= x"00000000";
        IDEX_L_Data2    <= x"33333333";
        IDEX_L_Gen_Inm  <= x"BBBBBBBB";
        IDEX_L_Reg_Rs1  <= x"D";
        IDEX_L_Reg_Rs2  <= x"8";
        IDEX_L_Reg_Rd   <= x"F";
        IDEX_L_PC       <= "0000100";
        IDEX_L_NOP      <= '0';
        wait for 10 ns;
        RST             <= '0';
        IDEX_L_Ctr_WB   <= "10";
        IDEX_L_Ctr_MEM  <= "11";
        IDEX_L_Ctr_EX   <= "000";
        IDEX_L_Funct3   <= "000";
        IDEX_L_Funct7   <= "0100010";
        IDEX_L_Data1    <= x"55555555";
        IDEX_L_Data2    <= x"22222222";
        IDEX_L_Gen_Inm  <= x"66666666";
        IDEX_L_Reg_Rs1  <= x"0";
        IDEX_L_Reg_Rs2  <= x"2";
        IDEX_L_Reg_Rd   <= x"3";
        IDEX_L_PC       <= "0000000";
        IDEX_L_NOP      <= '0';
        wait for 10 ns;        
        RST             <= '1';
        IDEX_L_Ctr_WB   <= "11";
        IDEX_L_Ctr_MEM  <= "10";
        IDEX_L_Ctr_EX   <= "110";
        IDEX_L_Funct3   <= "010";
        IDEX_L_Funct7   <= "0100111";
        IDEX_L_Data1    <= x"55557777";
        IDEX_L_Data2    <= x"33332222";
        IDEX_L_Gen_Inm  <= x"61616161";
        IDEX_L_Reg_Rs1  <= x"A";
        IDEX_L_Reg_Rs2  <= x"F";
        IDEX_L_Reg_Rd   <= x"D";
        IDEX_L_PC       <= "0000100";
        IDEX_L_NOP      <= '1';
        wait for 10 ns;
        RST             <= '0';
        IDEX_L_Ctr_WB   <= "00";
        IDEX_L_Ctr_MEM  <= "01";
        IDEX_L_Ctr_EX   <= "010";
        IDEX_L_Funct3   <= "110";
        IDEX_L_Funct7   <= "1100000";
        IDEX_L_Data1    <= x"ABCDEFAB";
        IDEX_L_Data2    <= x"50505050";
        IDEX_L_Gen_Inm  <= x"60606060";
        IDEX_L_Reg_Rs1  <= x"2";
        IDEX_L_Reg_Rs2  <= x"3";
        IDEX_L_Reg_Rd   <= x"6";
        IDEX_L_PC       <= "0010000";
        IDEX_L_NOP      <= '0';
        wait for 10 ns;
        RST             <= '0';
        IDEX_L_Ctr_WB   <= "10";
        IDEX_L_Ctr_MEM  <= "11";
        IDEX_L_Ctr_EX   <= "110";
        IDEX_L_Funct3   <= "010";
        IDEX_L_Funct7   <= "0100000";
        IDEX_L_Data1    <= x"0BCDEFAB";
        IDEX_L_Data2    <= x"10505050";
        IDEX_L_Gen_Inm  <= x"10606060";
        IDEX_L_Reg_Rs1  <= x"0";
        IDEX_L_Reg_Rs2  <= x"1";
        IDEX_L_Reg_Rd   <= x"0";
        IDEX_L_PC       <= "0010100";
        IDEX_L_NOP      <= '1';
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
