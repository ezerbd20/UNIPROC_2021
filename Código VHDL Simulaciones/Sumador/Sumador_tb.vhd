library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Sumador_tb is
--  Port ( );
end Sumador_tb;

architecture Behavioral of Sumador_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Sumador
    Port ( -------------------------------------------------
           --------       Puertos de Entrada        --------
           --------           del Modulo            --------
           -------------------------------------------------
           In_A  : in  STD_LOGIC_VECTOR (6 downto 0);
           In_B  : in  STD_LOGIC_VECTOR (5 downto 0);
           -------------------------------------------------
           --------         Puerto de Salida        --------
           --------            del Modulo           --------
           -------------------------------------------------
           Out_C : out STD_LOGIC_VECTOR (6 downto 0));    
    END COMPONENT;
    
    --Inputs
    signal In_A  : std_logic_vector(6 downto 0):= (others => '0');
    signal In_B  : std_logic_vector(5 downto 0):= (others => '0');
    
    --Outputs
    signal Out_C : std_logic_vector(6 downto 0);
    
begin

    --Instantiate the Unit Under Test (UUT)
    uut : Sumador PORT MAP (
        In_A    => In_A,
        In_B    => In_B,
        Out_C   => Out_C
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        In_A    <= "1000011";
        In_B    <= "000100";
        wait for 10 ns;        
        In_A    <= "1010000";
        In_B    <= "000101";
        wait for 10 ns;    
        In_A    <= "0001111";
        In_B    <= "000111";
        wait for 10 ns;    
        In_A    <= "1001100";
        In_B    <= "000001"; 
        wait for 10 ns;    
        In_A    <= "0001011";
        In_B    <= "000011";
        wait for 10 ns;    
        In_A    <= "0001001";
        In_B    <= "000010";
        wait for 10 ns;          
        In_A    <= "1000011";
        In_B    <= "000100";
        wait for 10 ns;   
    wait;
    end process;

end Behavioral;
