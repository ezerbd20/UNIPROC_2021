library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Unidad_de_Deteccion_de_Riesgos_tb is
--  Port ( );
end Unidad_de_Deteccion_de_Riesgos_tb;

architecture Behavioral of Unidad_de_Deteccion_de_Riesgos_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Unidad_de_Deteccion_de_Riesgos
  Port ( --------------------------------------------------
         ---------       Puertos de Entrada       ---------
         --------------------------------------------------
         IDEX_Ctr_MEM_R : in  STD_LOGIC;
         ID_Ctr_MEM_W   : in  STD_LOGIC;
         IFID_Reg_Rs1   : in  STD_LOGIC_VECTOR(3 downto 0);
         IFID_Reg_Rs2   : in  STD_LOGIC_VECTOR(3 downto 0);
         IDEX_Reg_Rd    : in  STD_LOGIC_VECTOR(3 downto 0);
         --------------------------------------------------
         ---------       Puertos de Salida        ---------
         --------------------------------------------------
         MUX_Ctr_Sel    : out STD_LOGIC;
         PC_E           : out STD_LOGIC;
         IFID_E         : out STD_LOGIC
   );
    END COMPONENT;

    --Inputs
    signal IDEX_Ctr_MEM_R   : std_logic:= '0';
    signal ID_Ctr_MEM_W     : std_logic:= '0';
    signal IFID_Reg_Rs1     : std_logic_vector(3 downto 0):=(others => '0');
    signal IFID_Reg_Rs2     : std_logic_vector(3 downto 0):=(others => '0');
    signal IDEX_Reg_Rd      : std_logic_vector(3 downto 0):=(others => '0');
    
    --Outputs   
    signal MUX_Ctr_Sel      : std_logic;
    signal PC_E             : std_logic;
    signal IFID_E           : std_logic;

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Unidad_de_Deteccion_de_Riesgos PORT MAP (
        IDEX_Ctr_MEM_R  => IDEX_Ctr_MEM_R,
        ID_Ctr_MEM_W    => ID_Ctr_MEM_W,
        IFID_Reg_Rs1    => IFID_Reg_Rs1,
        IFID_Reg_Rs2    => IFID_Reg_Rs2,
        IDEX_Reg_Rd     => IDEX_Reg_Rd,
        MUX_Ctr_Sel     => MUX_Ctr_Sel,
        PC_E            => PC_E,
        IFID_E          => IFID_E
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        IDEX_Ctr_MEM_R  <= '0';
        ID_Ctr_MEM_W    <= '0';
        IFID_Reg_Rs1    <= x"4";
        IFID_Reg_Rs2    <= x"8";
        IDEX_Reg_Rd     <= x"7";
        wait for 10 ns;
        IDEX_Ctr_MEM_R  <= '1';
        ID_Ctr_MEM_W    <= '0';
        IFID_Reg_Rs1    <= x"B";
        IFID_Reg_Rs2    <= x"9";
        IDEX_Reg_Rd     <= x"B";
        wait for 10 ns;
        IDEX_Ctr_MEM_R  <= '1';
        ID_Ctr_MEM_W    <= '0';
        IFID_Reg_Rs1    <= x"3";
        IFID_Reg_Rs2    <= x"D";
        IDEX_Reg_Rd     <= x"D";
        wait for 10 ns;
        IDEX_Ctr_MEM_R  <= '1';
        ID_Ctr_MEM_W    <= '1';
        IFID_Reg_Rs1    <= x"A";
        IFID_Reg_Rs2    <= x"E";
        IDEX_Reg_Rd     <= x"A";
        wait for 10 ns;
        IDEX_Ctr_MEM_R  <= '1';
        ID_Ctr_MEM_W    <= '0';
        IFID_Reg_Rs1    <= x"A";
        IFID_Reg_Rs2    <= x"E";
        IDEX_Reg_Rd     <= x"A";
        wait for 10 ns;
        IDEX_Ctr_MEM_R  <= '0';
        ID_Ctr_MEM_W    <= '0';
        IFID_Reg_Rs1    <= x"A";
        IFID_Reg_Rs2    <= x"E";
        IDEX_Reg_Rd     <= x"A";
        wait for 10 ns;
        IDEX_Ctr_MEM_R  <= '1';
        ID_Ctr_MEM_W    <= '0';
        IFID_Reg_Rs1    <= x"F";
        IFID_Reg_Rs2    <= x"B";
        IDEX_Reg_Rd     <= x"F";
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
