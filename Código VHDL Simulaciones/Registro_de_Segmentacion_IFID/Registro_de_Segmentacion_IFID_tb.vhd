library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Registro_de_Segmentacion_IFID_tb is
--  Port ( );
end Registro_de_Segmentacion_IFID_tb;

architecture Behavioral of Registro_de_Segmentacion_IFID_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Registro_de_Segmentacion_IFID
    Port ( -----------------------------------------------
           -------       Puertos de Entrada        -------
           -----------------------------------------------
           CLK          : in  STD_LOGIC;
           RST          : in  STD_LOGIC;
           IFID_E       : in  STD_LOGIC;
           IFID_L_Instr : in  STD_LOGIC_VECTOR(31 downto 0);
           IFID_L_PC    : in  STD_LOGIC_VECTOR(6  downto 0);
           Branch_Taken : in  STD_LOGIC;           
           -----------------------------------------------
           ------     Puerto Especial de Entrada    ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           IFID_L_NOP   : in  STD_LOGIC;
           -----------------------------------------------
           -------         Puerto de Salida        -------
           -----------------------------------------------
           IFID_H_Instr : out STD_LOGIC_VECTOR(31 downto 0);
           IFID_H_PC    : out STD_LOGIC_VECTOR(6  downto 0);
           -----------------------------------------------
           ------     Puerto Especial de Salida     ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           IFID_H_NOP   : out STD_LOGIC);    
    END COMPONENT;

    --Inputs
    signal CLK          : std_logic:= '0';
    signal RST          : std_logic:= '0';
    signal IFID_E       : std_logic:= '0';
    signal IFID_L_Instr : std_logic_vector(31 downto 0):= (others => '0');
    signal IFID_L_PC    : std_logic_vector(6  downto 0):= (others => '0');
    signal Branch_Taken : std_logic:= '0';
    signal IFID_L_NOP   : std_logic:= '0';
    
    --Outputs
    signal IFID_H_Instr : std_logic_vector(31 downto 0);
    signal IFID_H_PC    : std_logic_vector(6  downto 0);
    signal IFID_H_NOP   : std_logic;

    --Clock period definitions
    constant clk_period : time := 10 ns;

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Registro_de_Segmentacion_IFID PORT MAP (
        CLK          => CLK,
        RST          => RST,
        IFID_E       => IFID_E,
        IFID_L_Instr => IFID_L_Instr,
        IFID_L_PC    => IFID_L_PC,
        Branch_Taken => Branch_Taken,
        IFID_L_NOP   => IFID_L_NOP,
        IFID_H_Instr => IFID_H_Instr,
        IFID_H_PC    => IFID_H_PC,
        IFID_H_NOP   => IFID_H_NOP
    );

    --Clock process definitions
    clk_process : process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        RST          <= '0';
        IFID_E       <= '0';
        IFID_L_Instr <= "01000000010100111000010010110011";
        IFID_L_PC    <= "0000100";
        Branch_Taken <= '0';
        IFID_L_NOP   <= '0';
        wait for 10 ns;
        RST          <= '0';
        IFID_E       <= '1';
        IFID_L_Instr <= "00000000000100000000110001100011";
        IFID_L_PC    <= "0001000";
        Branch_Taken <= '0';
        IFID_L_NOP   <= '0';
        wait for 10 ns;
        RST          <= '0';
        IFID_E       <= '0';
        IFID_L_Instr <= "00000000001100110110010110110011";
        IFID_L_PC    <= "0010000";
        Branch_Taken <= '1';
        IFID_L_NOP   <= '0';
        wait for 10 ns;
        RST          <= '0';
        IFID_E       <= '0';
        IFID_L_Instr <= "11111110000000000000100011100011";
        IFID_L_PC    <= "0100000";
        Branch_Taken <= '0';
        IFID_L_NOP   <= '0';
        wait for 10 ns;
        RST          <= '1';
        IFID_E       <= '0';
        IFID_L_Instr <= "00000000000100000000110001100011";
        IFID_L_PC    <= "0011000";
        Branch_Taken <= '0';
        IFID_L_NOP   <= '0';
        wait for 10 ns;
        RST          <= '0';
        IFID_E       <= '0';
        IFID_L_Instr <= "01000000010100111000010010110011";
        IFID_L_PC    <= "0000100";
        Branch_Taken <= '0';
        IFID_L_NOP   <= '0';
        wait for 10 ns;
        RST          <= '0';
        IFID_E       <= '0';
        IFID_L_Instr <= "00000000001100110110010110110011";
        IFID_L_PC    <= "0001000";
        Branch_Taken <= '0';
        IFID_L_NOP   <= '0';
        wait for 10 ns;

    wait;
    end process;


end Behavioral;
