-------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2020
--  Institucion            : Universidad Nacional de Ingenieria
--  Facultad               : FEC 
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Unidad_Aritmetico_Logica
--  Descripcion del Modulo : 
--      En este modulo se encuentra la Unidad "Aritmetico-Logica" (ALU),
--      su trabajo consiste en realizar operaciones aritmeticas y
--      logicas sobre los datos provenientes de los puertos de entrada,
--      y entregar el resultado de la operacion en el puerto de salida. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Unidad_Aritmetico_Logica is
    Port ( --------------------------------------------
           ------       Puertos de Entrada       ------
           --------------------------------------------
           In_A    : in  STD_LOGIC_VECTOR(31 downto 0);
           In_B    : in  STD_LOGIC_VECTOR(31 downto 0);
           ALU_Ctr : in  STD_LOGIC_VECTOR(3  downto 0);
           --------------------------------------------
           ------        Puerto de Salida        ------
           --------------------------------------------
           ALU_Out : out STD_LOGIC_VECTOR(31 downto 0));
end Unidad_Aritmetico_Logica;

architecture Behavioral of Unidad_Aritmetico_Logica is

begin

    process (In_A, In_B, ALU_Ctr)
    begin
        case ALU_Ctr is            
            --Operacion Aritmetica de Suma
            when x"0" =>              
                ALU_Out <= In_A + In_B;            
            
            --Operacion Aritmetica de Resta
            when x"1" =>
                ALU_Out <= In_A - In_B;    
            
            --Operacion Logica AND
            when x"2" =>
                ALU_Out <= In_A AND In_B;    
            
            --Operacion Logica OR
            when x"3" =>
                ALU_Out <= In_A OR In_B;
            
            --Operacion Logica XOR
            when x"4" =>
                ALU_Out <= In_A XOR In_B;    
            
            --Operacion Logica de Desplazamiento Logico hacia la izquierda
            when x"5" =>
                ALU_Out <= std_logic_vector(shift_left(unsigned(In_A),to_integer(unsigned(In_B(4 downto 0)))));
                 
            --Operacion Logica de Desplazamiento Logico hacia la derecha
            when x"6" =>
                ALU_Out <= std_logic_vector(shift_right(unsigned(In_A),to_integer(unsigned(In_B(4 downto 0))))); 

            --Operacion Logica de Desplazamiento Aritmetico hacia la derecha
            when x"7" =>
                ALU_Out <= std_logic_vector(shift_right(signed(In_A),to_integer(unsigned(In_B(4 downto 0))))); 
            
            --Operacion Logica de Comparacion con signo: In_A < In_B
            when x"8" =>
                if signed(In_A) < signed(In_B) then
                    ALU_Out <= x"00000001";
                else 
                    ALU_Out <= (others => '0');
                end if;

            --Operacion Logica de Comparacion sin signo: In_A < In_B
            when x"9" =>
                if unsigned(In_A) < unsigned(In_B) then
                    ALU_Out <= x"00000001";
                else 
                    ALU_Out <= (others => '0');
                end if;
                
            --Casos no Estimados
            when others => 
                ALU_Out <= (others => '0');          
        end case;
        
    end process;

end Behavioral;

