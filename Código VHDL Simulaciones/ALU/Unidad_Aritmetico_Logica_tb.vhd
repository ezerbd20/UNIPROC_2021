library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Unidad_Aritmetico_Logica_tb is
--  Port ( );
end Unidad_Aritmetico_Logica_tb;

architecture Behavioral of Unidad_Aritmetico_Logica_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Unidad_Aritmetico_Logica
    Port ( --------------------------------------------
           ------       Puertos de Entrada       ------
           --------------------------------------------
           In_A    : in  STD_LOGIC_VECTOR(31 downto 0);
           In_B    : in  STD_LOGIC_VECTOR(31 downto 0);
           ALU_Ctr : in  STD_LOGIC_VECTOR(3  downto 0);
           --------------------------------------------
           ------        Puerto de Salida        ------
           --------------------------------------------
           ALU_Out : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT;
    
    --Inputs
    signal In_A     : std_logic_vector(31 downto 0):= (others => '0');
    signal In_B     : std_logic_vector(31 downto 0):= (others => '0');
    signal ALU_Ctr  : std_logic_vector(3  downto 0):= (others => '0');

    --Outputs
    signal ALU_Out  : std_logic_vector(31 downto 0);
    
begin

    --Instantiate the Unit Under Test (UUT)
    uut : Unidad_Aritmetico_Logica PORT MAP (
        In_A    => In_A,
        In_B    => In_B,
        ALU_Ctr => ALU_Ctr,
        ALU_Out => ALU_Out
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        In_A    <= x"55555555";
        In_B    <= x"55555555";
        ALU_Ctr <= x"0";
        wait for 10 ns;
        In_A    <= x"AAAAAAAA";
        In_B    <= x"55555555";
        ALU_Ctr <= x"3";
        wait for 10 ns;
        In_A    <= x"0000000E";
        In_B    <= x"0000000F";
        ALU_Ctr <= x"8";
        wait for 10 ns;
        In_A    <= x"FFFFFFFF";
        In_B    <= x"55555555";
        ALU_Ctr <= x"F";
        wait for 10 ns;
        In_A    <= x"00000006";
        In_B    <= x"00000002";
        ALU_Ctr <= x"1";
        wait for 10 ns;
        In_A    <= x"55555555";
        In_B    <= x"AAAAAAAA";
        ALU_Ctr <= x"2";
        wait for 10 ns;
        In_A    <= x"00000002";
        In_B    <= x"00000007";
        ALU_Ctr <= x"9";
        wait for 10 ns;        
    wait;
    end process;
    
end Behavioral;
