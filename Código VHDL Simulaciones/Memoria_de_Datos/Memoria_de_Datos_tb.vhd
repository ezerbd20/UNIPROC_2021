library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Memoria_de_Datos_tb is
--  Port ( );
end Memoria_de_Datos_tb;

architecture Behavioral of Memoria_de_Datos_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Memoria_de_Datos
    Port ( ----------------------------------------------
           ------       Puertos de Entrada         ------
           ------           - Modulo -             ------
           ----------------------------------------------
           CLK       : in  STD_LOGIC;
           RST       : in  STD_LOGIC; 
           Mem_Write : in  STD_LOGIC;
           Mem_Read  : in  STD_LOGIC;
           Address   : in  STD_LOGIC_VECTOR(5  downto 0);
           Data_In   : in  STD_LOGIC_VECTOR(31 downto 0);
           ----------------------------------------------
           ------    Puerto Especial de Entrada    ------
           ------       - Interfaz Grafica -       ------
           ----------------------------------------------
           Count_V   : in  integer;
           ----------------------------------------------
           ------         Puerto de Salida         ------
           ------            - Modulo -            ------
           ----------------------------------------------
           Data_Out  : out STD_LOGIC_VECTOR(31 downto 0);
           ----------------------------------------------
           ------    Puerto Especial de Salida     ------
           ------       - Interfaz Grafica -       ------
           ----------------------------------------------
           MemD_Out  : out STD_LOGIC_VECTOR(31 downto 0)
           );    
    END COMPONENT;    

    --Inputs
    signal CLK       : std_logic:='0';
    signal RST       : std_logic:='0';
    signal Mem_Write : std_logic:='0';
    signal Mem_Read  : std_logic:='0';
    signal Address   : std_logic_vector(5  downto 0):=(others => '0');
    signal Data_In   : std_logic_vector(31 downto 0):=(others => '0');
    signal Count_V   : integer:=7;

    --Outputs
    signal Data_Out  : std_logic_vector(31 downto 0);
    signal MemD_Out  : std_logic_vector(31 downto 0);

    --Clock period definitions
    constant clk_period : time := 10 ns;

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Memoria_de_Datos PORT MAP (
        CLK       => CLK,
        RST       => RST,
        Mem_Write => Mem_Write,
        Mem_Read  => Mem_Read,
        Address   => Address,
        Data_In   => Data_In,
        Count_V   => Count_V,
        Data_Out  => Data_Out,
        MemD_Out  => MemD_Out
    );

    --Clock process definitions
    clk_process : process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        RST       <= '0';
        Mem_Write <= '1';
        Mem_Read  <= '0';
        Address   <= "000100";
        Data_In   <= x"FFFFFFFF";
        Count_V   <= 3;
        wait for 10 ns;
        RST       <= '0';
        Mem_Write <= '0';
        Mem_Read  <= '1';
        Address   <= "000000";
        Data_In   <= x"EEEEEEEE";
        Count_V   <= 9;
        wait for 10 ns;
        RST       <= '0';
        Mem_Write <= '1';
        Mem_Read  <= '0';
        Address   <= "011000";
        Data_In   <= x"55555555";
        Count_V   <= 9;
        wait for 10 ns;
        RST       <= '0';
        Mem_Write <= '0';
        Mem_Read  <= '1';
        Address   <= "000000";
        Data_In   <= x"11111111";
        Count_V   <= 8;
        wait for 10 ns;
        RST       <= '0';
        Mem_Write <= '1';
        Mem_Read  <= '1';
        Address   <= "000000";
        Data_In   <= x"DDDDDDDD";
        Count_V   <= 7;
        wait for 10 ns;
        RST       <= '0';
        Mem_Write <= '0';
        Mem_Read  <= '1';
        Address   <= "000000";
        Data_In   <= x"EEEEEEEE";
        Count_V   <= 4;
        wait for 10 ns;
        RST       <= '0';
        Mem_Write <= '0';
        Mem_Read  <= '1';
        Address   <= "100000";
        Data_In   <= x"EEEEEEEF";
        Count_V   <= 6;
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
