---------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2020
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Desplazador_de_1bit_hacia_la_Izquierda
--  Descripcion del Modulo : 
--      En este modulo se encuentra el Desplazador de 1 Bit hacia la 
--      Izquierda, su trabajo consiste en realizar un desplazamiento
--      logico de 1 bit hacia la izquierda, sobre el valor inmediato
--      de 32 bits, proveniente del modulo generador de inmediatos
---------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Desplazador_de_1bit_hacia_la_Izquierda is
    Port ( Inp  : in  STD_LOGIC_VECTOR (31 downto 0);
           Outp : out STD_LOGIC_VECTOR (31 downto 0));
end Desplazador_de_1bit_hacia_la_Izquierda;

architecture Behavioral of Desplazador_de_1bit_hacia_la_Izquierda is

begin

    --Desplazamiento Logico de 1 Bit hacia la Izquierda
    Outp <= Inp(30 downto 0) & '0';

end Behavioral;
