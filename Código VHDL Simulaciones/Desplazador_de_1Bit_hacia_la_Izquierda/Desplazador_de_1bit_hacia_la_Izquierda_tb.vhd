library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Desplazador_de_1bit_hacia_la_Izquierda_tb is
--  Port ( );
end Desplazador_de_1bit_hacia_la_Izquierda_tb;

architecture Behavioral of Desplazador_de_1bit_hacia_la_Izquierda_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Desplazador_de_1bit_hacia_la_Izquierda
    Port ( Inp  : in  STD_LOGIC_VECTOR (31 downto 0);
           Outp : out STD_LOGIC_VECTOR (31 downto 0));
    END COMPONENT;

    --Inputs
    signal Inp  : std_logic_vector(31 downto 0);

    --Outputs
    signal Outp : std_logic_vector(31 downto 0);

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Desplazador_de_1bit_hacia_la_Izquierda PORT MAP (
        Inp     => Inp,
        Outp    => Outp
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        Inp <= x"7FFFFFFF";
        wait for 10 ns;
        Inp <= x"FFFFFFFE";
        wait for 10 ns;
        Inp <= x"FFFFFFFF";
        wait for 10 ns;
        Inp <= x"70000000";
        wait for 10 ns;
        Inp <= x"00000001";
        wait for 10 ns;
        Inp <= x"80000000";
        wait for 10 ns;
        Inp <= x"40000000";
        wait for 10 ns;
        
    wait;
    end process;

end Behavioral;
