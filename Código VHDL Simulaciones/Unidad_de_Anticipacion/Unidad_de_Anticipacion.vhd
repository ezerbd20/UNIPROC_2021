--------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2020
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Unidad_de_Anticipacion
--  Descripcion del Modulo : 
--      Modulo encargado de controlar los multiplexores, que implementan  
--      la tecnica "Anticipacion" en la etapa "EX" y la etapa "MEM", cuando 
--      es detectado un "Data Hazard" en alguna de las dos etapas antes 
--      mencionadas
--------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Unidad_de_Anticipacion is
  Port ( --------------------------------------------------------
         --------           Puertos de Entrada           --------
         --------------------------------------------------------
         IDEX_Reg_Rs1       : in  STD_LOGIC_VECTOR(3 downto 0);
         IDEX_Reg_Rs2       : in  STD_LOGIC_VECTOR(3 downto 0);         
         EXMEM_Ctr_WB_W     : in  STD_LOGIC;
         MEMWB_Ctr_WB_W     : in  STD_LOGIC;
         EXMEM_Ctr_MEM_R    : in  STD_LOGIC;
         IDEX_Ctr_MEM_W     : in  STD_LOGIC;
         EXMEM_Reg_Rd       : in  STD_LOGIC_VECTOR(3 downto 0);
         MEMWB_Reg_Rd       : in  STD_LOGIC_VECTOR(3 downto 0);
         --------------------------------------------------------
         --------           Puertos de Salida            --------
         --------------------------------------------------------
         MUX_Anti_ALU_A_Sel : out STD_LOGIC_VECTOR(1 downto 0);
         MUX_Anti_ALU_B_Sel : out STD_LOGIC_VECTOR(1 downto 0);
         MUX_Anti_MemD_Sel  : out STD_LOGIC
  );
end Unidad_de_Anticipacion;

architecture Behavioral of Unidad_de_Anticipacion is

begin
    
    --Deteccion de "Data Hazard" en la etapa de Ejecucion (EX), vinculado al "Registro Fuente 1 (Rs1)"
    EX_MUX_Rs1 : process(EXMEM_Ctr_WB_W, MEMWB_Ctr_WB_W, EXMEM_Reg_Rd, EXMEM_Ctr_MEM_R, MEMWB_Reg_Rd, IDEX_Reg_Rs1)
    begin
        --"Grupo 1A"
        if (EXMEM_Ctr_WB_W = '1' and EXMEM_Ctr_MEM_R = '0' and EXMEM_Reg_Rd /= x"0" and EXMEM_Reg_Rd = IDEX_Reg_Rs1) then
            --El valor de "Registro Fuente 1", procedente del Registro de Pipeline "IDEX", es ignorado
            --en favor del resultado de la ALU almacenado en el Registro de Pipeline "EXMEM"
            MUX_Anti_ALU_A_Sel    <= "10";
        
        --"Grupo 2A"
        elsif (MEMWB_Ctr_WB_W = '1' and MEMWB_Reg_Rd /= x"0" and MEMWB_Reg_Rd = IDEX_Reg_Rs1) then
            --El valor de "Registro Fuente 1", procedente del Registro de Pipeline "IDEX", es ignorado
            --en favor del valor que sera escrito en el banco de registros, proporcionado por el MUX de la etapa "WB"
            MUX_Anti_ALU_A_Sel    <= "01";
        
        else
            --NO Harzard
            MUX_Anti_ALU_A_Sel    <= "00";
        end if;
    end process;
    
    --Deteccion de "Data Hazard" en la etapa de Ejecucion (EX), vinculado al "Registro Fuente 2 (Rs2)"
    EX_MUX_Rs2 : process(EXMEM_Ctr_WB_W, MEMWB_Ctr_WB_W, EXMEM_Reg_Rd, MEMWB_Reg_Rd, IDEX_Reg_Rs2, EXMEM_Ctr_MEM_R)
    begin
        --"Grupo 1B"
        if (EXMEM_Ctr_WB_W = '1' and EXMEM_Ctr_MEM_R = '0' and EXMEM_Reg_Rd /= x"0" and EXMEM_Reg_Rd = IDEX_Reg_Rs2) then
            --El valor de "Registro Fuente 2", procedente del Registro de Pipeline "IDEX", es ignorado
            --en favor del resultado de la ALU almacenado en el Registro de Pipeline "EXMEM"
            MUX_Anti_ALU_B_Sel    <= "10";
        
        --"Grupo 2B"
        elsif (MEMWB_Ctr_WB_W = '1' and MEMWB_Reg_Rd /= x"0" and MEMWB_Reg_Rd = IDEX_Reg_Rs2) then
            --El valor de "Registro Fuente 2", procedente del Registro de Pipeline "IDEX", es ignorado
            --en favor del valor que sera escrito en el banco de registros, proporcionado por el MUX de la etapa "WB"
            MUX_Anti_ALU_B_Sel    <= "01";
        
        else
            --NO Harzard
            MUX_Anti_ALU_B_Sel    <= "00";
        end if;
    end process;

    --Deteccion de "Data Hazard" en la etapa de Memoria (MEM), vinculado al "Registro Fuente 2 (Rs2)"
    MEM_DMem_Data_in_Sel : process(EXMEM_Ctr_MEM_R, IDEX_Ctr_MEM_W, EXMEM_Reg_Rd, IDEX_Reg_Rs2)
    begin
        --Copia memoria-a-memoria
        if (EXMEM_Ctr_MEM_R = '1' and IDEX_Ctr_MEM_W = '1' and EXMEM_Reg_Rd = IDEX_Reg_Rs2) then
            --El valor de "Registro Fuente 2", procedente del Registro de Pipeline "EXMEM", es ignorado
            --en favor del dato leido de la Memoria de Datos, en el ciclo de reloj anterior
            MUX_Anti_MemD_Sel <= '1';
        
        else
            --NO Hazard
            MUX_Anti_MemD_Sel <= '0';
        end if;
    end process;

end Behavioral;
