library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Unidad_de_Anticipacion_tb is
--  Port ( );
end Unidad_de_Anticipacion_tb;

architecture Behavioral of Unidad_de_Anticipacion_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Unidad_de_Anticipacion
      Port ( --------------------------------------------------------
             --------           Puertos de Entrada           --------
             --------------------------------------------------------
             IDEX_Reg_Rs1       : in  STD_LOGIC_VECTOR(3 downto 0);
             IDEX_Reg_Rs2       : in  STD_LOGIC_VECTOR(3 downto 0);         
             EXMEM_Ctr_WB_W     : in  STD_LOGIC;
             MEMWB_Ctr_WB_W     : in  STD_LOGIC;
             EXMEM_Ctr_MEM_R    : in  STD_LOGIC;
             IDEX_Ctr_MEM_W     : in  STD_LOGIC;
             EXMEM_Reg_Rd       : in  STD_LOGIC_VECTOR(3 downto 0);
             MEMWB_Reg_Rd       : in  STD_LOGIC_VECTOR(3 downto 0);
             --------------------------------------------------------
             --------           Puertos de Salida            --------
             --------------------------------------------------------
             MUX_Anti_ALU_A_Sel : out STD_LOGIC_VECTOR(1 downto 0);
             MUX_Anti_ALU_B_Sel : out STD_LOGIC_VECTOR(1 downto 0);
             MUX_Anti_MemD_Sel  : out STD_LOGIC
      );
    END COMPONENT;

    --Inputs
    signal IDEX_Reg_Rs1         : std_logic_vector(3 downto 0):=(others => '0');
    signal IDEX_Reg_Rs2         : std_logic_vector(3 downto 0):=(others => '0');
    signal EXMEM_Ctr_WB_W       : std_logic:='0';
    signal MEMWB_Ctr_WB_W       : std_logic:='0';
    signal EXMEM_Ctr_MEM_R      : std_logic:= '0';
    signal IDEX_Ctr_MEM_W       : std_logic:='0';
    signal EXMEM_Reg_Rd         : std_logic_vector(3 downto 0):=(others => '0');
    signal MEMWB_Reg_Rd         : std_logic_vector(3 downto 0):=(others => '0');
 
    --Outputs 
    signal MUX_Anti_ALU_A_Sel    : std_logic_vector(1 downto 0);
    signal MUX_Anti_ALU_B_Sel    : std_logic_vector(1 downto 0);
    signal MUX_Anti_MemD_Sel     : std_logic;
    
begin

    --Instantiate the Unit Under Test (UUT)
    uut : Unidad_de_Anticipacion PORT MAP (
        IDEX_Reg_Rs1        => IDEX_Reg_Rs1,
        IDEX_Reg_Rs2        => IDEX_Reg_Rs2,
        EXMEM_Ctr_WB_W      => EXMEM_Ctr_WB_W,
        MEMWB_Ctr_WB_W      => MEMWB_Ctr_WB_W,
        EXMEM_Ctr_MEM_R     => EXMEM_Ctr_MEM_R,
        IDEX_Ctr_MEM_W      => IDEX_Ctr_MEM_W,
        EXMEM_Reg_Rd        => EXMEM_Reg_Rd,
        MEMWB_Reg_Rd        => MEMWB_Reg_Rd,
        MUX_Anti_ALU_A_Sel  => MUX_Anti_ALU_A_Sel,
        MUX_Anti_ALU_B_Sel  => MUX_Anti_ALU_B_Sel,
        MUX_Anti_MemD_Sel   => MUX_Anti_MemD_Sel
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        
        --No hay Riesgos
        IDEX_Reg_Rs1    <= x"8";
        IDEX_Reg_Rs2    <= x"A";
        EXMEM_Ctr_WB_W  <= '1';
        MEMWB_Ctr_WB_W  <= '0';
        EXMEM_Ctr_MEM_R <= '0';
        IDEX_Ctr_MEM_W  <= '0';
        EXMEM_Reg_Rd    <= x"5";
        MEMWB_Reg_Rd    <= x"1";        
        wait for 10 ns;
        
        --"Grupo 1B"
        IDEX_Reg_Rs1    <= x"6";
        IDEX_Reg_Rs2    <= x"2";
        EXMEM_Ctr_WB_W  <= '1';
        MEMWB_Ctr_WB_W  <= '0';
        EXMEM_Ctr_MEM_R <= '0';
        IDEX_Ctr_MEM_W  <= '0';
        EXMEM_Reg_Rd    <= x"2";
        MEMWB_Reg_Rd    <= x"D";        
        wait for 10 ns;
        
        --Copia memoria-a-memoria
        IDEX_Reg_Rs1    <= x"B";
        IDEX_Reg_Rs2    <= x"A";
        EXMEM_Ctr_WB_W  <= '0';
        MEMWB_Ctr_WB_W  <= '0';
        EXMEM_Ctr_MEM_R <= '1';
        IDEX_Ctr_MEM_W  <= '1';
        EXMEM_Reg_Rd    <= x"A";
        MEMWB_Reg_Rd    <= x"6";        
        wait for 10 ns;
        
        --"Grupo 2A"
        IDEX_Reg_Rs1    <= x"8";
        IDEX_Reg_Rs2    <= x"E";
        EXMEM_Ctr_WB_W  <= '0';
        MEMWB_Ctr_WB_W  <= '1';
        EXMEM_Ctr_MEM_R <= '0';
        IDEX_Ctr_MEM_W  <= '0';
        EXMEM_Reg_Rd    <= x"7";
        MEMWB_Reg_Rd    <= x"8";        
        wait for 10 ns;

        --"Grupo 1B"
        IDEX_Reg_Rs1    <= x"A";
        IDEX_Reg_Rs2    <= x"B";
        EXMEM_Ctr_WB_W  <= '1';
        MEMWB_Ctr_WB_W  <= '0';
        EXMEM_Ctr_MEM_R <= '0';
        IDEX_Ctr_MEM_W  <= '0';
        EXMEM_Reg_Rd    <= x"B";
        MEMWB_Reg_Rd    <= x"1";        
        wait for 10 ns;

        --Copia memoria-a-memoria
        IDEX_Reg_Rs1    <= x"1";
        IDEX_Reg_Rs2    <= x"2";
        EXMEM_Ctr_WB_W  <= '0';
        MEMWB_Ctr_WB_W  <= '0';
        EXMEM_Ctr_MEM_R <= '1';
        IDEX_Ctr_MEM_W  <= '1';
        EXMEM_Reg_Rd    <= x"2";
        MEMWB_Reg_Rd    <= x"F";        
        wait for 10 ns;

        --"Grupo 2A"
        IDEX_Reg_Rs1    <= x"5";
        IDEX_Reg_Rs2    <= x"B";
        EXMEM_Ctr_WB_W  <= '1';
        MEMWB_Ctr_WB_W  <= '1';
        EXMEM_Ctr_MEM_R <= '0';
        IDEX_Ctr_MEM_W  <= '0';
        EXMEM_Reg_Rd    <= x"B";
        MEMWB_Reg_Rd    <= x"5";        
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
