library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Registro_de_Segmentacion_MEMWB_tb is
--  Port ( );
end Registro_de_Segmentacion_MEMWB_tb;

architecture Behavioral of Registro_de_Segmentacion_MEMWB_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Registro_de_Segmentacion_MEMWB
    Port ( -----------------------------------------------------------------
           ---------              Puertos de Entrada               ---------
           -----------------------------------------------------------------
           CLK                          : in  STD_LOGIC;
           RST                          : in  STD_LOGIC;
           MEMWB_L_Ctr_WB               : in  STD_LOGIC_VECTOR(1  downto 0);
           MEMWB_L_MemD_Data_Out        : in  STD_LOGIC_VECTOR(31 downto 0);  
           MEMWB_L_ALU_Out              : in  STD_LOGIC_VECTOR(31 downto 0);         
           MEMWB_L_Reg_Rd               : in  STD_LOGIC_VECTOR(3  downto 0);
           -----------------------------------------------------------------
           ---------            Puerto Especial de Entrada         ---------
           ---------                - Interfaz Grafica -           ---------
           -----------------------------------------------------------------    
           MEMWB_L_PC                   : in  STD_LOGIC_VECTOR(6  downto 0);
           MEMWB_L_NOP                  : in  STD_LOGIC;           
           -----------------------------------------------------------------
           ---------               Puertos de Salida               ---------
           -----------------------------------------------------------------       
           MEMWB_L_Ctr_WB_W_Aux         : out STD_LOGIC;
           MEMWB_H_Ctr_WB_W             : out STD_LOGIC;
           MEMWB_L_Multi_MUX_WB_Sel_Aux : out STD_LOGIC;
           MEMWB_H_Multi_MUX_WB_Sel     : out STD_LOGIC;
           MEMWB_L_MemD_Data_Out_Aux    : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_H_MemD_Data_Out        : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_L_ALU_Out_Aux          : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_H_ALU_Out              : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_L_Reg_Rd_Aux           : out STD_LOGIC_VECTOR(3  downto 0);
           MEMWB_H_Reg_Rd               : out STD_LOGIC_VECTOR(3  downto 0);               
           -----------------------------------------------------------------
           ---------            Puerto Especial de Salida          ---------
           ---------                - Interfaz Grafica -           ---------
           -----------------------------------------------------------------
           MEMWB_H_PC                   : out STD_LOGIC_VECTOR(6  downto 0);
           MEMWB_H_NOP                  : out STD_LOGIC);
    END COMPONENT;

    --Inputs
    signal CLK                          : std_logic:='0';
    signal RST                          : std_logic:='0';
    signal MEMWB_L_Ctr_WB               : std_logic_vector(1  downto 0):=(others => '0');
    signal MEMWB_L_MemD_Data_Out        : std_logic_vector(31 downto 0):=(others => '0');
    signal MEMWB_L_ALU_Out              : std_logic_vector(31 downto 0):=(others => '0');
    signal MEMWB_L_Reg_Rd               : std_logic_vector(3  downto 0):=(others => '0');
    signal MEMWB_L_PC                   : std_logic_vector(6  downto 0):=(others => '0');
    signal MEMWB_L_NOP                  : std_logic:='0';

    --Outputs
    signal MEMWB_L_Ctr_WB_W_Aux         : std_logic;
    signal MEMWB_H_Ctr_WB_W             : std_logic;
    signal MEMWB_L_Multi_MUX_WB_Sel_Aux : std_logic;
    signal MEMWB_H_Multi_MUX_WB_Sel     : std_logic;
    signal MEMWB_L_MemD_Data_Out_Aux    : std_logic_vector(31 downto 0);
    signal MEMWB_H_MemD_Data_Out        : std_logic_vector(31 downto 0);
    signal MEMWB_L_ALU_Out_Aux          : std_logic_vector(31 downto 0);
    signal MEMWB_H_ALU_Out              : std_logic_vector(31 downto 0);
    signal MEMWB_L_Reg_Rd_Aux           : std_logic_vector(3  downto 0);
    signal MEMWB_H_Reg_Rd               : std_logic_vector(3  downto 0);
    signal MEMWB_H_PC                   : std_logic_vector(6  downto 0);
    signal MEMWB_H_NOP                  : std_logic;

    --Clock period definitions
    constant clk_period : time := 10 ns;

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Registro_de_Segmentacion_MEMWB PORT MAP (
        CLK                          => CLK,
        RST                          => RST,
        MEMWB_L_Ctr_WB               => MEMWB_L_Ctr_WB,
        MEMWB_L_MemD_Data_Out        => MEMWB_L_MemD_Data_Out,
        MEMWB_L_ALU_Out              => MEMWB_L_ALU_Out,
        MEMWB_L_Reg_Rd               => MEMWB_L_Reg_Rd,
        MEMWB_L_PC                   => MEMWB_L_PC,
        MEMWB_L_NOP                  => MEMWB_L_NOP,
        MEMWB_L_Ctr_WB_W_Aux         => MEMWB_L_Ctr_WB_W_Aux,
        MEMWB_H_Ctr_WB_W             => MEMWB_H_Ctr_WB_W,
        MEMWB_L_Multi_MUX_WB_Sel_Aux => MEMWB_L_Multi_MUX_WB_Sel_Aux,
        MEMWB_H_Multi_MUX_WB_Sel     => MEMWB_H_Multi_MUX_WB_Sel,
        MEMWB_L_MemD_Data_Out_Aux    => MEMWB_L_MemD_Data_Out_Aux,
        MEMWB_H_MemD_Data_Out        => MEMWB_H_MemD_Data_Out,
        MEMWB_L_ALU_Out_Aux          => MEMWB_L_ALU_Out_Aux,
        MEMWB_H_ALU_Out              => MEMWB_H_ALU_Out,
        MEMWB_L_Reg_Rd_Aux           => MEMWB_L_Reg_Rd_Aux,
        MEMWB_H_Reg_Rd               => MEMWB_H_Reg_Rd,
        MEMWB_H_PC                   => MEMWB_H_PC,
        MEMWB_H_NOP                  => MEMWB_H_NOP
    );
    
    --Clock process definitions
    clk_process : process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;
    
    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        RST                     <= '0';
        MEMWB_L_Ctr_WB          <= "01";
        MEMWB_L_MemD_Data_Out   <= x"AAAAAAAA";
        MEMWB_L_ALU_Out         <= x"FFFFFFFF";
        MEMWB_L_Reg_Rd          <= x"9";
        MEMWB_L_PC              <= "0000100";
        MEMWB_L_NOP             <= '1';
        wait for 10 ns;
        RST                     <= '0';
        MEMWB_L_Ctr_WB          <= "00";
        MEMWB_L_MemD_Data_Out   <= x"101010A0";
        MEMWB_L_ALU_Out         <= x"B57C6EAA";
        MEMWB_L_Reg_Rd          <= x"A";
        MEMWB_L_PC              <= "0100000";
        MEMWB_L_NOP             <= '0';
        wait for 10 ns;
        RST                     <= '0';
        MEMWB_L_Ctr_WB          <= "10";
        MEMWB_L_MemD_Data_Out   <= x"1234567D";
        MEMWB_L_ALU_Out         <= x"5A785BCE";
        MEMWB_L_Reg_Rd          <= x"0";
        MEMWB_L_PC              <= "0010000";
        MEMWB_L_NOP             <= '1';
        wait for 10 ns;
        RST                     <= '0';
        MEMWB_L_Ctr_WB          <= "11";
        MEMWB_L_MemD_Data_Out   <= x"45DAB765";
        MEMWB_L_ALU_Out         <= x"123DDCC9";
        MEMWB_L_Reg_Rd          <= x"1";
        MEMWB_L_PC              <= "0010100";
        MEMWB_L_NOP             <= '1';
        wait for 10 ns;
        RST                     <= '1';
        MEMWB_L_Ctr_WB          <= "00";
        MEMWB_L_MemD_Data_Out   <= x"11111111";
        MEMWB_L_ALU_Out         <= x"00000000";
        MEMWB_L_Reg_Rd          <= x"D";
        MEMWB_L_PC              <= "0010000";
        MEMWB_L_NOP             <= '0';
        wait for 10 ns;
        RST                     <= '0';
        MEMWB_L_Ctr_WB          <= "01";
        MEMWB_L_MemD_Data_Out   <= x"12121212";
        MEMWB_L_ALU_Out         <= x"0A0A0A0A";
        MEMWB_L_Reg_Rd          <= x"3";
        MEMWB_L_PC              <= "0011000";
        MEMWB_L_NOP             <= '0';
        wait for 10 ns;
        RST                     <= '0';
        MEMWB_L_Ctr_WB          <= "10";
        MEMWB_L_MemD_Data_Out   <= x"65656565";
        MEMWB_L_ALU_Out         <= x"135680AC";
        MEMWB_L_Reg_Rd          <= x"8";
        MEMWB_L_PC              <= "0010000";
        MEMWB_L_NOP             <= '0';
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
