----------------------------------------------------------------------------  
--  Nombre del Proyecto    : UNIPROC 2020                                       
--  Institucion            : Universidad Nacional de Ingenieria                 
--  Facultad               : FEC  
--  Carrera                : Ing. Electronica                                              
--  Pais                   : Nicaragua                                          
--  Nombre del Autor       : Jason Ortiz                                
--  Nombre del Modulo      : Registro_de_Segmentacion_MEMWB
--  Descripcion del Modulo : 
--      En este modulo se encuentra el 
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Registro_de_Segmentacion_MEMWB is
    Port ( -----------------------------------------------------------------
           ---------              Puertos de Entrada               ---------
           -----------------------------------------------------------------
           CLK                          : in  STD_LOGIC;
           RST                          : in  STD_LOGIC;
           MEMWB_L_Ctr_WB               : in  STD_LOGIC_VECTOR(1  downto 0);
           MEMWB_L_MemD_Data_Out        : in  STD_LOGIC_VECTOR(31 downto 0);  
           MEMWB_L_ALU_Out              : in  STD_LOGIC_VECTOR(31 downto 0);         
           MEMWB_L_Reg_Rd               : in  STD_LOGIC_VECTOR(3  downto 0);
           -----------------------------------------------------------------
           ---------            Puerto Especial de Entrada         ---------
           ---------                - Interfaz Grafica -           ---------
           -----------------------------------------------------------------    
           MEMWB_L_PC                   : in  STD_LOGIC_VECTOR(6  downto 0);
           MEMWB_L_NOP                  : in  STD_LOGIC;           
           -----------------------------------------------------------------
           ---------               Puertos de Salida               ---------
           -----------------------------------------------------------------       
           MEMWB_L_Ctr_WB_W_Aux         : out STD_LOGIC;
           MEMWB_H_Ctr_WB_W             : out STD_LOGIC;
           MEMWB_L_Multi_MUX_WB_Sel_Aux : out STD_LOGIC;
           MEMWB_H_Multi_MUX_WB_Sel     : out STD_LOGIC;
           MEMWB_L_MemD_Data_Out_Aux    : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_H_MemD_Data_Out        : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_L_ALU_Out_Aux          : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_H_ALU_Out              : out STD_LOGIC_VECTOR(31 downto 0);
           MEMWB_L_Reg_Rd_Aux           : out STD_LOGIC_VECTOR(3  downto 0);
           MEMWB_H_Reg_Rd               : out STD_LOGIC_VECTOR(3  downto 0);               
           -----------------------------------------------------------------
           ---------            Puerto Especial de Salida          ---------
           ---------                - Interfaz Grafica -           ---------
           -----------------------------------------------------------------
           MEMWB_H_PC                   : out STD_LOGIC_VECTOR(6  downto 0);
           MEMWB_H_NOP                  : out STD_LOGIC);
end Registro_de_Segmentacion_MEMWB;

architecture Behavioral of Registro_de_Segmentacion_MEMWB is

    signal MEMWB_PC_Aux               : std_logic_vector(6  downto 0);
    signal MEMWB_NOP_Aux              : std_logic;

    signal MEMWB_Ctr_WB_W_Aux         : std_logic;
    signal MEMWB_Multi_MUX_WB_Sel_Aux : std_logic;
    signal MEMWB_Reg_Rd_Aux           : std_logic_vector(3  downto 0);
    signal MEMWB_ALU_Out_Aux          : std_logic_vector(31 downto 0);
    signal MEMWB_MemD_Data_Out_Aux    : std_logic_vector(31 downto 0);

begin

    MEMWB_L_Ctr_WB_W_Aux         <= MEMWB_Ctr_WB_W_Aux;
    MEMWB_L_Multi_MUX_WB_Sel_Aux <= MEMWB_Multi_MUX_WB_Sel_Aux;
    MEMWB_L_MemD_Data_Out_Aux    <= MEMWB_MemD_Data_Out_Aux;
    MEMWB_L_ALU_Out_Aux          <= MEMWB_ALU_Out_Aux;
    MEMWB_L_Reg_Rd_Aux           <= MEMWB_Reg_Rd_Aux;    

    process(CLK, RST)
    begin
        --Escritura de Informacion
        if RST = '1' then
            MEMWB_Ctr_WB_W_Aux          <= '0';
            MEMWB_Multi_MUX_WB_Sel_Aux  <= '0';
            MEMWB_MemD_Data_Out_Aux     <= (others => '0'); 
            MEMWB_ALU_Out_Aux           <= (others => '0'); 
            MEMWB_Reg_Rd_Aux            <= (others => '0');   
            
            MEMWB_PC_Aux                <= (others => '0');     
            MEMWB_NOP_Aux               <= '0';  
        elsif (falling_edge(CLK)) then
            MEMWB_Ctr_WB_W_Aux         <= MEMWB_L_Ctr_WB(1);
            MEMWB_Multi_MUX_WB_Sel_Aux <= MEMWB_L_Ctr_WB(0);
            MEMWB_MemD_Data_Out_Aux    <= MEMWB_L_MemD_Data_Out;    
            MEMWB_ALU_Out_Aux          <= MEMWB_L_ALU_Out;    
            MEMWB_Reg_Rd_Aux           <= MEMWB_L_Reg_Rd;   
            
            MEMWB_PC_Aux               <= MEMWB_L_PC;
            MEMWB_NOP_Aux              <= MEMWB_L_NOP;
        end if; 

        --Lectura de Informacion
        if RST = '1' then
            MEMWB_H_Ctr_WB_W          <= '0';
            MEMWB_H_Multi_MUX_WB_Sel  <= '0';
            MEMWB_H_MemD_Data_Out     <= (others => '0');
            MEMWB_H_ALU_Out           <= (others => '0'); 
            MEMWB_H_Reg_Rd            <= (others => '0');            
            
            MEMWB_H_PC                <= (others => '0'); 
            MEMWB_H_NOP               <= '0';             
        elsif (rising_edge(CLK)) then
            MEMWB_H_Ctr_WB_W          <= MEMWB_Ctr_WB_W_Aux;
            MEMWB_H_Multi_MUX_WB_Sel  <= MEMWB_Multi_MUX_WB_Sel_Aux;
            MEMWB_H_MemD_Data_Out     <= MEMWB_MemD_Data_Out_Aux; 
            MEMWB_H_ALU_Out           <= MEMWB_ALU_Out_Aux;
            MEMWB_H_Reg_Rd            <= MEMWB_Reg_Rd_Aux;            
                
            MEMWB_H_PC                <= MEMWB_PC_Aux;
            MEMWB_H_NOP               <= MEMWB_NOP_Aux;
        end if;          
    end process;

end Behavioral;
