library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Unidad_de_Control_tb is
--  Port ( );
end Unidad_de_Control_tb;

architecture Behavioral of Unidad_de_Control_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Unidad_de_Control
    Port ( -------------------------------------------------
           -------         Puerto de Entrada         -------
           -------------------------------------------------
           Opcode        : in  STD_LOGIC_VECTOR(6 downto 0);
           -------------------------------------------------
           -------         Puertos de Salida         -------
           -------------------------------------------------           
           Control_Out2  : out STD_LOGIC;
           Control_Out1  : out STD_LOGIC_VECTOR(6 downto 0)); 
    END COMPONENT;

    --Inputs
    signal Opcode       : std_logic_vector(6 downto 0):=(others => '0');

    --Outputs
    signal Control_Out2 : std_logic;
    signal Control_Out1 : std_logic_vector(6 downto 0);

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Unidad_de_Control PORT MAP (
        Opcode       => Opcode,
        Control_Out2 => Control_Out2,
        Control_Out1 => Control_Out1
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        Opcode <= "0110011";
        wait for 10 ns;
        Opcode <= "0100011";
        wait for 10 ns;
        Opcode <= "1100011";    
        wait for 10 ns;
        Opcode <= "0000011";
        wait for 10 ns;                
        Opcode <= "0100011";
        wait for 10 ns;  
        Opcode <= "0010011";
        wait for 10 ns; 
        Opcode <= "0110011";
        wait for 10 ns; 
    wait;
    end process;

end Behavioral;
