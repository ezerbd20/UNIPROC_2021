library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Banco_de_Registros_tb is
--  Port ( );
end Banco_de_Registros_tb;

architecture Behavioral of Banco_de_Registros_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Banco_de_Registros
    Port ( -----------------------------------------------
           ------        Puertos de Entrada         ------
           ------            - Modulo -             ------
           -----------------------------------------------
           CLK        : in  STD_LOGIC; 
           RST        : in  STD_LOGIC;                                        
           Addr1_Read : in  STD_LOGIC_VECTOR(3  downto 0);
           Addr2_Read : in  STD_LOGIC_VECTOR(3  downto 0);
           Reg_Write  : in  STD_LOGIC; 
           Addr_Write : in  STD_LOGIC_VECTOR(3  downto 0);
           Data_In    : in  STD_LOGIC_VECTOR(31 downto 0);
           -----------------------------------------------
           ------     Puerto Especial de Entrada    ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           Count_V    : in  integer; 
           -----------------------------------------------
           ------         Puertos de Salida         ------
           ------             - Modulo -            ------
           -----------------------------------------------
           Data1_Out  : out STD_LOGIC_VECTOR(31 downto 0);
           Data2_Out  : out STD_LOGIC_VECTOR(31 downto 0);
           -----------------------------------------------
           ------    Puertos Especiales de Salida   ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           Reg_Left   : out STD_LOGIC_VECTOR(31 downto 0);
           Reg_Right  : out STD_LOGIC_VECTOR(31 downto 0)
           );
    END COMPONENT;

    --Inputs
    signal CLK          : std_logic:='0';
    signal RST          : std_logic:='0';
    signal Addr1_Read   : std_logic_vector(3  downto 0):=(others => '0');
    signal Addr2_Read   : std_logic_vector(3  downto 0):=(others => '0');
    signal Reg_Write    : std_logic:='0';
    signal Addr_Write   : std_logic_vector(3  downto 0):=(others => '0');
    signal Data_In      : std_logic_vector(31 downto 0):=(others => '0');
    signal Count_V      : integer:= 31;
    
    --Outputs
    signal Data1_Out    : std_logic_vector(31 downto 0);
    signal Data2_Out    : std_logic_vector(31 downto 0);
    signal Reg_Left     : std_logic_vector(31 downto 0);
    signal Reg_Right    : std_logic_vector(31 downto 0);

    --Clock period definitions
    constant clk_period : time := 10 ns;

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Banco_de_Registros PORT MAP (
        CLK         => CLK,
        RST         => RST,
        Addr1_Read  => Addr1_Read,
        Addr2_Read  => Addr2_Read,
        Reg_Write   => Reg_Write,
        Addr_Write  => Addr_Write,
        Data_In     => Data_In,
        Count_V     => Count_V,
        Data1_Out   => Data1_Out,
        Data2_Out   => Data2_Out,
        Reg_Left    => Reg_Left,
        Reg_Right   => Reg_Right
    );
    
    --Clock process definitions
    clk_process : process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;
    
    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        RST         <= '0';
        Addr1_Read  <= x"1";
        Addr2_Read  <= x"2";
        Reg_Write   <= '0';
        Addr_Write  <= x"4";
        Data_In     <= x"FFFF0000";
        Count_V     <= 31;
        wait for 10 ns;        
        RST         <= '0';
        Addr1_Read  <= x"6";
        Addr2_Read  <= x"E";
        Reg_Write   <= '0';
        Addr_Write  <= x"E";
        Data_In     <= x"AAAA0000";
        Count_V     <= 33;
        wait for 10 ns;  
        RST         <= '0';
        Addr1_Read  <= x"6";
        Addr2_Read  <= x"E";
        Reg_Write   <= '1';
        Addr_Write  <= x"E";
        Data_In     <= x"AAAA0000";
        Count_V     <= 33;
        wait for 10 ns;  
        RST         <= '0';
        Addr1_Read  <= x"0";
        Addr2_Read  <= x"E";
        Reg_Write   <= '1';
        Addr_Write  <= x"0";
        Data_In     <= x"AAAAAAAA";
        Count_V     <= 35;
        wait for 10 ns;  
        RST         <= '0';
        Addr1_Read  <= x"F";
        Addr2_Read  <= x"8";
        Reg_Write   <= '0';
        Addr_Write  <= x"0";
        Data_In     <= x"0157AF12";
        Count_V     <= 31;
        wait for 10 ns;  
        RST         <= '0';
        Addr1_Read  <= x"3";
        Addr2_Read  <= x"3";
        Reg_Write   <= '0';
        Addr_Write  <= x"0";
        Data_In     <= x"00000000";
        Count_V     <= 32;
        wait for 10 ns;  
        RST         <= '0';
        Addr1_Read  <= x"D";
        Addr2_Read  <= x"B";
        Reg_Write   <= '1';
        Addr_Write  <= x"6";
        Data_In     <= x"0157AF12";
        Count_V     <= 33;
        wait for 10 ns;  
        
    wait;
    end process;

end Behavioral;
