----------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2020
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : MUX_3_a_1
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "MUX" 3-a-1,
--      su trabajo consiste en seleccionar uno de los
---     multiples puertos de entrada que posee, y 
--      transferir a su unico puerto de salida, la 
--      informacion presente en el puerto seleccionado.  
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MUX_3_a_1 is
    Port ( ---------------------------------------------------
           ------          Puertos de Entrada           ------
           ---------------------------------------------------
           In_A  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_B  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_C  : in  STD_LOGIC_VECTOR(31 downto 0);
           Sel   : in  STD_LOGIC_VECTOR(1  downto 0);
           ---------------------------------------------------
           ------           Puerto de Salida            ------
           ---------------------------------------------------
           Out_D : out STD_LOGIC_VECTOR(31 downto 0));
end MUX_3_a_1;

architecture Behavioral of MUX_3_a_1 is

begin
    
    process (In_A, In_B, In_C, Sel)
    begin
        case Sel is
            when "01" => 
                Out_D <= In_B;
            when "10" => 
                Out_D <= In_C;
            when others => 
                Out_D <= In_A;
        end case;
    end process;

end Behavioral;