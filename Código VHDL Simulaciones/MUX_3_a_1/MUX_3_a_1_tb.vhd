library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity MUX_3_a_1_tb is
--  Port ( );
end MUX_3_a_1_tb;

architecture Behavioral of MUX_3_a_1_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT MUX_3_a_1
    Port ( ---------------------------------------------------
           ------          Puertos de Entrada           ------
           ---------------------------------------------------
           In_A  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_B  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_C  : in  STD_LOGIC_VECTOR(31 downto 0);
           Sel   : in  STD_LOGIC_VECTOR(1  downto 0);
           ---------------------------------------------------
           ------           Puerto de Salida            ------
           ---------------------------------------------------
           Out_D : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT;

    --Inputs
    signal In_A  : std_logic_vector(31 downto 0):=(others => '0');
    signal In_B  : std_logic_vector(31 downto 0):=(others => '0');
    signal In_C  : std_logic_vector(31 downto 0):=(others => '0');
    signal Sel   : std_logic_vector(1  downto 0):=(others => '0');

    --Outputs
    signal Out_D : std_logic_vector(31 downto 0);

begin

    --Instantiate the Unit Under Test (UUT)
    uut : MUX_3_a_1 PORT MAP (
        In_A    => In_A,
        In_B    => In_B,
        In_C    => In_C,
        Sel     => Sel,
        Out_D   => Out_D
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        In_A <= x"AAAAAAAA";
        In_B <= x"BBBBBBBB";
        In_C <= x"CCCCCCCC";
        Sel  <= "00";
        wait for 10 ns;
        In_A <= x"AAAAAAAA";
        In_B <= x"BBBBBBBB";
        In_C <= x"CCCCCCCC";
        Sel  <= "10";
        wait for 10 ns;
        In_A <= x"AAAAAAAA";
        In_B <= x"BBBBBBBB";
        In_C <= x"CCCCCCCC";
        Sel  <= "01";
        wait for 10 ns;    
        In_A <= x"99999999";
        In_B <= x"88888888";
        In_C <= x"77777777";
        Sel  <= "00";
        wait for 10 ns;           
        In_A <= x"11111111";
        In_B <= x"22222222";
        In_C <= x"33333333";
        Sel  <= "01";
        wait for 10 ns;   
        In_A <= x"44444444";
        In_B <= x"55555555";
        In_C <= x"66666666";
        Sel  <= "10";
        wait for 10 ns; 
        In_A <= x"77777777";
        In_B <= x"88888888";
        In_C <= x"99999999";
        Sel  <= "00";
        wait for 10 ns; 

    wait;
    end process;

end Behavioral;
