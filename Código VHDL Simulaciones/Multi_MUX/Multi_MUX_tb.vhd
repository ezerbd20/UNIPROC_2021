library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Multi_MUX_tb is
--  Port ( );
end Multi_MUX_tb;

architecture Behavioral of Multi_MUX_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Multi_MUX
    Port ( In_1A  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_2A  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_1B  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_2B  : in  STD_LOGIC_VECTOR(31 downto 0);
           Sel1   : in  STD_LOGIC;
           Sel2   : in  STD_LOGIC;
           Out_1C : out STD_LOGIC_VECTOR(31 downto 0);
           Out_2C : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT;

    --Inputs
    signal In_1A  : std_logic_vector(31 downto 0):= (others => '0');
    signal In_2A  : std_logic_vector(31 downto 0):= (others => '0');
    signal In_1B  : std_logic_vector(31 downto 0):= (others => '0');
    signal In_2B  : std_logic_vector(31 downto 0):= (others => '0');
    signal Sel1   : std_logic:= '0';
    signal Sel2   : std_logic:= '0';

    --Outputs
    signal Out_1C : std_logic_vector(31 downto 0);
    signal Out_2C : std_logic_vector(31 downto 0);

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Multi_MUX PORT MAP (
        In_1A   => In_1A,
        In_2A   => In_2A,
        In_1B   => In_1B,
        In_2B   => In_2B,
        Sel1    => Sel1,
        Sel2    => Sel2,
        Out_1C  => Out_1C,
        Out_2C  => Out_2C
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        In_1A   <= x"AAAAAAAA";
        In_2A   <= x"BBBBBBBB";
        In_1B   <= x"CCCCCCCC";
        In_2B   <= x"DDDDDDDD";
        Sel1    <= '1';
        Sel2    <= '0';
        wait for 10 ns;
        In_1A   <= x"AAAAAAAA";
        In_2A   <= x"BBBBBBBB";
        In_1B   <= x"CCCCCCCC";
        In_2B   <= x"DDDDDDDD";
        Sel1    <= '0';
        Sel2    <= '1';
        wait for 10 ns;
        In_1A   <= x"55555555";
        In_2A   <= x"66666666";
        In_1B   <= x"77777777";
        In_2B   <= x"88888888";
        Sel1    <= '1';
        Sel2    <= '0';
        wait for 10 ns;
        In_1A   <= x"55555555";
        In_2A   <= x"66666666";
        In_1B   <= x"77777777";
        In_2B   <= x"88888888";
        Sel1    <= '0';
        Sel2    <= '1';
        wait for 10 ns;
        In_1A   <= x"11111111";
        In_2A   <= x"22222222";
        In_1B   <= x"33333333";
        In_2B   <= x"44444444";
        Sel1    <= '1';
        Sel2    <= '1';
        wait for 10 ns;
        In_1A   <= x"11111111";
        In_2A   <= x"22222222";
        In_1B   <= x"33333333";
        In_2B   <= x"44444444";
        Sel1    <= '0';
        Sel2    <= '0';
        wait for 10 ns;
        In_1A   <= x"55555555";
        In_2A   <= x"66666666";
        In_1B   <= x"77777777";
        In_2B   <= x"88888888";
        Sel1    <= '1';
        Sel2    <= '1';
        wait for 10 ns;
        
    wait;
    end process;

end Behavioral;
