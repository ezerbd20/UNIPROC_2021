----------------------------------------------------------------------------  
--  Nombre del Proyecto    : UNIPROC 2020                                       
--  Institucion            : Universidad Nacional de Ingenieria                 
--  Facultad               : FEC  
--  Carrera                : Ing. Electronica                                              
--  Pais                   : Nicaragua                                          
--  Nombre del Autor       : Jason Ortiz                                
--  Nombre del Modulo      : Generador_de_Inmediatos
--  Descripcion del Modulo : 
--      En este modulo se encuentra el Generador de Inmediatos,
--      su trabajo consiste en extender hasta los 32 bits, el bit
--      de signo del valor inmediato de 12 bits, presente en las
--      instrucciones tipo I, tipo S y tipo B.
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Generador_de_Inmediatos is
    Port ( --------------------------------------------
           ------       Puerto de Entrada        ------
           --------------------------------------------
           Instr  : in  STD_LOGIC_VECTOR (31 downto 0);           
           --------------------------------------------
           ------        Puerto de Salida        ------
           --------------------------------------------
           Inm    : out STD_LOGIC_VECTOR (31 downto 0));
end Generador_de_Inmediatos;

architecture Behavioral of Generador_de_Inmediatos is
    
    alias Opcode  : std_logic_vector(6 downto 0) is Instr(6  downto 0);

begin

    process(Instr)
    --Variable para Extender el Bit de Signo
    variable Sign_Exten : std_logic_vector(20 downto 0);
    begin
        --Extender el Bit de Signo
        Sign_Exten := (20 downto 0 => Instr(31));     
                         
        --Instrucciones Tipo I
        if (Opcode = "0000011" or Opcode = "0010011") then                           
            Inm  <= Sign_Exten & Instr(30 downto 20);
             
        --Instrucciones Tipo S
        elsif (Opcode = "0100011") then                   
            Inm  <= Sign_Exten & Instr(30 downto 25) & Instr(11 downto 7);
            
        --Instrucciones Tipo B
        elsif (Opcode = "1100011") then
            Inm  <= Sign_Exten & Instr(7) & Instr(30 downto 25) & Instr(11 downto 8);
      
        --Casos no Estimados
        else
            Inm  <= (others => '0');  
        end if;
    end process;

end Behavioral;
