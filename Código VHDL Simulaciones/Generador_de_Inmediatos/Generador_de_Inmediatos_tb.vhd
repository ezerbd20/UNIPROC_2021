
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Generador_de_Inmediatos_tb is
--  Port ( );
end Generador_de_Inmediatos_tb;

architecture Behavioral of Generador_de_Inmediatos_tb is


    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Generador_de_Inmediatos
    Port ( --------------------------------------------
           ------       Puerto de Entrada        ------
           --------------------------------------------
           Instr  : in  STD_LOGIC_VECTOR (31 downto 0);           
           --------------------------------------------
           ------        Puerto de Salida        ------
           --------------------------------------------
           Inm    : out STD_LOGIC_VECTOR (31 downto 0));    
    END COMPONENT;    

    --Inputs
    signal Instr : std_logic_vector(31 downto 0):=(others => '0');

    --Outputs
    signal Inm   : std_logic_vector(31 downto 0);

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Generador_de_Inmediatos PORT MAP (
        Instr   => Instr,
        Inm     => Inm
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        Instr <= "00000001010000010010011000000011";    --lw   x12, 20(x2)
        wait for 10 ns;
        Instr <= "00000000000100000000110001100011";    --beq  x0, x1, 24
        wait for 10 ns;
        Instr <= "00000000010100010010001000100011";    --sw   x5, 4(x2)
        wait for 10 ns;
        Instr <= "11111110000000000000100011100011";    --beq  x0, x0, -16
        wait for 10 ns;
        Instr <= "11111110000000000000011011100011";    --beq  x0, x0, -20
        wait for 10 ns;
        Instr <= "00000000110000010000011100010011";    --addi  x14,  x2, 12
        wait for 10 ns;
        Instr <= "00000000001000000010110000100011";    --sw    x2, 24(x0)
        wait for 10 ns;

    wait;
    end process;
    
end Behavioral;
