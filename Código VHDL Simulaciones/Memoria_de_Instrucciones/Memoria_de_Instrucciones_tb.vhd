library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Memoria_de_Instrucciones_tb is
--  Port ( );
end Memoria_de_Instrucciones_tb;

architecture Behavioral of Memoria_de_Instrucciones_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Memoria_de_Instrucciones
    Port ( ----------------------------------------------
           ------        Puerto de Entrada         ------
           ------           - Modulo -             ------
           ----------------------------------------------
           Address   : in  STD_LOGIC_VECTOR(6  downto 0);
           ----------------------------------------------
           ------     Puerto Especial de Entrada   ------
           ------        - Interfaz Grafica -      ------
           ----------------------------------------------
           Count_V   : in  integer;
           ----------------------------------------------
           ------        Puerto de Salida          ------
           ------           - Modulo -             ------
           ----------------------------------------------
           Instr_Out : out STD_LOGIC_VECTOR(31 downto 0);
           ----------------------------------------------
           ------     Puerto Especial de Salida    ------
           ------        - Interfaz Grafica -      ------
           ----------------------------------------------
           MemI_Out  : out STD_LOGIC_VECTOR(31 downto 0)
           );    
    END COMPONENT;

    --Inputs
    signal Address      : std_logic_vector(6 downto 0):=(others => '0');
    signal Count_V      : integer:=3;
    
    --Outputs   
    signal Instr_Out    : std_logic_vector(31 downto 0);
    signal MemI_Out     : std_logic_vector(31 downto 0);
    
    --Clock period definitions
    constant clk_period : time := 10 ns;

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Memoria_de_Instrucciones PORT MAP (
        Address     => Address,
        Count_V     => Count_V,
        Instr_Out   => Instr_Out,
        MemI_Out    => MemI_Out
    );
    
    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        Address <= "0000000";
        Count_V <= 4;
        wait for 10 ns;
        Address <= "1000000";
        Count_V <= 7;
        wait for 10 ns;
        Address <= "1001000";
        Count_V <= 6;
        wait for 10 ns;
        Address <= "1010100";
        Count_V <= 8;
        wait for 10 ns;
        Address <= "1010000";
        Count_V <= 3;
        wait for 10 ns;
        Address <= "0010000";
        Count_V <= 9;
        wait for 10 ns;
        Address <= "1100100";
        Count_V <= 5;
        wait for 10 ns;
    wait;
    end process;

end Behavioral;
