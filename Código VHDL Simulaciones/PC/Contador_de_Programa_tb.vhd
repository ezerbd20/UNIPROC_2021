library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Contador_de_Programa_tb is
--  Port ( );
end Contador_de_Programa_tb;

architecture Behavioral of Contador_de_Programa_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Contador_de_Programa
        Port ( 
           -----------------------------------------------
           -------       Puertos de Entrada        -------
           -----------------------------------------------
           CLK      : in  STD_LOGIC;
           RST      : in  STD_LOGIC;
           PC_E     : in  STD_LOGIC;
           PC_In    : in  STD_LOGIC_VECTOR (6 downto 0);
           -----------------------------------------------
           -------         Puerto de Salida        -------
           -----------------------------------------------
           PC_Out   : out STD_LOGIC_VECTOR (6 downto 0));
    END COMPONENT;
    
    --Inputs
    signal CLK      : std_logic:='0';
    signal RST      : std_logic:='0';
    signal PC_E     : std_logic:='0';
    signal PC_In    : std_logic_vector(6 downto 0):=(others => '0');

    --Outputs
    signal PC_Out   : std_logic_vector(6 downto 0);

    --Clock period definitions
    constant clk_period : time := 10 ns;
            
begin

    --Instantiate the Unit Under Test (UUT)
    uut : Contador_de_Programa PORT MAP (
        CLK     => CLK,
        RST     => RST,
        PC_E    => PC_E,
        PC_In   => PC_In,
        PC_Out  => PC_Out
    );
    
    --Clock process definitions
    clk_process : process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        RST     <= '0';
        PC_E    <= '0';
        PC_In   <= "0000000";
        wait for 10 ns;
        RST     <= '0';
        PC_E    <= '0';
        PC_In   <= "1000001";
        wait for 10 ns;
        RST     <= '0';
        PC_E    <= '1';
        PC_In   <= "1000111";
        wait for 10 ns;
        RST     <= '0';
        PC_E    <= '0';
        PC_In   <= "1000000";
        wait for 10 ns;
        RST     <= '1';
        PC_E    <= '0';
        PC_In   <= "1111111";
        wait for 10 ns;
        RST     <= '0';
        PC_E    <= '0';
        PC_In   <= "1000000";
        wait for 10 ns;
        RST     <= '1';
        PC_E    <= '0';
        PC_In   <= "0000100";
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
