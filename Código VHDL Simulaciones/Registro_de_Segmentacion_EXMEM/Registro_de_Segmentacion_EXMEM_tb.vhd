library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Registro_de_Segmentacion_EXMEM_tb is
--  Port ( );
end Registro_de_Segmentacion_EXMEM_tb;

architecture Behavioral of Registro_de_Segmentacion_EXMEM_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Registro_de_Segmentacion_EXMEM
    Port ( -----------------------------------------------------------------
           ---------              Puertos de Entrada               ---------
           ----------------------------------------------------------------- 
           CLK                           : in  STD_LOGIC;
           RST                           : in  STD_LOGIC;   
           EXMEM_L_Ctr_WB                : in  STD_LOGIC_VECTOR(1  downto 0);
           EXMEM_L_Ctr_MEM               : in  STD_LOGIC_VECTOR(1  downto 0);
           EXMEM_L_ALU_Out               : in  STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_L_Anti_In_B             : in  STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_L_MUX_Anti_MemD_Sel     : in  STD_LOGIC;
           EXMEM_L_Reg_Rd                : in  STD_LOGIC_VECTOR(3  downto 0);           
           -----------------------------------------------------------------
           ---------          Puertos Especiales de Entrada        ---------
           ---------              - Interfaz Grafica -             ---------
           -----------------------------------------------------------------
           EXMEM_L_PC                    : in  STD_LOGIC_VECTOR(6  downto 0);
           EXMEM_L_NOP                   : in  STD_LOGIC;
           EXMEM_L_Reg_Rs2               : in  STD_LOGIC_VECTOR(3  downto 0);
           -----------------------------------------------------------------
           ---------               Puertos de Salida               ---------
           ----------------------------------------------------------------- 
           EXMEM_H_Ctr_WB                : out STD_LOGIC_VECTOR(1  downto 0);
           EXMEM_H_Ctr_MEM_R             : out STD_LOGIC;
           EXMEM_L_Ctr_MEM_R_Aux         : out STD_LOGIC;
           EXMEM_L_Ctr_MEM_W_Aux         : out STD_LOGIC;
           EXMEM_L_ALU_Out_Aux           : out STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_H_ALU_Out               : out STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_L_Anti_In_B_Aux         : out STD_LOGIC_VECTOR(31 downto 0);           
           EXMEM_L_MUX_Anti_MemD_Sel_Aux : out STD_LOGIC;
           EXMEM_H_Reg_Rd                : out STD_LOGIC_VECTOR(3  downto 0);
           -----------------------------------------------------------------
           ---------          Puertos Especiales de Salida         ---------
           ---------              - Interfaz Grafica -             ---------
           -----------------------------------------------------------------
           EXMEM_H_PC                    : out STD_LOGIC_VECTOR(6  downto 0);
           EXMEM_H_NOP                   : out STD_LOGIC;
           EXMEM_H_MUX_Anti_MemD_Sel     : out STD_LOGIC;         
           EXMEM_H_Reg_Rs2               : out STD_LOGIC_VECTOR(3  downto 0));  
    END COMPONENT;

    --Inputs
    signal CLK                           : std_logic:='0';
    signal RST                           : std_logic:='0';
    signal EXMEM_L_Ctr_WB                : std_logic_vector(1  downto 0):=(others => '0');
    signal EXMEM_L_Ctr_MEM               : std_logic_vector(1  downto 0):=(others => '0');
    signal EXMEM_L_ALU_Out               : std_logic_vector(31 downto 0):=(others => '0');
    signal EXMEM_L_Anti_In_B             : std_logic_vector(31 downto 0):=(others => '0');
    signal EXMEM_L_MUX_Anti_MemD_Sel     : std_logic:='0';
    signal EXMEM_L_Reg_Rd                : std_logic_vector(3  downto 0):=(others => '0');
    signal EXMEM_L_PC                    : std_logic_vector(6  downto 0):=(others => '0');
    signal EXMEM_L_NOP                   : std_logic:='0';
    signal EXMEM_L_Reg_Rs2               : std_logic_vector(3  downto 0):=(others => '0');

    --Outputs
    signal EXMEM_H_Ctr_WB                : std_logic_vector(1  downto 0);
    signal EXMEM_H_Ctr_MEM_R             : std_logic;
    signal EXMEM_L_Ctr_MEM_R_Aux         : std_logic;
    signal EXMEM_L_Ctr_MEM_W_Aux         : std_logic;
    signal EXMEM_L_ALU_Out_Aux           : std_logic_vector(31 downto 0);
    signal EXMEM_H_ALU_Out               : std_logic_vector(31 downto 0);
    signal EXMEM_L_Anti_In_B_Aux         : std_logic_vector(31 downto 0);
    signal EXMEM_L_MUX_Anti_MemD_Sel_Aux : std_logic;
    signal EXMEM_H_Reg_Rd                : std_logic_vector(3  downto 0);
    signal EXMEM_H_PC                    : std_logic_vector(6  downto 0);
    signal EXMEM_H_NOP                   : std_logic;
    signal EXMEM_H_MUX_Anti_MemD_Sel    : std_logic;
    signal EXMEM_H_Reg_Rs2               : std_logic_vector(3  downto 0);

    --Clock period definitions
    constant clk_period : time := 10 ns;

begin

    --Instantiate the Unit Under Test (UUT)
    uut : Registro_de_Segmentacion_EXMEM PORT MAP (
        CLK                             => CLK,
        RST                             => RST,
        EXMEM_L_Ctr_WB                  => EXMEM_L_Ctr_WB,
        EXMEM_L_Ctr_MEM                 => EXMEM_L_Ctr_MEM,
        EXMEM_L_ALU_Out                 => EXMEM_L_ALU_Out,
        EXMEM_L_Anti_In_B               => EXMEM_L_Anti_In_B,
        EXMEM_L_MUX_Anti_MemD_Sel       => EXMEM_L_MUX_Anti_MemD_Sel,
        EXMEM_L_Reg_Rd                  => EXMEM_L_Reg_Rd,
        EXMEM_L_PC                      => EXMEM_L_PC,
        EXMEM_L_NOP                     => EXMEM_L_NOP,
        EXMEM_L_Reg_Rs2                 => EXMEM_L_Reg_Rs2,
        EXMEM_H_Ctr_WB                  => EXMEM_H_Ctr_WB,
        EXMEM_H_Ctr_MEM_R               => EXMEM_H_Ctr_MEM_R,
        EXMEM_L_Ctr_MEM_R_Aux           => EXMEM_L_Ctr_MEM_R_Aux,
        EXMEM_L_Ctr_MEM_W_Aux           => EXMEM_L_Ctr_MEM_W_Aux,
        EXMEM_L_ALU_Out_Aux             => EXMEM_L_ALU_Out_Aux,
        EXMEM_H_ALU_Out                 => EXMEM_H_ALU_Out,
        EXMEM_L_Anti_In_B_Aux           => EXMEM_L_Anti_In_B_Aux,
        EXMEM_L_MUX_Anti_MemD_Sel_Aux   => EXMEM_L_MUX_Anti_MemD_Sel_Aux,
        EXMEM_H_Reg_Rd                  => EXMEM_H_Reg_Rd,
        EXMEM_H_PC                      => EXMEM_H_PC,
        EXMEM_H_NOP                     => EXMEM_H_NOP,
        EXMEM_H_MUX_Anti_MemD_Sel       => EXMEM_H_MUX_Anti_MemD_Sel,
        EXMEM_H_Reg_Rs2                 => EXMEM_H_Reg_Rs2
    );


    --Clock process definitions
    clk_process : process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;
    
    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        RST                         <= '0';
        EXMEM_L_Ctr_WB              <= "01";
        EXMEM_L_Ctr_MEM             <= "00";
        EXMEM_L_ALU_Out             <= x"AA000000";
        EXMEM_L_Anti_In_B           <= x"FFFFFFFF";
        EXMEM_L_MUX_Anti_MemD_Sel   <= '0';
        EXMEM_L_Reg_Rd              <= x"4";
        EXMEM_L_PC                  <= "0000100";
        EXMEM_L_NOP                 <= '1';
        EXMEM_L_Reg_Rs2             <= x"7";
        wait for 10 ns;
        RST                         <= '0';
        EXMEM_L_Ctr_WB              <= "11";
        EXMEM_L_Ctr_MEM             <= "11";
        EXMEM_L_ALU_Out             <= x"CCCCEEEE";
        EXMEM_L_Anti_In_B           <= x"55559999";
        EXMEM_L_MUX_Anti_MemD_Sel   <= '1';
        EXMEM_L_Reg_Rd              <= x"A";
        EXMEM_L_PC                  <= "0001000";
        EXMEM_L_NOP                 <= '0';
        EXMEM_L_Reg_Rs2             <= x"E";
        wait for 10 ns;
        RST                         <= '0';
        EXMEM_L_Ctr_WB              <= "10";
        EXMEM_L_Ctr_MEM             <= "10";
        EXMEM_L_ALU_Out             <= x"50505050";
        EXMEM_L_Anti_In_B           <= x"22222222";
        EXMEM_L_MUX_Anti_MemD_Sel   <= '0';
        EXMEM_L_Reg_Rd              <= x"B";
        EXMEM_L_PC                  <= "0010000";
        EXMEM_L_NOP                 <= '1';
        EXMEM_L_Reg_Rs2             <= x"C";
        wait for 10 ns;
        RST                         <= '0';
        EXMEM_L_Ctr_WB              <= "01";
        EXMEM_L_Ctr_MEM             <= "01";
        EXMEM_L_ALU_Out             <= x"12345678";
        EXMEM_L_Anti_In_B           <= x"87654321";
        EXMEM_L_MUX_Anti_MemD_Sel   <= '1';
        EXMEM_L_Reg_Rd              <= x"D";
        EXMEM_L_PC                  <= "0100000";
        EXMEM_L_NOP                 <= '0';
        EXMEM_L_Reg_Rs2             <= x"1";
        wait for 10 ns;        
        RST                         <= '1';
        EXMEM_L_Ctr_WB              <= "11";
        EXMEM_L_Ctr_MEM             <= "10";
        EXMEM_L_ALU_Out             <= x"A5A5A5A5";
        EXMEM_L_Anti_In_B           <= x"AED3462A";
        EXMEM_L_MUX_Anti_MemD_Sel   <= '0';
        EXMEM_L_Reg_Rd              <= x"E";
        EXMEM_L_PC                  <= "0010100";
        EXMEM_L_NOP                 <= '1';
        EXMEM_L_Reg_Rs2             <= x"7";
        wait for 10 ns;  
        RST                         <= '0';
        EXMEM_L_Ctr_WB              <= "00";
        EXMEM_L_Ctr_MEM             <= "00";
        EXMEM_L_ALU_Out             <= x"1234ABCD";
        EXMEM_L_Anti_In_B           <= x"A1A1A1A1";
        EXMEM_L_MUX_Anti_MemD_Sel   <= '0';
        EXMEM_L_Reg_Rd              <= x"6";
        EXMEM_L_PC                  <= "0011000";
        EXMEM_L_NOP                 <= '0';
        EXMEM_L_Reg_Rs2             <= x"3";
        wait for 10 ns;  
        RST                         <= '0';
        EXMEM_L_Ctr_WB              <= "10";
        EXMEM_L_Ctr_MEM             <= "01";
        EXMEM_L_ALU_Out             <= x"77777777";
        EXMEM_L_Anti_In_B           <= x"88888888";
        EXMEM_L_MUX_Anti_MemD_Sel   <= '1';
        EXMEM_L_Reg_Rd              <= x"1";
        EXMEM_L_PC                  <= "0010100";
        EXMEM_L_NOP                 <= '1';
        EXMEM_L_Reg_Rs2             <= x"0";
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
