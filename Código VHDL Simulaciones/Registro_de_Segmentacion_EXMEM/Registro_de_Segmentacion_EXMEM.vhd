----------------------------------------------------------------------------  
--  Nombre del Proyecto    : UNIPROC 2020                                       
--  Institucion            : Universidad Nacional de Ingenieria                 
--  Facultad               : FEC  
--  Carrera                : Ing. Electronica                                              
--  Pais                   : Nicaragua                                          
--  Nombre del Autor       : Jason Ortiz                                
--  Nombre del Modulo      : Registro_de_Segmentacion_EXMEM
--  Descripcion del Modulo : 
--      En este modulo se encuentra el 
----------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Registro_de_Segmentacion_EXMEM is
    Port ( -----------------------------------------------------------------
           ---------              Puertos de Entrada               ---------
           ----------------------------------------------------------------- 
           CLK                           : in  STD_LOGIC;
           RST                           : in  STD_LOGIC;   
           EXMEM_L_Ctr_WB                : in  STD_LOGIC_VECTOR(1  downto 0);
           EXMEM_L_Ctr_MEM               : in  STD_LOGIC_VECTOR(1  downto 0);
           EXMEM_L_ALU_Out               : in  STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_L_Anti_In_B             : in  STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_L_MUX_Anti_MemD_Sel     : in  STD_LOGIC;
           EXMEM_L_Reg_Rd                : in  STD_LOGIC_VECTOR(3  downto 0);           
           -----------------------------------------------------------------
           ---------          Puertos Especiales de Entrada        ---------
           ---------              - Interfaz Grafica -             ---------
           -----------------------------------------------------------------
           EXMEM_L_PC                    : in  STD_LOGIC_VECTOR(6  downto 0);
           EXMEM_L_NOP                   : in  STD_LOGIC;
           EXMEM_L_Reg_Rs2               : in  STD_LOGIC_VECTOR(3  downto 0);
           -----------------------------------------------------------------
           ---------               Puertos de Salida               ---------
           ----------------------------------------------------------------- 
           EXMEM_H_Ctr_WB                : out STD_LOGIC_VECTOR(1  downto 0);
           EXMEM_H_Ctr_MEM_R             : out STD_LOGIC;
           EXMEM_L_Ctr_MEM_R_Aux         : out STD_LOGIC;
           EXMEM_L_Ctr_MEM_W_Aux         : out STD_LOGIC;
           EXMEM_L_ALU_Out_Aux           : out STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_H_ALU_Out               : out STD_LOGIC_VECTOR(31 downto 0);
           EXMEM_L_Anti_In_B_Aux         : out STD_LOGIC_VECTOR(31 downto 0);           
           EXMEM_L_MUX_Anti_MemD_Sel_Aux : out STD_LOGIC;
           EXMEM_H_Reg_Rd                : out STD_LOGIC_VECTOR(3  downto 0);
           -----------------------------------------------------------------
           ---------          Puertos Especiales de Salida         ---------
           ---------              - Interfaz Grafica -             ---------
           -----------------------------------------------------------------
           EXMEM_H_PC                    : out STD_LOGIC_VECTOR(6  downto 0);
           EXMEM_H_NOP                   : out STD_LOGIC;
           EXMEM_H_MUX_Anti_MemD_Sel     : out STD_LOGIC;         
           EXMEM_H_Reg_Rs2               : out STD_LOGIC_VECTOR(3  downto 0));
end Registro_de_Segmentacion_EXMEM;

architecture Behavioral of Registro_de_Segmentacion_EXMEM is

    signal EXMEM_PC_Aux                : std_logic_vector(6  downto 0);
    signal EXMEM_NOP_Aux               : std_logic;

    signal EXMEM_Ctr_WB_Aux            : std_logic_vector(1  downto 0);
    signal EXMEM_Ctr_MEM_R_Aux         : std_logic;
    signal EXMEM_Ctr_MEM_W_Aux         : std_logic;
    signal EXMEM_Reg_Rs2_Aux           : std_logic_vector(3  downto 0);
    signal EXMEM_ALU_Out_Aux           : std_logic_vector(31 downto 0);
    signal EXMEM_MUX_Anti_MemD_Sel_Aux : std_logic;
    signal EXMEM_Anti_In_B_Aux         : std_logic_vector(31 downto 0);
    signal EXMEM_Reg_Rd_Aux            : std_logic_vector(3  downto 0);

begin

    EXMEM_L_Ctr_MEM_R_Aux         <= EXMEM_Ctr_MEM_R_Aux;
    EXMEM_L_Ctr_MEM_W_Aux         <= EXMEM_Ctr_MEM_W_Aux;
    EXMEM_L_ALU_Out_Aux           <= EXMEM_ALU_Out_Aux;
    EXMEM_L_MUX_Anti_MemD_Sel_Aux <= EXMEM_MUX_Anti_MemD_Sel_Aux;
    EXMEM_L_Anti_In_B_Aux         <= EXMEM_Anti_In_B_Aux;


    process(CLK, RST)
    begin
        --Escritura de Informacion
        if RST = '1' then
            EXMEM_Ctr_WB_Aux            <= (others => '0');
            EXMEM_Ctr_MEM_R_Aux         <= '0';
            EXMEM_Ctr_MEM_W_Aux         <= '0';
            EXMEM_ALU_Out_Aux           <= (others => '0');
            EXMEM_Anti_In_B_Aux         <= (others => '0');
            EXMEM_MUX_Anti_MemD_Sel_Aux <= '0';
            EXMEM_Reg_Rd_Aux            <= (others => '0');
            EXMEM_Reg_Rs2_Aux           <= (others => '0');
            
            EXMEM_PC_Aux                <= (others => '0');
            EXMEM_NOP_Aux               <= '0';
        elsif (falling_edge(CLK)) then
            EXMEM_Ctr_WB_Aux            <= EXMEM_L_Ctr_WB;
            EXMEM_Ctr_MEM_R_Aux         <= EXMEM_L_Ctr_MEM(0);
            EXMEM_Ctr_MEM_W_Aux         <= EXMEM_L_Ctr_MEM(1);
            EXMEM_ALU_Out_Aux           <= EXMEM_L_ALU_Out; 
            EXMEM_Anti_In_B_Aux         <= EXMEM_L_Anti_In_B;
            EXMEM_MUX_Anti_MemD_Sel_Aux <= EXMEM_L_MUX_Anti_MemD_Sel;
            EXMEM_Reg_Rd_Aux            <= EXMEM_L_Reg_Rd;
            EXMEM_Reg_Rs2_Aux           <= EXMEM_L_Reg_Rs2;    
            
            EXMEM_PC_Aux                <= EXMEM_L_PC;
            EXMEM_NOP_Aux               <= EXMEM_L_NOP;                 
        end if;    
        
        --Lectura de Informacion
        if RST = '1' then
            EXMEM_H_Ctr_WB               <= (others => '0');
            EXMEM_H_Ctr_MEM_R            <= '0';
            EXMEM_H_ALU_Out              <= (others => '0');       
            EXMEM_H_MUX_Anti_MemD_Sel    <= '0';
            EXMEM_H_Reg_Rd               <= (others => '0');

            EXMEM_H_PC                   <= (others => '0');
            EXMEM_H_NOP                  <= '0';
            EXMEM_H_Reg_Rs2              <= (others => '0');            
        elsif (rising_edge(CLK)) then
            EXMEM_H_Ctr_WB               <= EXMEM_Ctr_WB_Aux;
            EXMEM_H_Ctr_MEM_R            <= EXMEM_Ctr_MEM_R_Aux;
            EXMEM_H_ALU_Out              <= EXMEM_ALU_Out_Aux;          
            EXMEM_H_MUX_Anti_MemD_Sel    <= EXMEM_MUX_Anti_MemD_Sel_Aux;
            EXMEM_H_Reg_Rd               <= EXMEM_Reg_Rd_Aux;

            EXMEM_H_PC                   <= EXMEM_PC_Aux;
            EXMEM_H_NOP                  <= EXMEM_NOP_Aux;
            EXMEM_H_Reg_Rs2              <= EXMEM_Reg_Rs2_Aux;
        end if;                
    end process;

end Behavioral;
