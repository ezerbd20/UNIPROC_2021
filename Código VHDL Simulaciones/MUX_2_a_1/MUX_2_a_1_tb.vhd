library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MUX_2_a_1_tb is
--Puerto Generico
Generic (M   : Integer := 32);
--  Port ( );
end MUX_2_a_1_tb;

architecture Behavioral of MUX_2_a_1_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT MUX_2_a_1
    --Puerto Generico
    Generic (N   : Integer);
    --Puertos Normales
    Port ( ---------------------------------------------
           ------        Puertos de Entrada       ------
           ---------------------------------------------
           In_A  : in  STD_LOGIC_VECTOR(N - 1 downto 0);
           In_B  : in  STD_LOGIC_VECTOR(N - 1 downto 0);
           Sel   : in  STD_LOGIC;
           ---------------------------------------------
           ------        Puerto de Salida         ------
           ---------------------------------------------
           Out_C : out STD_LOGIC_VECTOR(N - 1 downto 0));  
    END COMPONENT;

    --Inputs
    signal In_A  : std_logic_vector(M - 1 downto 0):= (others => '0');
    signal In_B  : std_logic_vector(M - 1 downto 0):= (others => '0');
    signal Sel   : std_logic:= '0';

    --Outputs
    signal Out_C : std_logic_vector(M - 1 downto 0);

begin

    --Instantiate the Unit Under Test (UUT)
    uut : MUX_2_a_1
        GENERIC MAP (N  => M)
        PORT MAP (
        In_A    => In_A,
        In_B    => In_B,
        Sel     => Sel,
        Out_C   => Out_C
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        In_A <= (others => '0');
        In_B <= (others => '1');
        Sel  <= '1';
        wait for 10 ns;
        In_A <= x"EEEEEEEE";
        In_B <= x"AAAAAAAA";
        Sel  <= '0';
        wait for 10 ns;
        In_A <= x"22222222";
        In_B <= x"11111111";
        Sel  <= '1';
        wait for 10 ns;
        In_A <= x"12345678";
        In_B <= x"87654321";
        Sel  <= '0';
        wait for 10 ns;
        In_A <= x"DFDFDFDF";
        In_B <= x"ADADADAD";
        Sel  <= '1';
        wait for 10 ns;
        In_A <= x"5A5A5A5A";
        In_B <= x"00000000";
        Sel  <= '0';
        wait for 10 ns;
        In_A <= x"77777777";
        In_B <= x"33333333";
        Sel  <= '1';
        wait for 10 ns;
 
    wait;
    end process;

end Behavioral;
