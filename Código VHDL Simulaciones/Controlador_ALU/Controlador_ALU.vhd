-------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2020
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Controlador_ALU
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "Controlador ALU", 
--      su trabajo consiste en ser el elemento de control 
--      secundario que genera la se�al de control para la 
--      ALU.
-------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Controlador_ALU is
    Port ( --------------------------------------------
           -----        Puertos de Entrada        -----
           --------------------------------------------
           ALU_Op  : in  STD_LOGIC_VECTOR (1 downto 0);
           Funct3  : in  STD_LOGIC_VECTOR (2 downto 0);
           Funct7  : in  STD_LOGIC_VECTOR (6 downto 0);
           --------------------------------------------
           -----         Puerto de Salida         -----
           --------------------------------------------
           ALU_Ctr : out STD_LOGIC_VECTOR (3 downto 0));
end Controlador_ALU;

architecture Behavioral of Controlador_ALU is


begin

    process(ALU_Op, Funct3, Funct7)
    begin    
        --Instrucciones lw, sw
        if (ALU_Op = "01")then
            ALU_Ctr <= "0000";--ADD
            
        --Instrucciones Aritmeticas y Logicas
        --Instrucciones Aritmeticas y Logicas con Inmediatos
        elsif (ALU_Op = "10") then   
                       
             --Instruccion SUB
            if (Funct3 = "000") and (Funct7 = "0100000") then
                ALU_Ctr <= "0001";--SUBSTRACT
                
            --Instrucciones ADD y ADDI
            elsif (Funct3 = "000") then  
                ALU_Ctr <= "0000";--ADD
                
            --Instrucciones AND y ANDI
            elsif (Funct3 = "111") then                
                ALU_Ctr <= "0010";--AND
            
            --Instrucciones OR y ORI
            elsif (Funct3 = "110") then             
                ALU_ctr <= "0011";--OR        
            
            --Instrucciones XOR y XORI
            elsif (Funct3 = "100") then                       
                ALU_Ctr <= "0100";--XOR 
                
            --Instrucciones SLL y SLLI
            elsif (Funct3 = "001") then          
                ALU_Ctr <= "0101";--SLL
                
            --Instrucciones SRA y SRAI
            elsif (Funct3 = "101") and (Funct7 = "0100000") then 
                ALU_Ctr <= "0111";--SRA

            --Instrucciones SRL y SRLI
            elsif (Funct3 = "101") then  
                ALU_Ctr <= "0110";--SRL
                
            --Instrucciones SLT y SLTI            
            elsif (Funct3 = "010") then
                ALU_Ctr <= "1000";--SLT
                
            --Instrucciones STLU y SLTIU
            elsif (Funct3 = "011") then                
                ALU_Ctr <= "1001";--SLTU                                                                               

            --Casos no Estimados
            else    
                ALU_Ctr <= "1111"; --Establecer salida de la ALU en 0
            end if;           
        
        --Instrucciones de Salto Condicional y Casos no Estimados
        else
            ALU_Ctr <= "1111"; --Establecer salida de la ALU en 0
        end if;                
    end process;

end Behavioral;
