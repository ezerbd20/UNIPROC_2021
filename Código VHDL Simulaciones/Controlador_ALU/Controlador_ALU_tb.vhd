library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Controlador_ALU_tb is
--  Port ( );
end Controlador_ALU_tb;

architecture Behavioral of Controlador_ALU_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Controlador_ALU
    Port ( --------------------------------------------
           -----        Puertos de Entrada        -----
           --------------------------------------------
           ALU_Op  : in  STD_LOGIC_VECTOR (1 downto 0);
           Funct3  : in  STD_LOGIC_VECTOR (2 downto 0);
           Funct7  : in  STD_LOGIC_VECTOR (6 downto 0);
           --------------------------------------------
           -----         Puerto de Salida         -----
           --------------------------------------------
           ALU_Ctr : out STD_LOGIC_VECTOR (3 downto 0));
    END COMPONENT;
    
    --Inputs
    signal ALU_Op   : std_logic_vector(1 downto 0):=(others => '0');
    signal Funct3   : std_logic_vector(2 downto 0):=(others => '0');
    signal Funct7   : std_logic_vector(6 downto 0):=(others => '0');
        
    --Outputs
    signal ALU_Ctr  : std_logic_vector(3 downto 0);
        
begin

    --Instantiate the Unit Under Test (UUT)
    uut : Controlador_ALU PORT MAP (
        ALU_Op  => ALU_Op,
        Funct3  => Funct3,
        Funct7  => Funct7,
        ALU_Ctr => ALU_Ctr
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        ALU_Op  <= "10";
        Funct3  <= "000";
        Funct7  <= "0100000";
        wait for 10 ns;
        ALU_Op  <= "01";
        Funct3  <= "000";
        Funct7  <= "0100000";
        wait for 10 ns;
        ALU_Op  <= "10";
        Funct3  <= "000";
        Funct7  <= "0100001";
        wait for 10 ns;
        ALU_Op  <= "11";
        Funct3  <= "001";
        Funct7  <= "1111111";
        wait for 10 ns;                
        ALU_Op  <= "10";
        Funct3  <= "101";
        Funct7  <= "1111110";
        wait for 10 ns; 
        ALU_Op  <= "10";
        Funct3  <= "010";
        Funct7  <= "0000000";
        wait for 10 ns; 
        ALU_Op  <= "01";
        Funct3  <= "011";
        Funct7  <= "0010000";
        wait for 10 ns; 

    wait;
    end process;
    
end Behavioral;
