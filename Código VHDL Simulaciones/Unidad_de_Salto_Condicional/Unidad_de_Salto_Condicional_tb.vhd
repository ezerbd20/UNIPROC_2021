library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Unidad_de_Salto_Condicional_tb is
--  Port ( );
end Unidad_de_Salto_Condicional_tb;

architecture Behavioral of Unidad_de_Salto_Condicional_tb is

    --Component Declaration for the Unit Under Test (UUT)
    COMPONENT Unidad_de_Salto_Condicional
    Port ( Ctr_Branch       : in  STD_LOGIC;
           Funct3           : in  STD_LOGIC_VECTOR(2  downto 0);
           Rs1_Data_Read    : in  STD_LOGIC_VECTOR(31 downto 0);
           Rs2_Data_Read    : in  STD_LOGIC_VECTOR(31 downto 0);
           Branch_Taken     : out STD_LOGIC);
    END COMPONENT;

    --Inputs
    signal Ctr_Branch       : std_logic:='0';
    signal Funct3           : std_logic_vector(2  downto 0):=(others => '0');
    signal Rs1_Data_Read    : std_logic_vector(31 downto 0):=(others => '0');
    signal Rs2_Data_Read    : std_logic_vector(31 downto 0):=(others => '0');    
       
    --Outputs    
    signal Branch_Taken     : std_logic;
    
begin

    --Instantiate the Unit Under Test (UUT)
    uut : Unidad_de_Salto_Condicional PORT MAP (
        Ctr_Branch      => Ctr_Branch,
        Funct3          => Funct3,
        Rs1_Data_Read   => Rs1_Data_Read,
        Rs2_Data_Read   => Rs2_Data_Read,
        Branch_Taken    => Branch_Taken
    );

    --Stimulus Process
    stim_proc : process
    begin
        
        --Insert Stimulus here
        Ctr_Branch      <= '0';
        Funct3          <= "010";
        Rs1_Data_Read   <= x"AAAAAAAA";
        Rs2_Data_Read   <= x"AAAAAAAA";
        wait for 10 ns;
        Ctr_Branch      <= '1';
        Funct3          <= "000";
        Rs1_Data_Read   <= x"CCCCCCCC";
        Rs2_Data_Read   <= x"CCCCCCCC";
        wait for 10 ns;
        Ctr_Branch      <= '1';
        Funct3          <= "100";
        Rs1_Data_Read   <= x"AAAAAAAA";
        Rs2_Data_Read   <= x"CCCCCCCC";
        wait for 10 ns;        
        Ctr_Branch      <= '1';
        Funct3          <= "001";
        Rs1_Data_Read   <= x"BBBBBBBB";
        Rs2_Data_Read   <= x"55555555";
        wait for 10 ns;  
        Ctr_Branch      <= '0';
        Funct3          <= "001";
        Rs1_Data_Read   <= x"BBBBBBBB";
        Rs2_Data_Read   <= x"55555555";
        wait for 10 ns;   
        Ctr_Branch      <= '1';
        Funct3          <= "000";
        Rs1_Data_Read   <= x"00000002";
        Rs2_Data_Read   <= x"00000001";
        wait for 10 ns;   
        Ctr_Branch      <= '0';
        Funct3          <= "100";
        Rs1_Data_Read   <= x"00000003";
        Rs2_Data_Read   <= x"00000003";
        wait for 10 ns;

    wait;
    end process;

end Behavioral;
