------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Banco_de_Registros
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "Banco de Registros", 
--      su trabajo consiste en almacenar los datos que seran
--      utilizados como operandos en las instrucciones que
--      el microprocesador ejecutara.
------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Banco_de_Registros is
    Port ( -----------------------------------------------
           ------        Puertos de Entrada         ------
           ------            - Modulo -             ------
           -----------------------------------------------
           CLK        : in  STD_LOGIC; 
           RST        : in  STD_LOGIC;                                        
           Addr1_Read : in  STD_LOGIC_VECTOR(3  downto 0);
           Addr2_Read : in  STD_LOGIC_VECTOR(3  downto 0);
           Reg_Write  : in  STD_LOGIC; 
           Addr_Write : in  STD_LOGIC_VECTOR(3  downto 0);
           Data_In    : in  STD_LOGIC_VECTOR(31 downto 0);
           -----------------------------------------------
           ------     Puerto Especial de Entrada    ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           Count_V    : in  integer; 
           -----------------------------------------------
           ------         Puertos de Salida         ------
           ------             - Modulo -            ------
           -----------------------------------------------
           Data1_Out  : out STD_LOGIC_VECTOR(31 downto 0);
           Data2_Out  : out STD_LOGIC_VECTOR(31 downto 0);
           -----------------------------------------------
           ------    Puertos Especiales de Salida   ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           Reg_Left   : out STD_LOGIC_VECTOR(31 downto 0);
           Reg_Right  : out STD_LOGIC_VECTOR(31 downto 0)
           );
end Banco_de_Registros;

architecture Behavioral of Banco_de_Registros is

    -------------------------------------------------------------------
    ---------      Declaracion del Array "registers"      -------------
    -------------------------------------------------------------------   
    type registers is array (0 to 15) of std_logic_vector(31 downto 0);      
    
    -------------------------------------------------------------------
    -------------       Declaracion e Inicializacion      -------------
    -------------          del Banco de Registros         -------------
    -------------------------------------------------------------------
    signal Reg             :registers:=(
        x"00000000",     --00
        x"00000001",     --01       
        x"0000000C",     --02 
        x"FFFF0000",     --03       
        x"FFFFFFFF",     --04      
        x"55555555",     --05
        x"0000FFFF",     --06 
        x"55555555",     --07     
        x"FFFFFFFF",     --08 
        x"FFFFFFFF",     --09
        x"FFFFFFFF",     --10
        x"EEEEEEEE",     --11
        x"FFFFFFFF",     --12
        x"FFFFFFFF",     --13
        x"FFFFFFFF",     --14
        x"FFFFFFFF");    --15       


begin
    
    PROCESS(CLK, RST)
    begin
       --Establecer nuevamente los valores iniciales del banco de registros
        if RST = '1' then
            Reg(0)  <= x"00000000";
            Reg(1)  <= x"00000001";
            Reg(2)  <= x"0000000C";
            Reg(3)  <= x"FFFF0000";
            Reg(4)  <= x"FFFFFFFF";
            Reg(5)  <= x"55555555";
            Reg(6)  <= x"0000FFFF";
            Reg(7)  <= x"55555555";
            Reg(8)  <= x"FFFFFFFF";
            Reg(9)  <= x"FFFFFFFF";
            Reg(10) <= x"FFFFFFFF";
            Reg(11) <= x"EEEEEEEE";
            Reg(12) <= x"FFFFFFFF";
            Reg(13) <= x"FFFFFFFF";
            Reg(14) <= x"FFFFFFFF";
            Reg(15) <= x"FFFFFFFF";
                   
        --Escibir Datos en la primera mitad de los ciclos de reloj
        elsif (rising_edge(CLK)) then
             
            --Verificar que el registro numero "0", no sera escrito
            --Verificar que el puerto de escritura, ha sido establecido en '1' logico
            if (Reg_Write = '1' and Addr_Write /= x"0") then
                --Escribir el dato "Data_In", en el registro numero "Addr_Write"
                Reg(conv_integer(Addr_Write)) <= Data_In;       
            end if;       
                      
        end if;
    end process;


    --Retornar el dato almacenado en el registro numero "Addr1_Read"
    Data1_Out <= Reg(conv_integer(Addr1_Read));
    
    --Retornar el dato almacenado en el registro numero "Addr2_Read"
    Data2_Out <= Reg(conv_integer(Addr2_Read));
           
    -----------------------------------------------------
    --------     Segmento de Codigo para      -----------
    --------       la Interfaz Grafica        -----------
    -----------------------------------------------------
    --Registros de la Izquierda (0 - 7)
	 Reg_Left   <= Reg(Count_V - 30); 
	 
	 --Registros de la Derecha (08 - 15)
	 Reg_Right   <= Reg(Count_V - 22);
    
end Behavioral;
