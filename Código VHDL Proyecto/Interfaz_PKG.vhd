----------------------------------------------------------------------------   
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Paquete     : Interfaz_PKG
--  Descripcion del Modulo : 
--      Paquete VHDL que alberga funciones y elementos necesarios para generar 
--      la interfaz grafica del microprocesador
----------------------------------------------------------------------------     

library ieee;
use ieee.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


package Interfaz_PKG is
  
  --Tipo Array, Utilizado para Representar los Caracteres Mostrados
  --en la Interfaz Grafica
  type Caracter  is array (0 to 11) of std_logic_vector (11 downto 0);
  
  --Tipo Array, Utilizado para Recrear la Interfaz Grafica
  type Screen is array (0 to 39) of String(1 to 53);

  --Tipo Record, Creado para el Retorno de la Funcion "Interface_Animation"
  type Inter_Anim is record
    Charac       : character;
    Change_Color : std_logic_vector(1 downto 0);
 end record;

  --Tipo Record, creado para el retorno de la funcion "Charac_Print"
  type VGA_Color is record
    Red      : std_logic_vector(4 downto 0);
    Green    : std_logic_vector(5 downto 0);
    Blue     : std_logic_vector(4 downto 0);
 end record;


    -------------------------------------------------------------------------- 
    -------         Declaracion de Arrays que Representan los          -------
    -------     Caracteres que se muestran en la Interfaz Gragica      -------
    --------------------------------------------------------------------------    
    
    --Array que representa la letra "A"
    constant A                             : caracter:=(
          "000000000000",
          "000001100000",
          "000011110000",
          "000111111000",
          "001110011100",
          "001100001100",
          "001100001100",
          "001111111100",
          "001111111100",
          "001100001100",
          "001100001100",
          "000000000000");

    --Array que representa la letra "B"
    constant B                             : caracter:=(
          "000000000000",
          "001111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "001111111000",
          "001111111000",
          "001100001100",
          "001100001100",
          "001111111100",
          "001111111000",
          "000000000000");

    --Array que representa la letra "C"
    constant C                             : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");

    --Array que representa la letra "D"
    constant D                             : caracter:=(
          "000000000000",
          "001111111000",
          "001111111100",
          "000110001100",
          "000110001100",
          "000110001100",
          "000110001100",
          "000110001100",
          "000110001100",
          "001111111100",
          "001111111000",
          "000000000000");

    --Array que representa la letra "E"
    constant E                             : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "001100000000",
          "001100000000",
          "001111100000",
          "001111100000",
          "001100000000",
          "001100000000",
          "001111111100",
          "001111111100",
          "000000000000");
          
    --Array que representa la letra "F"
    constant F                             : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "001100000000",
          "001100000000",
          "001111100000",
          "001111100000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "000000000000");          

    --Array que representa la letra "G"
    constant G                             : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100000000",
          "001100000000",
          "001100111100",
          "001100111100",
          "001100001100",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");
          
    --Array que representa la letra "H"
    constant H                             : caracter:=(
          "000000000000",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001111111100",
          "001111111100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "000000000000");          

    --Array que representa la letra "I"
    constant I                             : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "001111111100",
          "001111111100",
          "000000000000");

    --Array que representa la letra "J"
    constant J                             : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "000000110000",
          "000000110000",
          "000000110000",
          "000000110000",
          "001100110000",
          "001100110000",
          "001111110000",
          "000111100000",
          "000000000000");
          
    --Array que representa la letra "K"
    constant K                             : caracter:=(
          "000000000000",
          "001100001100",
          "001100011100",
          "001100111000",
          "001101110000",
          "001111100000",
          "001111100000",
          "001101110000",
          "001100111000",
          "001100011100",
          "001100001100",
          "000000000000");
          
    --Array que representa la letra "L"
    constant L                            : caracter:=(
          "000000000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001111111100",
          "001111111100",
          "000000000000");


     --Array que representa la letra "M"
     constant M                            : caracter:=(
          "000000000000",
          "001100001100",
          "001110011100",
          "001111111100",
          "001111111100",
          "001101101100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "000000000000");

     --Array que representa la letra "N"
     constant N                            : caracter:=(
          "000000000000",
          "001100001100",
          "001110001100",
          "001111001100",
          "001111101100",
          "001101111100",
          "001100111100",
          "001100011100",
          "001100001100",
          "001100001100",
          "001100001100",
          "000000000000");

     --Array que representa la letra "O"
     constant O                            : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");

     --Array que representa la letra "P"
     constant P                            : caracter:=(
          "000000000000",
          "001111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "001111111100",
          "001111111000",
          "001100000000",
          "001100000000",
          "001100000000",
          "001100000000",
          "000000000000");

     --Array que representa la letra "Q"
     constant Q                            : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001101101100",
          "001101111100",
          "001100111000",
          "001111111100",
          "000111101100",
          "000000000000");
          
     --Array que representa la letra "R"
     constant R                            : caracter := (
          "000000000000",
          "001111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "001111111100",
          "001111111000",
          "001100011100",
          "001100001100",
          "001100001100",
          "001100001100",
          "000000000000");

     --Array que representa la letra "S"
     constant S                            : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "001100000000",
          "001111111000",
          "000111111100",
          "000000001100",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");

      --Array que representa la letra "T"
      constant T                           : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000000000000");

      --Array que representa la letra "U"
      constant U                           : caracter:=(
          "000000000000",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");
          
      --Array que representa la letra "V"
      constant V                           : caracter:=(
          "000000000000",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "000111111000",
          "000011110000",
          "000001100000",
          "000000000000");
          
      --Array que representa la letra "W"
      constant W                           : caracter:=(
          "000000000000",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001101101100",
          "001111111100",
          "001111111100",
          "001110011100",
          "001100001100",
          "000000000000");          
          
      --Array que representa la letra "X"
      constant X                           : caracter:=(
          "000000000000",
          "001100001100",
          "001100001100",
          "001110011100",
          "000111011000",
          "000011110000",
          "000011110000",
          "000110111000",
          "001110011100",
          "001100001100",
          "001100001100",
          "000000000000");          

      --Array que representa la letra "Y"
      constant Y                           : caracter:=(
          "000000000000",
          "001100001100",
          "001100001100",
          "001100001100",
          "001100001100",
          "001111111100",
          "000111111100",
          "000000001100",
          "000000001100",
          "001111111100",
          "001111111000",
          "000000000000");

      --Array que representa la letra "Z"
      constant Z                           : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "000000011100",
          "000000111000",
          "000001110000",
          "000011100000",
          "000111000000",
          "001110000000",
          "001111111100",
          "001111111100",
          "000000000000");

      --Array que representa al Numero "0"
      constant cero                        : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100011100",
          "001100111100",
          "001101111100",
          "001111101100",
          "001111001100",
          "001110001100",
          "001111111100",
          "000111111000",
          "000000000000");

      --Array que representa al Numero "1"
      constant uno                         : caracter:=(
          "000000000000",
          "000001100000",
          "000011100000",
          "000111100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000111111000",
          "000111111000",
          "000000000000");

      --Array que representa al Numero "2"
      constant dos                         : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "000000011100",
          "000000111000",
          "000001110000",
          "000011100000",
          "000111000000",
          "001111111100",
          "001111111100",
          "000000000000");

      --Array que representa al Numero "3"
      constant tres                        : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "000000001100",
          "000001111000",
          "000001111000",
          "000000001100",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");

      --Array que representa al Numero "4"
      constant cuatro                      : caracter:=(
          "000000000000",
          "000000111000",
          "000001111000",
          "000011111000",
          "000111011000",
          "001110011000",
          "001100011000",
          "001111111100",
          "001111111100",
          "000000011000",
          "000000011000",
          "000000000000");

      --Array que representa al Numero "5"
      constant cinco                       : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "001100000000",
          "001100000000",
          "001111111000",
          "001111111100",
          "000000001100",
          "000000001100",
          "001111111100",
          "001111111000",
          "000000000000");

      --Array que representa al Numero "6"
      constant seis                        : caracter:=(
          "000000000000",
          "000111111100",
          "001111111100",
          "001100000000",
          "001100000000",
          "001111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");

      --Array que representa al Numero "7"
      constant siete                       : caracter:=(
          "000000000000",
          "001111111100",
          "001111111100",
          "000000001100",
          "000000011000",
          "000000110000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000001100000",
          "000000000000");

      --Array que representa al Numero "8"
      constant ocho                        : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "000111111000",
          "000111111000",
          "001100001100",
          "001100001100",
          "001111111100",
          "000111111000",
          "000000000000");

      --Array que representa al Numero "9"
      constant nueve                       : caracter:=(
          "000000000000",
          "000111111000",
          "001111111100",
          "001100001100",
          "001100001100",
          "000111111100",
          "000111111100",
          "000000001100",
          "000000001100",
          "000111111100",
          "000111111000",
          "000000000000");

      --Array que representa al caracter especial "."
      constant punto                       : caracter:=(
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000001100000",
          "000001100000",
          "000000000000");

      --Array que representa al caracter especial "-"
      constant guion                       : caracter:=(
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "001111111100",
          "001111111100",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000");

      --Array que representa al caracter especial "_"
      constant guion_bajo                  : caracter  := (
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "001111111100",
          "001111111100",
          "000000000000");

      --Array que Representa al caracter Especial " "
      constant espacio                     : caracter  := (
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000",
          "000000000000");

      --Array que Representa al Caracter Especial "("
      constant parentesis_1                : caracter  := (
          "000000000000",
          "000000110000",
          "000001100000",
          "000011000000",
          "000110000000",
          "000110000000",
          "000110000000",
          "000110000000",
          "000011000000",
          "000001100000",
          "000000110000",
          "000000000000");
          
      --Array que Representa al Caracter Especial ")"
      constant parentesis_2                : caracter  := (
          "000000000000",
          "000011000000",
          "000001100000",
          "000000110000",
          "000000011000",
          "000000011000",
          "000000011000",
          "000000011000",
          "000000110000",
          "000001100000",
          "000011000000",
          "000000000000"); 

      --Array que Representa al Caracter Especial "|"
      constant pleca                       : caracter  := (
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111",
          "111111111111");

      --Array que Representa los Registros del Pipeline "/"
      constant pipeline_reg                : caracter  := (
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111",
          "111110001111");       

    --Declaracion de la Funcion de Animacion de la Interfaz Grafica
    function Interface_Animation(Interfaz : screen; X_Position : integer; Y_Position: integer; Hazard_Cases : std_logic_vector(4 downto 0);  Hazard_Registers : STD_LOGIC_VECTOR(15  downto 0); IF_In : STD_LOGIC_VECTOR(6 downto 0); ID_In : STD_LOGIC_VECTOR(7 downto 0); EX_In : STD_LOGIC_VECTOR(7 downto 0); MEM_In : STD_LOGIC_VECTOR(7 downto 0); WB_In : STD_LOGIC_VECTOR(7 downto 0); MemI_In : STD_LOGIC_VECTOR(31 downto 0); MemD_In : STD_LOGIC_VECTOR(31 downto 0); Reg_In1 : STD_LOGIC_VECTOR(31 downto 0); Reg_In2 : STD_LOGIC_VECTOR(31 downto 0); Count_V: integer; Count_H : integer) return Inter_Anim;

    --Declaracion de la Funcion de Impresion de Caracteres en Pantalla
    function Charac_Print(X_Position : integer; Y_Position: integer; Count_V: integer; Count_H : integer; Charact : character; Change_Color : std_logic_vector(1 downto 0); Video_Enable : std_logic) return vga_color;

end Interfaz_PKG;


package body Interfaz_PKG is

    --Funcion de Animacion de la Interfaz Grafica
    function Interface_Animation(Interfaz : screen; X_Position : integer; Y_Position: integer; Hazard_Cases : std_logic_vector(4 downto 0);  Hazard_Registers : STD_LOGIC_VECTOR(15  downto 0); IF_In : STD_LOGIC_VECTOR(6 downto 0); ID_In : STD_LOGIC_VECTOR(7 downto 0); EX_In : STD_LOGIC_VECTOR(7 downto 0); MEM_In : STD_LOGIC_VECTOR(7 downto 0); WB_In : STD_LOGIC_VECTOR(7 downto 0); MemI_In : STD_LOGIC_VECTOR(31 downto 0); MemD_In : STD_LOGIC_VECTOR(31 downto 0); Reg_In1 : STD_LOGIC_VECTOR(31 downto 0); Reg_In2 : STD_LOGIC_VECTOR(31 downto 0); Count_V: integer; Count_H : integer) return Inter_Anim is
    
    --Constantes
    constant Bubble             : string(1 to 3):="NOP";                          --Constante Utilizada para Indicar la Presencia de Burbujas en el Pipeline    
    --Variables
    variable Str                : string(1 to 53);                                --Variable donde se Almacena una Fila de Caracteres de la Interfaz Grafica
    variable Charac             : character;                                      --Variable donde se Almacena el Caracter que se Desea Imprimir en Pantalla    
    variable Change_Color       : std_logic_vector(1 downto 0):="00";             --Cambio de Color: "00" = Colores Normales, "01" = Invertir Colores, "11" = Solo Color Azul
    variable Orig_Column        : integer range 0 to 53;                          --Columna Origen: Indica una Columna de la Interfaz Grafica, desde la cual se Empezara un Conteo de Columnas
    variable Plus               : integer range 0 to 12;                          --El Valor de "Plus" es Sumado a "Orig_Column", para Acceder a Otras Columnas
    variable Mem_Reg            : std_logic_vector(31 downto 0);                  --Variable para Almacenar Informacion Proveniente de la Memoria de Instrucciones, Memoria de Datos o Registros
    variable Pipeline           : std_logic_vector(5 downto 0);                   --Variable Utilizada para Almacenar la Direccion de la Instruccion que se Encuentra en cada Etapa (IF, ID, EX, MEM, WB)   
    variable Hex_Dec_Conv       : std_logic:='0';                                 --Variable Utilizada para Seleccionar el tipo de Conversion: '0' = De Binario a Hexadecimal, '1' = De Binario a Decimal
    variable Hex                : std_logic_vector(3 downto 0);                   --Variable Donde se Almacena un Valor Hexadecimal
    ---------------------------------------------------------
    variable Count_H_Aux        : integer range 0 to 53;                          --Variable para el Conteo Secundario de Posiciones, desde la Posicion mas Significativa, hasta la Menos Significativa 
    ---------------------------------------------------------
    variable BCD_Vector         : std_logic_vector(19 downto 0):=(others => '0'); --Variable Utilizada para el Calculo de Binario a Decimal
    ---------------------------------------------------------
    variable Hazard             : String(1 to 19);                                 --Variable Utilizada para Indicar el Tipo de Riesgo
    variable Hz_IDEX_Reg_Rs1    : integer range 0 to 16;
    variable Hz_IDEX_Reg_Rs2    : integer range 0 to 16;                          --Variables Utilizadas para Determinar los Registros en los que Ocurren los Riesgos
    variable Hz_EXMEM_Reg_Rs2   : integer range 0 to 16;
    variable Hz_IDEX_Reg_Rd     : integer range 0 to 16;
    --------------------------------------------------------
    variable funct_return       : Inter_Anim;                                     --Variable Tipo interface_character, para Retorno de la Funcion
    
    begin          

        --Seleccionar una Fila de la Interfaz
        Str     := Interfaz(Count_V);
        
        --Seleccionar un Caracter de la Fila Seleccionada
        Charac  := Str(Count_H + 1);
          
        --Convertir y Almacenar como Entero, la Direccion del Registro Fuente 1
        --Procedente del Registro de Segmentacion IDEX
        Hz_IDEX_Reg_Rs1  := conv_integer(Hazard_Registers(3 downto 0));
        
        --Convertir y Almacenar como Entero, la Direccion del Registro Fuente 2
        --Procedente del Registro de Segmentacion IDEX
        Hz_IDEX_Reg_Rs2  := conv_integer(Hazard_Registers(7 downto 4));

        --Convertir y Almacenar como Entero, la Direccion del Registro Destino
        --Procedente del Registro de Segmentacion IDEX
        Hz_IDEX_Reg_Rd   := conv_integer(Hazard_Registers(11 downto 8));

        --Convertir y Almacenar como Entero, la Direccion del Registro Fuente 2
        --Procedente del Registro de Segmentacion EXMEM
        Hz_EXMEM_Reg_Rs2 := conv_integer(Hazard_Registers(15 downto 12));
        
        -------------------------------------------------------------------
        ------      Caracteres Dinamicos de la Interfaz Grafica      ------
        ------      '^', '~', '&', '$', '%', '#', '=', '+', '>'      ------ 
        ------------------------------------------------------------------- 
        
        --Etapa IF sin Inicializar
        if (Charac = '&' and IF_In(6) = '0')then
    
            --Sustituir el Caracter '&', por '-'
            Charac := '-';   
                
        --Etapa ID sin Inicializar, o se Detecta la Presencia de una Burbuja 
        elsif (Charac = '$' and (ID_In(6) = '0' or ID_In(7) = '1')) then
                
            --Sustituir el Caracter '$', por '-',
            if ID_In(6) = '0' then
                Charac := '-'; 
            
            --Sustituir el Caracter '$', por los Caracteres de Bubble (NOP)
            else
                Charac := Bubble(Count_H - 14);
            end if;
        
        --Etapa EX sin Inicializar, o se Detecta la Presencia de una Burbuja         
        elsif (Charac = '%' and (EX_In(6) = '0' or EX_In(7) = '1')) then
                
            --Sustituir el Caracter '%', por '-'
            if EX_In(6) = '0' then
                Charac := '-'; 
            
            --Sustituir el Caracter '%', por los Caracteres de Bubble (NOP)
            else
                Charac := Bubble(Count_H - 24);
            end if;
                    
        --Etapa MEM sin Inicializar o se Detecta la Presencia de una Burbuja
        elsif (Charac = '#' and (MEM_In(6) = '0' or MEM_In(7) = '1')) then
                
            --Sustituir el Caracter '#', por '-'
            if MEM_In(6) = '0' then
                Charac := '-'; 
            
            --Sustituir el Caracter '#', por los Caracteres de Bubble (NOP)
            else
                Charac := Bubble(Count_H - 34);
            end if; 
                    
        --Etapa WB sin Inicializar o se Detecta la Presencia de una Burbuja
        elsif (Charac = '+' and (WB_In(6) = '0' or WB_In(7) = '1')) then
                
            --Sustituir el Caracter '+', por '-'
            if WB_In(6) = '0' then
                Charac := '-'; 
            
            --Sustituir el Caracter '+', por los Caracteres de Bubble (NOP)
            else
                Charac := Bubble(Count_H - 44);
            end if;  
                       
        --Sustituir Cualquiera de los Caracteres '^', '~', '&', '$', '%', '#', '+', '<', '>', por los Datos Correspondientes Proporcionados por el Microprocesador
        elsif Charac = '^' or Charac = '~' or Charac = '&' or Charac = '$' or Charac = '%' or Charac = '#' or Charac = '+' or Charac = '<' or Charac = '>'then				
            case Charac is
                
                --Memoria de Instrucciones
                when '^' =>                     
                    
                    Orig_Column	 := 11;                    --Columna Origen 11
                    Plus       	 := 7;                     --Numero de Columnas Adicionales a las que se Desea Acceder           
                    Mem_Reg      := MemI_In;               --Almacenar en Mem_Reg, la Instruccion Proporcionada por MemI_In                          
                    Hex_Dec_Conv := '0';                   --Convertir a Hexadecimal
    
                --Memoria de Datos
                when '~' =>    

                    Orig_Column  := 38;                    --Columna Origen 38
                    Plus       	 := 7;                     --Numero de Columnas Adicionales a las que se Desea Acceder  
                    Mem_Reg      := MemD_In;               --Almacenar en Mem_Reg, el Dato Proporcionado por MemD_In  
                    Hex_Dec_Conv := '0';                   --Convertir a Hexadecimal
    
                --Instruction Fetch (IF)
                when '&' =>    
                    
                    Orig_Column  := 6;                     --Columna Origen 6
                    Plus       	 := 2;                     --Numero de Columnas Adicionales a las que se Desea Acceder 
                    Pipeline     := IF_In(5 downto 0);     --Almacenar en Pipeline, la Direccion de la Instruccion que se Entrada en IF
                    Hex_Dec_Conv := '1';                   --Convertir a Decimal
    
                --Instruction Decode (ID)
                when '$' =>    
                    
                    Orig_Column  := 16;                    --Columna Origen 16
                    Plus       	 := 2;                     --Numero de Columnas Adicionales a las que se Desea Acceder       
                    Pipeline	 := ID_In(5 downto 0);    --Almacenar en Pipeline, la Direccion de la Instruccion que se Entrada en ID
                    Hex_Dec_Conv := '1';                   --Convertir a Decimal
    
                --Execute (Ex)
                when '%' =>    
                    
                    Orig_Column  := 26;                    --Columna Origen 26
                    Plus       	 := 2;                     --Numero de Columnas Adicionales a las que se Desea Acceder 
                    Pipeline     := EX_In(5 downto 0);     --Almacenar en Pipeline, la Direccion de la Instruccion que se Entrada en EX
                    Hex_Dec_Conv := '1';                   --Convertir a Decimal
    
                --Memory (MEM)
                when '#' =>    
                    
                    Orig_Column  := 36;                    --Columna Origen 36
                    Plus       	 := 2;                     --Numero de Columnas Adicionales a las que se Desea Acceder 
                    Pipeline     := MEM_In(5 downto 0);    --Almacenar en Pipeline, la Direccion de la Instruccion que se Entrada en MEM
                    Hex_Dec_Conv := '1';                   --Convertir a Decimal
    
                --Write Back (WB)
                when '+' =>    
                    
                    Orig_Column  := 46;                    --Columna Origen 46
                    Plus       	 := 2;                     --Numero de Columnas Adicionales a las que se Desea Acceder 
                    Pipeline     := WB_In(5 downto 0);     --Almacenar en Pipeline, la Direccion de la Instruccion que se Entrada en WB
                    Hex_Dec_Conv := '1';                   --Convertir a Decimal
                                        
                --Registros de la Izquierda (0 - 7)
                when '<' =>    
                                    
                    Orig_Column  := 11;                    --Columna Origen 11
                    Plus       	 := 7;                     --Numero de Columnas Adicionales a las que se Desea Acceder  
                    Mem_Reg      := Reg_In1;               --Almacenar en Mem_Reg, el Dato Proporcionado por Reg_In1
                    Hex_Dec_Conv := '0';                   --Convertir a Hexadecimal                                 
                                                              
                --Registros de la Derecha (08 - 15)
                when '>' =>                                  
                    	
                    Orig_Column  := 38;                    --Columna Origen 38
                    Plus       	 := 7;                     --Numero de Columnas Adicionales a las que se Desea Acceder               
                    Mem_Reg      := Reg_In2;               --Almacenar en Mem_Reg, el Dato Proporcionado por Reg_In2
                    Hex_Dec_Conv := '0';                   --Convertir a Hexadecimal    
                                                                                                
                when others =>
            end case;
            
            --Conteo de las 4 Posiciones Menos Significativas, ejemplo: Memoria de Instrucciones => 00. ----_^^^^ 
            if (Count_H + 1) >= Orig_Column and (Count_H + 1) < Orig_Column + 4 then                
                Count_H_Aux := (Orig_Column + Plus) - (Count_H + 1);
            
            --Conteo de las 4 Posiciones  Mas  Significativas, ejemplo: Memoria de Instrucciones => 00. ^^^^_---- 
            elsif (Count_H + 1) >= (Orig_Column + 5) and (Count_H + 1) < (Orig_Column + 9) then
                Count_H_Aux := (Orig_Column + Plus + 1) - (Count_H + 1);
            end if;
                      
            --Conversor Hexadecimal y Decimal
            case Hex_Dec_Conv is
                
                --Convertir a Decimal
                when '1' =>
                
                    --BCD (Binary Coded Decimal)
                    BCD_vector(10 downto 3) := "00" & Pipeline; 
                                  
                    for ij in 0 to 4 loop
                        
                        if BCD_Vector(11 downto 8) > 4 then
                            BCD_Vector(11 downto 8) := BCD_Vector(11 downto 8) + 3;
                        end if;
                        
                        if BCD_Vector(15 downto 12) > 4 then
                            BCD_Vector(15 downto 12) := BCD_Vector(15 downto 12) + 3;
                        end if;
                        
                        BCD_Vector(17 downto 1) := BCD_Vector(16 downto 0);
                    end loop;
                    
                    --Representar los Valores Decimales como valores Hexadecimales del 0 al 9
                    Hex := BCD_Vector((Count_H_Aux * 4) + 11 downto (Count_H_Aux * 4) + 8);                     
                                   
                --Convertir a Hexadecimal
                when others =>                
                    
                    Hex := Mem_Reg(((Count_H_Aux + 1) * 4) - 1 downto ((Count_H_Aux + 1) * 4) - 4);
            end case;            
            
            --Convertir de Hexadecimal a Caracter
            case Hex is
                when x"0" => 
                    Charac  := '0'; --0
                when x"1" => 
                    Charac  := '1'; --1
                when x"2" => 
                    Charac  := '2'; --2
                when x"3" => 
                    Charac  := '3'; --3
                when x"4" => 
                    Charac  := '4'; --4
                when x"5" => 
                    Charac  := '5'; --5
                when x"6" =>    
                    Charac  := '6'; --6
                when x"7" => 
                    Charac  := '7'; --7
                when x"8" => 
                    Charac  := '8'; --8
                when x"9" => 
                    Charac  := '9'; --9
                when x"A" => 
                    Charac  := 'A'; --A
                when x"B" => 
                    Charac  := 'B'; --B
                when x"C" => 
                    Charac  := 'C'; --C
                when x"D" => 
                    Charac  := 'D'; --D
                when x"E" => 
                    Charac  := 'E'; --E
                when x"F" => 
                    Charac  := 'F'; --F
                when others => 
            end case;
        end if;
        
        ----------------------------------------------------------------------------
        ---------     Riesgos: Invertir Colores en el Titulo de la Etapa   ---------
        ---------     de Segmentacion y en los Registros de Segmentacion   ---------
        ----------------------------------------------------------------------------

        --Todos los Registros de Segmentacion (IFID, IDEX, EXMEM, MEMWB)
        if Charac = '/' then
            Change_Color := "11";    --Solo Color Azul
        end if;

        --Caso 0: Riesgos de Datos en la Etapa de Segmentacion EX
        --Anticipacion en el Registro de Pipeline EXMEM
        if (Hazard_Cases(0) = '1') then
        
            --Titulo de Etapa de Segmentacion "EX"
            if (Count_H = 25 or Count_H = 26 or Count_H = 27) and Count_V = 20 then 
                Change_Color := "01";    --Invertir Colores
            end if;  
            
            --Registro de Pipeline EXMEM
            if (Count_H = 31) and (Charac = '/') then                       
                Change_Color := "00";    --No Invertir Colores
            end if;              

             --Hz_IDEX_Reg_Rs1 es uno de los Registros de la Izquierda (0 - 7)
            if (Count_V - 30) = Hz_IDEX_Reg_Rs1 and Hz_IDEX_Reg_Rs1 < 8 and (Count_H + 1) < 26 and Hz_IDEX_Reg_Rs1 /= 0 then                      
                Change_Color := "01";    --Invertir Colores
                    
            --Hz_IDEX_Reg_Rs1 es uno de los Registros de la Derecha (08 - 15)
            elsif ((Count_V - 22) = Hz_IDEX_Reg_Rs1 and Count_V > 29) and Hz_IDEX_Reg_Rs1 < 16 and (Count_H + 1) > 28 then     
                Change_Color := "01";    --Invertir Colores
            end if;
 
              --Hz_IDEX_Reg_Rs2 es uno de los Registros de la Izquierda (0 - 7)
            if (Count_V - 30) = Hz_IDEX_Reg_Rs2 and Hz_IDEX_Reg_Rs2 < 8 and (Count_H + 1) < 26 and Hz_IDEX_Reg_Rs2 /= 0 then                      
                Change_Color := "01";    --Invertir Colores
                    
            --Hz_IDEX_Reg_Rs2 es uno de los Registros de la Derecha (08 - 15)
            elsif ((Count_V - 22) = Hz_IDEX_Reg_Rs2 and Count_V > 29) and Hz_IDEX_Reg_Rs2 < 16 and (Count_H + 1) > 28 then     
                Change_Color := "01";    --Invertir Colores
            end if; 

        end if;        
 
        --Caso 1: Riesgos de Datos en la Etapa de Segmentacion EX
        --Anticipacion en el Registro de Pipeline MEMWB
        if (Hazard_Cases(1) = '1') then
        
            --Titulo de Etapa de Segmentacion "EX"
            if (Count_H = 25 or Count_H = 26 or Count_H = 27) and Count_V = 20 then
                Change_Color := "01";    --Invertir Colores
            end if;
            
            --Registro de Pipeline MEMWB
            if (Count_H = 41) and Charac = '/' then         
                Change_Color := "00";    --No Invertir Colores
            end if;        

             --Hz_IDEX_Reg_Rs1 es uno de los Registros de la Izquierda (0 - 7)
            if (Count_V - 30) = Hz_IDEX_Reg_Rs1 and Hz_IDEX_Reg_Rs1 < 8 and (Count_H + 1) < 26 and Hz_IDEX_Reg_Rs1 /= 0 then                      
                Change_Color := "01";    --Invertir Colores
                    
            --Hz_IDEX_Reg_Rs1 es uno de los Registros de la Derecha (08 - 15)
            elsif ((Count_V - 22) = Hz_IDEX_Reg_Rs1 and Count_V > 29) and Hz_IDEX_Reg_Rs1 < 16 and (Count_H + 1) > 28 then     
                Change_Color := "01";    --Invertir Colores
            end if;
 
              --Hz_IDEX_Reg_Rs2 es uno de los Registros de la Izquierda (0 - 7)
            if (Count_V - 30) = Hz_IDEX_Reg_Rs2 and Hz_IDEX_Reg_Rs2 < 8 and (Count_H + 1) < 26 and Hz_IDEX_Reg_Rs2 /= 0 then                      
                Change_Color := "01";    --Invertir Colores
                    
            --Hz_IDEX_Reg_Rs2 es uno de los Registros de la Derecha (08 - 15)
            elsif ((Count_V - 22) = Hz_IDEX_Reg_Rs2 and Count_V > 29) and Hz_IDEX_Reg_Rs2 < 16 and (Count_H + 1) > 28 then     
                Change_Color := "01";    --Invertir Colores
            end if; 
        end if;

        --Caso 2: Riesgos de Datos en la Etapa de Segmentacion MEM
        --Anticipacion en el Registro de Pipeline MEMWB   
        if (Hazard_Cases(2) = '1') then
        
            --Titulo de Etapa de Segmentacion "MEM"
            if (Count_H = 35 or Count_H = 36 or Count_H = 37) and Count_V = 20 then  
                Change_Color := "01";    --Invertir Colores
            end if;
            
            --Registro de Pipeline MEMWB
            if (Count_H = 41) and Charac = '/' then         
                Change_Color := "00";    --No Invertir Colores
            end if;
 
             --Hz_EXMEM_Reg_Rs2 es uno de los Registros de la Izquierda (0 - 7)
            if (Count_V - 30) = Hz_EXMEM_Reg_Rs2 and Hz_EXMEM_Reg_Rs2 < 8 and (Count_H + 1) < 26 and Hz_EXMEM_Reg_Rs2 /= 0 then                      
                Change_Color := "01";    --Invertir Colores
                    
            --Hz_EXMEM_Reg_Rs2 es uno de los Registros de la Derecha (08 - 15)
            elsif ((Count_V - 22) = Hz_EXMEM_Reg_Rs2 and Count_V > 29) and Hz_EXMEM_Reg_Rs2 < 16 and (Count_H + 1) > 28 then     
                Change_Color := "01";    --Invertir Colores
            end if;
        end if;            

        --Caso 3: Riesgos de Datos en la Etapa de Segmentacion ID       
        --Estancamiento en el Registro de Segmentacion IFID
        if (Hazard_Cases(3) = '1') then 
        
            --Titulo de Etapa de Segmentacion "ID"
            if (Count_H = 15 or Count_H = 16 or Count_H = 17) and Count_V = 20 then
                Change_Color := "01";    --Invertir Colores
            end if;      

            --Registro de Pipeline IFID, Cuadro Superior
            if (Count_H = 11) and Charac = '/' and Count_V = 21 then  
                Change_Color := "01";    --Invertir Colores
                
            --Registro de Pipeline IFID, Cuadro Medio
            elsif (Count_H = 11) and Charac = '/' and Count_V = 22 then
                Change_Color := "01";    --Solo Color Azul
                    
            --Registro de Pipeline IFID, Cuadro Inferior
            elsif (Count_H = 11) and Charac = '/' and Count_V = 23 then    
                Change_Color := "01";    --Invertir Colores
            end if; 

            --Hz_IDEX_Reg_Rd es uno de los Registros de la Izquierda (0 - 7)
            if (Count_V - 30) = Hz_IDEX_Reg_Rd and Hz_IDEX_Reg_Rd < 8 and (Count_H + 1) < 26 and Hz_IDEX_Reg_Rd /= 0 then                      
                Change_Color := "01";    --Invertir Colores
                    
            --Hz_IDEX_Reg_Rd es uno de los Registros de la Derecha (08 - 15)
            elsif ((Count_V - 22) = Hz_IDEX_Reg_Rd and Count_V > 29) and Hz_IDEX_Reg_Rd < 16 and (Count_H + 1) > 28 then     
                Change_Color := "01";    --Invertir Colores
            end if;        
            
        end if;

        --Caso 4: Riesgos de Control en la Etapa de Segmentacion ID
        --Desecho de Instruccion en el Registro de Pipeline IFID  
        if (Hazard_Cases(4) = '1') then
    
            --Titulo de Etapa de Segmentacion "ID"    
            if (Count_H = 15 or Count_H = 16 or Count_H = 17) and Count_V = 20 then
                Change_Color := "01";    --Invertir Colores
            end if;
            
            --Registro de Pipeline IFID, Cuadro Superior
            if (Count_H = 11) and Charac = '/' and Count_V = 21 then  
                Change_Color := "00";    --No Invertir Colores
                    
            --Registro de Pipeline IFID, Cuadro Medio
            elsif (Count_H = 11) and Charac = '/' and Count_V = 22 then
                Change_Color := "01";    --Invertir Colores
                    
            --Registro de Pipeline IFID, Cuadro Inferior
            elsif (Count_H = 11) and Charac = '/' and Count_V = 23 then    
                Change_Color := "00";    --No Invertir Colores
            end if; 
        end if;

        ---------------------------------------------------------------------------------------------------
        ---------      Indicar en la Interfaz Grafica, el Tipo de Hazard que esta Ocurriendo      ---------
        ---------------------------------------------------------------------------------------------------

        --Indicar el Tipo de Riesgo, en el Segmento de la Interfaz Grafica, donde se Encuentran los Caracteres "\"
        if Charac = '\' then
            
            --Riesgo de Control y Riesgo de Datos Simultaneos
            if (Hazard_Cases(4) = '1' and (Hazard_Cases(0) = '1' or Hazard_Cases(1) = '1' or Hazard_Cases(2) = '1' 
            or Hazard_Cases(3) = '1')) then
                Hazard := "RIESGO DAT. Y CONT.";            
            
            --Riesgos de Datos (Caso 0,1,2 y 3)
            elsif (Hazard_Cases(0) = '1' or Hazard_Cases(1) = '1' or Hazard_Cases(2) = '1' or Hazard_Cases(3) = '1') then
                Hazard := "  RIESGO DE DATOS  ";
                    
            --Riesgos de Control (Caso 4)
            elsif (Hazard_Cases(4) = '1') then
                Hazard := " RIESGO DE CONTROL ";						
                            
            --No Existen Riesgos
            else						
                Hazard := "   NO HAY RIESGOS  ";                               
            end if;
                
            --Seleccionar de "Hazard", el Caracter a Dibujar 
            Charac := Hazard(Count_H + 1 - 18);
        end if; 
 
        -----------------------------------------------------------------------
        ---------        Retornos de la Funcion Interface_Proc        ---------
        -----------------------------------------------------------------------      
           
        --Retornar el Caracter a Dibujar
        funct_return.Charac       := Charac;
        
        --Retornar el Cambio de Color Seleccionado
        funct_return.Change_Color := Change_color;
            
        return funct_return;
    end Interface_Animation;
    
    --Funcion de Impresion de Caracteres en Pantalla
    function Charac_Print(X_Position : integer; Y_Position: integer; Count_V: integer; Count_H : integer; Charact : character; Change_Color : std_logic_vector(1 downto 0); Video_Enable : std_logic) return VGA_Color is
    variable Row_Select       : integer range 0 to 11;      --Contador de Filas para el Array
    variable Column_Select    : integer range 0 to 11;      --Contador de Columnas para el Array
    variable Row_Array        : std_logic_vector(0 to 11);  --Variable donde se Almacena la Fila del Array Seleccionada
    variable No_Array         : std_logic;                  --Variable donde se Indica, si se Imprimira Algun Caracter o No
    variable Array_Element    : std_logic;                  --Variable que Indica, si el Elemento del Array Seleccionado, es un '1' o un '0'
    variable Color            : vga_color;                  --Variable de Retorno
    begin
        if Video_Enable = '1' then
        
            --Seleccion del elemento dentro del array a dibujar
            if ((Y_Position >= (Count_V * 12)) and (Y_Position < (Count_V * 12) + 12)) and ((X_Position >= (Count_H * 12)) and (X_Position < (Count_H * 12) + 12)) then
                    row_select    := Y_Position - (Count_V * 12);    --Determinar la Fila, donde se encuentra el elemento del Array a Dibujar
                    column_select := X_Position - (Count_H * 12);    --Determinar la Columna, donde se encuentra el elemento del Array a Dibujar
                    No_Array  := '0';
            else
                    row_select    := 0;
                    column_select := 0;
                    No_Array  := '1';
            end if;

            --Seleccionar una Fila del Array que Representa el Caracter Seleccionado
            case Charact is
                when 'A'    =>
                    Row_Array := A(Row_Select);
                when 'B'    =>
                    Row_Array := B(Row_Select);
                when 'C'    =>
                    Row_Array := C(Row_Select);
                when 'D'    =>
                    Row_Array := D(Row_Select);
                when 'E'    =>
                    Row_Array := E(Row_Select);
                when 'F'    =>
                    Row_Array := F(Row_Select);
                when 'G'    =>
                    Row_Array := G(Row_Select);
                when 'H'    =>
                    Row_Array := H(Row_Select);
                when 'I'    =>
                    Row_Array := I(Row_Select);
                when 'J'    =>
                    Row_Array := J(Row_Select);  
                when 'K'    =>
                    Row_Array := K(Row_Select);
                when 'L'    =>
                    Row_Array := L(Row_Select);
                when 'M'    =>
                    Row_Array := M(Row_Select);
                when 'N'    =>
                    Row_Array := N(Row_Select);
                when 'O'    =>
                    Row_Array := O(Row_Select);
                when 'P'    =>
                    Row_Array := P(Row_Select);
                when 'Q'    =>
                    Row_Array := Q(Row_Select);
                when 'R'    =>
                    Row_Array := R(Row_Select);
                when 'S'    =>
                    Row_Array := S(Row_Select);
                when 'T'    =>
                    Row_Array := T(Row_Select);
                when 'U'    =>
                    Row_Array := U(Row_Select);
                when 'V'    =>
                    Row_Array := V(Row_Select);
                when 'W'    =>
                    Row_Array := W(Row_Select);
                when 'X'    =>
                    Row_Array := X(Row_Select);               
                when 'Y'    =>
                    Row_Array := Y(Row_Select);
                when 'Z'    =>
                    Row_Array := Z(Row_Select);
                when '0'    =>
                    Row_Array := cero(Row_Select);
                when '1'    =>
                    Row_Array := uno(Row_Select);
                when '2'    =>
                    Row_Array := dos(Row_Select);
                when '3'    =>
                    Row_Array := tres(Row_Select);
                when '4'    =>
                    Row_Array := cuatro(Row_Select);
                when '5'    =>
                    Row_Array := cinco(Row_Select);
                when '6'    =>
                    Row_Array := seis(Row_Select);
                when '7'    =>
                    Row_Array := siete(Row_Select);
                when '8'    =>
                    Row_Array := ocho(Row_Select);
                when '9'    =>
                    Row_Array := nueve(Row_Select);
                when '.'    =>
                    Row_Array := punto(Row_Select);
                when '-'    => 
                    Row_Array := guion(Row_Select);
                when '_'    =>
                    Row_Array := guion_bajo(Row_Select);
                when ' '    =>
                    Row_Array := espacio(Row_Select);           
                when '('    =>
                    Row_Array := parentesis_1(Row_Select);      
                when ')'    =>
                    Row_Array := parentesis_2(Row_Select);
                when '|'    =>
                    Row_Array := pleca(Row_Select);
                when '/'    =>
                    Row_Array := pipeline_reg(Row_Select);
                when others =>
                    No_Array  := '1';
                end case;

            --Seleccionar un elemento, de la fila seleccionada anteriormente
            Array_Element := Row_Array(Column_Select);

            --Se imprimira un caracter
            if No_Array = '0' then
            
                --El elemento seleccionado del array, es un '1'
                if (Array_Element = '1' xor Change_Color(0) = '1') or Change_Color(1) = '1'  then
                    
                    --Imprimir en pantalla, el color Azul
                    Color.Red   := "00000";
                    Color.Green := "000000";
                    Color.Blue  := "11111"; 
                
                --El elemento seleccionado del array, es un '0'
                else
                    
                    --Imprimir en pantalla, el color blanco
                    Color.Red   := "11111";
                    Color.Green := "110000";
                    Color.Blue  := "11111";
                end if;
            
            --No se imprimira un caracter
            else
                --Imprimir en pantalla, el color verde
                Color.Red   := "00000";
                Color.Green := "110000";
                Color.Blue  := "00000";
            end if;
            
        --Video_Enable = '0' 
        else
            --cuando se este fuera del area de video activo, es decir
            --Video_Enable = '0', se debe mandar a imprimir algo, de lo
            --contrario la pantalla aparece en negro
            Color.Red   := "00000";
            Color.Green := "000000";
            Color.Blue  := "00000";
        end if;
            
        return Color;
    end Charac_Print;

end Interfaz_PKG;
