----------------------------------------------------------------------------  
--  Nombre del Proyecto    : UNIPROC 2021                                   
--  Institucion            : Universidad Nacional de Ingenieria                 
--  Facultad               : FEC  
--  Carrera                : Ing. Electronica                                              
--  Pais                   : Nicaragua                                          
--  Nombre del Autor       : Jason Ortiz                                
--  Nombre del Modulo      : Registro_de_Segmentacion_IFID
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "Registro de Segmentacion IFID", su
--      trabajo consiste en tranferir a la etapa de segmentacion ID, la
--      informacion procendete de la etapa de segmentacion IF
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Registro_de_Segmentacion_IFID is
    Port ( -----------------------------------------------
           -------       Puertos de Entrada        -------
           -----------------------------------------------
           CLK          : in  STD_LOGIC;
           RST          : in  STD_LOGIC;
           IFID_E       : in  STD_LOGIC;
           IFID_L_Instr : in  STD_LOGIC_VECTOR(31 downto 0);
           IFID_L_PC    : in  STD_LOGIC_VECTOR(6  downto 0);
           Branch_Taken : in  STD_LOGIC;           
           -----------------------------------------------
           ------     Puerto Especial de Entrada    ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           IFID_L_NOP   : in  STD_LOGIC;
           -----------------------------------------------
           -------         Puerto de Salida        -------
           -----------------------------------------------
           IFID_H_Instr : out STD_LOGIC_VECTOR(31 downto 0);
           IFID_H_PC    : out STD_LOGIC_VECTOR(6  downto 0);
           -----------------------------------------------
           ------     Puerto Especial de Salida     ------
           ------        - Interfaz Grafica -       ------
           -----------------------------------------------
           IFID_H_NOP   : out STD_LOGIC);
end Registro_de_Segmentacion_IFID;

architecture Behavioral of Registro_de_Segmentacion_IFID is

    --Declaracion de Signals
    signal IFID_Instr_Aux : std_logic_vector(31 downto 0);
    signal IFID_PC_Aux    : std_logic_vector(6  downto 0);

    --Signal de uso especial para la interfaz grafica        
    signal IFID_NOP_Aux   : std_logic;

begin

    process(CLK, RST)
    begin    
        --Reset
        if RST = '1' then
            IFID_Instr_Aux  <= (others => '0');
            IFID_PC_Aux     <= (others => '0');
            IFID_NOP_Aux    <= '0';

        --Escribir Informacion en la Parte 
        --Baja de los Ciclos de Reloj
        elsif (falling_edge(CLK)) then
            if IFID_E = '0' then
    
                --Burbuja NOP
                IFID_NOP_Aux   <= IFID_L_NOP;
                
                --No se lleva a Cabo Ningun Salto
                if Branch_Taken = '0' then
                    IFID_Instr_Aux <= IFID_L_Instr;
                    IFID_PC_Aux    <= IFID_L_PC;
                
                --Se lleva a Cabo un Salto
                else
                    --Introducir operacion NOP
                    --expresada como instruccion:
                    --add x0, x0, x0
                    IFID_Instr_Aux <= x"00000033";
                end if;
                
            end if;  
        end if;                      

        --Reset
        if RST = '1' then            
            IFID_H_Instr   <= (others => '0'); 
            IFID_H_PC      <= (others => '0');   
            IFID_H_NOP     <= '0';
  
        --Leer Informacion en la Parte 
        --Alta de los Ciclos de Reloj
        elsif (rising_edge(CLK)) then
            IFID_H_Instr   <= IFID_Instr_Aux;
            IFID_H_PC      <= IFID_PC_Aux;
            
            IFID_H_NOP     <= IFID_NOP_Aux;
        end if;  
    end process; 


end Behavioral;
