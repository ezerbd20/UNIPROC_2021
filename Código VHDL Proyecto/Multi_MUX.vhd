------------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021                                    
--  Institucion            : Universidad Nacional de Ingenieria                 
--  Facultad               : FEC  
--  Carrera                : Ing. Electronica                                              
--  Pais                   : Nicaragua                                          
--  Nombre del Autor       : Jason Ortiz                                
--  Nombre del Modulo      : Multi_MUX
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "Multi MUX". Este modulo alberga 2
--      multiplexores 2 a 1, que trabajan de forma independiente uno del otro
------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Multi_MUX is
    Port ( --------------------------------------------
           ------       Puertos de Entrada       ------
           --------------------------------------------
           In_1A  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_2A  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_1B  : in  STD_LOGIC_VECTOR(31 downto 0);
           In_2B  : in  STD_LOGIC_VECTOR(31 downto 0);
           Sel1   : in  STD_LOGIC;
           Sel2   : in  STD_LOGIC;
           --------------------------------------------
           ------       Puertos de Salida        ------
           --------------------------------------------
           Out_1C : out STD_LOGIC_VECTOR(31 downto 0);
           Out_2C : out STD_LOGIC_VECTOR(31 downto 0));
end Multi_MUX;

architecture Behavioral of Multi_MUX is

begin

    --MUX 1
    Out_1C <= In_1A when Sel1 = '1' else In_1B;

    --MUX 2
    Out_2C <= In_2A when Sel2 = '1' else In_2B;

end Behavioral;
