----------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Unidad_de_Deteccion_de_Riesgos
--  Descripcion del Modulo : 
--      En este modulo se encuentra la "Unidad de Deteccion de Riesgos"
--      o "UDR", su trabajo consiste en detectar en la etapa "ID", si 
--      hay algun "Riesgo de Datos"  relacionado con la instruccion "lw",     
--      de ser afirmativo el caso, aplica la tecnica de "Estancamiento",        
--      al detener las instrucciones que se encuentran en las etapas "IF"  
--      e "ID", al mismo tiempo que manda a introducir una burbuja en la
--      etapa "EX"
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Unidad_de_Deteccion_de_Riesgos is
  Port ( --------------------------------------------------
         ---------       Puertos de Entrada       ---------
         --------------------------------------------------
         IDEX_Ctr_MEM_R : in  STD_LOGIC;
         ID_Ctr_MEM_W   : in  STD_LOGIC;
         IFID_Reg_Rs1   : in  STD_LOGIC_VECTOR(3 downto 0);
         IFID_Reg_Rs2   : in  STD_LOGIC_VECTOR(3 downto 0);
         IDEX_Reg_Rd    : in  STD_LOGIC_VECTOR(3 downto 0);
         --------------------------------------------------
         ---------       Puertos de Salida        ---------
         --------------------------------------------------
         MUX_Ctr_Sel    : out STD_LOGIC;
         PC_E           : out STD_LOGIC;
         IFID_E         : out STD_LOGIC
   );
end Unidad_de_Deteccion_de_Riesgos;

architecture Behavioral of Unidad_de_Deteccion_de_Riesgos is

begin
    
    process (IDEX_Ctr_MEM_R, ID_Ctr_MEM_W, IFID_Reg_Rs1, IFID_Reg_Rs2, IDEX_Reg_Rd)
    begin
        --Deteccion de Riesgos de Datos asociados a la instruccion "lw"
        if (IDEX_Ctr_MEM_R = '1') and (ID_Ctr_MEM_W = '0') and (IDEX_Reg_Rd /= x"0") and (IDEX_Reg_Rd = IFID_Reg_Rs1 or IDEX_Reg_Rd = IFID_Reg_Rs2) then          
            --Introducir una burbuja, reemplazando  
            --las senales de la "Unidad de Control",   
            --por ceros 
            MUX_Ctr_Sel <= '0';   
            --Impedir que el PC se actualice
            PC_E        <= '1';
            --No actualizar la informacion que se encuentra
            --almacenda en el Registro de Segmentacion "IFID"
            IFID_E      <= '1';     
        
        --No se Detectan Riesgos de Datos asociados a la instruccion "lw"
        else
            --No introducir burbuja
            MUX_Ctr_Sel <= '1'; 
            --Permitir que el Contador de Programa (PC)
            --se actualize con normalidad
            PC_E        <= '0';
            --Permitir que el Registro de Segmetacion "IFID"
            --se actualize con normalidad
            IFID_E      <= '0';
        end if; 
    end process;   
    
end Behavioral;
