--------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Contador_de_Programa
--  Descripcion del Modulo : 
--      En este modulo se encuentra el registro "Contador de Programa"
--      ("PC" o "Program Counter"), su trabajo consiste en proporcinar a 
--      la memoria de instrucciones, la direccion de memoria, donde se 
--      encuentra almacenada la instruccion a procesar.
--------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Contador_de_Programa is
    Port ( -----------------------------------------------
           -------       Puertos de Entrada        -------
           -----------------------------------------------
           CLK      : in  STD_LOGIC;
           RST      : in  STD_LOGIC;
           PC_E     : in  STD_LOGIC;
           PC_In    : in  STD_LOGIC_VECTOR (6 downto 0);
           -----------------------------------------------
           -------         Puerto de Salida        -------
           -----------------------------------------------
           PC_Out   : out STD_LOGIC_VECTOR (6 downto 0));
end Contador_de_Programa;

architecture Behavioral of Contador_de_Programa is

    signal Last_Address : std_logic_vector(6 downto 0);

begin

    process(CLK, RST)
    begin
        if (RST = '1') then   
            --Desactivar Bit de Conteo
            --Establecer Direccion de Instruccion en cero 
            PC_Out      <= '0' & "000000";
        
        elsif (rising_edge(CLK)) then
                     
            --Verificar que sea posible actualizar el PC
            if (PC_E = '0') then        
                
                --Verificar Bit de Conteo Inactivo
                if PC_In(6) = '0' then
                    --Activar Bit de Conteo
                    --Establecer Direccion de Instruccion en cero 
                    PC_Out      <= '1' & "000000";
                
                --Actualizar el Contador de Programa (PC)
                else
                    PC_Out       <= PC_In;
                    Last_Address <= PC_In;                    
                end if;    
                
            --No actualizar el PC
            else
                PC_Out      <= Last_Address;    
            end if;
        end if;
    end process;

end Behavioral;
