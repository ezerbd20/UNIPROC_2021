--------------------------------------------------------------------- 
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Multi_Debounce
--  Descripcion del Modulo : 
--      En este modulo se encuentra "Multi_Debounce", un modulo cuya   
--      funcion consiste en proveer instancias del modulo "Debounce",  
--      para cada pulsador e interruptor usado en el proyecto
--------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Multi_Debounce is
    Port ( -----------------------------------
           ---     Puertos de Entrada      ---   
           -----------------------------------
           CLK                : in  STD_LOGIC;
           Micro_CLK_Src_Sel  : in  STD_LOGIC;
           RST_VGA_In         : in  STD_LOGIC;
           RST_Micro_In       : in  STD_LOGIC;
           Push_Button_In     : in  STD_LOGIC;
           -----------------------------------
           ---      Puertos de Salida      ---   
           -----------------------------------
           Micro_CLK_Sel      : out STD_LOGIC;
           RST_VGA_Controller : out STD_LOGIC;
           RST_Micro          : out STD_LOGIC;
           Micro_Manual_CLK   : out STD_LOGIC);
end Multi_Debounce;

architecture Behavioral of Multi_Debounce is

    --DECLARACION DE COMPONENTE PARA "Debounce"
    COMPONENT Debounce
        Generic(N               : Integer);
        Port ( CLK              : in  STD_LOGIC; 
               Debounce_In      : in  STD_LOGIC;                    
               Debounce_Out     : out STD_LOGIC
               );  
    END COMPONENT; 

begin

    --INSTANCIACION DE "Debounce"
    Debounce_Micro_CLK_Src_Sel : Debounce 
    GENERIC MAP (N => 12000000) --0.5 segundos
    PORT MAP (        
        CLK                     => CLK,
        Debounce_In             => Micro_CLK_Src_Sel,
        Debounce_Out            => Micro_CLK_Sel
    );    

    --INSTANCIACION DE "Debounce"
    Debounce_VGA_RST : Debounce 
    GENERIC MAP (N => 6000000)  --0.25 segundos
    PORT MAP (
        CLK                     => CLK,
        Debounce_In             => RST_VGA_In,  
        Debounce_Out            => RST_VGA_Controller
    );
    
    --INSTANCIACION DE "Debounce"
    Debounce_Micro_RST : Debounce 
    GENERIC MAP (N => 6000000)  --0.25 segundos
    PORT MAP (
        CLK                     => CLK,
        Debounce_In             => RST_Micro_In,        
        Debounce_Out            => RST_Micro
    );      
    
    --INSTANCIACION DE "Debounce"
    Debounce_Manual_CLK_Micro : Debounce 
    GENERIC MAP (N => 6000000)  --0.25 segundos
    PORT MAP (
        CLK                     => CLK,
        Debounce_In             => Push_Button_In,
        Debounce_Out            => Micro_Manual_CLK
    ); 

end Behavioral;
