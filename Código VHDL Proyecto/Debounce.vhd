----------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Debounce
--  Descripcion del Modulo : 
--      Modulo encargado de eliminar los rebotes ocasionados por los 
--      pulsadores (Push_Button), y los interruptores (Switches)
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Debounce is
    --Puerto Generico
    Generic (N          : Integer);
    --Puertos Normales
    Port ( -------------------------------
           ---   Puertos de Entrada    ---
           -------------------------------
           CLK          : in  STD_LOGIC;
           Debounce_In  : in  STD_LOGIC;           
           -------------------------------
           ---    Puerto de Salida     ---
           -------------------------------
           Debounce_Out : out STD_LOGIC);
end Debounce;

architecture Behavioral of Debounce is

    --Tiempo de debounce
    constant Count_Max : integer := N;

    --Declaracion de la Maquina de Estado
    type State_Type is (Idle_State, Wait_State, Active_State);
    
    --Inicializacion de la Maquina de Estado
    signal State : State_Type:= Idle_State;
    
    --Declaracion de signal de conteo (es posible contar hasta 2 segundos)
    signal count : integer range 0 to 50000001;

begin

    process (State)
    begin
      case State is
        when Idle_State =>
            Debounce_Out <= '0';
        when Wait_State =>
            Debounce_Out <= '0';
        when Active_State =>
            Debounce_Out <= '1';
        end case;
    end process;

    process(CLK)
    begin
        if(CLK'event and CLK = '1') then
            case State is            
                when Idle_State =>
                                
                    if (Debounce_In = '1') then                      
                        State <= Wait_State;
                    else                    
                        State <= Idle_State;
                    end if;
                                       
                when Wait_State =>
                
                    if Count = Count_Max then
                        if (Debounce_In = '1') then                        
                            State     <= Active_State; 
                        else
                            State     <= Idle_State;  
                        end if;                        
                            Count <= 0;  
                    else
                        Count <= Count + 1; 
                    end if; 
                    
                when Active_State =>   
                    if (Debounce_In = '1') then 
                        State     <= Active_State;    
                    else
                        State     <= Idle_State; 
                    end if;                     
            end case;
        end if;
    end process;

end Behavioral;