----------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Microarquitectura
--  Descripcion del Modulo : 
--      Este modulo es el microprocesador UNIPROC 2021, todos los elementos
--      de la microarquitectura del microprocesador son instanciados en este 
--      modulo, asi como modulos especiales necesarios para el correcto 
--      funcionamiento de la interfaz grafica.
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Microarquitectura is
    Port ( -----------------------------------------------------------
           --------             Puertos de Entrada            --------
           --------                - Modulo -                 --------
           -----------------------------------------------------------
           CLK                    : in  STD_LOGIC;
           RST                    : in  STD_LOGIC;
           -----------------------------------------------------------
           --------        Puerto Especial de Entrada         --------
           --------           - Interfaz Grafica -            --------
           -----------------------------------------------------------
           Count_V                : in  integer;
           -----------------------------------------------------------
           --------       Puertos Especiales de Salida        --------
           --------           - Interfaz Grafica -            --------
           -----------------------------------------------------------
           Inter_IF               : out STD_LOGIC_VECTOR(6  downto 0);
           Inter_ID               : out STD_LOGIC_VECTOR(7  downto 0);
           Inter_EX               : out STD_LOGIC_VECTOR(7  downto 0);
           Inter_MEM              : out STD_LOGIC_VECTOR(7  downto 0);
           Inter_WB               : out STD_LOGIC_VECTOR(7  downto 0);
           -----------------------------------------------------------
           Inter_MemI             : out STD_LOGIC_VECTOR(31 downto 0);
           Inter_MemD             : out STD_LOGIC_VECTOR(31 downto 0);
           Inter_Reg_Left         : out STD_LOGIC_VECTOR(31 downto 0);
           Inter_Reg_Right        : out STD_LOGIC_VECTOR(31 downto 0);
           -----------------------------------------------------------
           Inter_Hazard_Cases     : out STD_LOGIC_VECTOR(4  downto 0);
           Inter_Hazard_Registers : out STD_LOGIC_VECTOR(15 downto 0));
end Microarquitectura;

architecture Behavioral of Microarquitectura is     
    
    -------------------------------------------------------------------
    ----------       Declaracion de los Componentes      --------------
    -------------------------------------------------------------------
    
    --DECLARACION DE COMPONENTE PARA "Contador_de_Programa"
    COMPONENT Contador_de_Programa
        Port ( CLK      : in  STD_LOGIC;
               RST      : in  STD_LOGIC;
               PC_E     : in  STD_LOGIC;
               PC_In    : in  STD_LOGIC_VECTOR (6 downto 0);
               PC_Out   : out STD_LOGIC_VECTOR (6 downto 0));
    END COMPONENT;

    --DECLARACION DE COMPONENTE PARA "Unidad_Aritmetico_Logica"
    COMPONENT Unidad_Aritmetico_Logica
        Port ( In_A    : in  STD_LOGIC_VECTOR(31 downto 0);
               In_B    : in  STD_LOGIC_VECTOR(31 downto 0);
               ALU_Ctr : in  STD_LOGIC_VECTOR(3  downto 0);
               ALU_Out : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT;

    --DECLARACION DE COMPONENTE PARA "Unidad_de_Salto_Condicional"
    COMPONENT Unidad_de_Salto_Condicional
        Port ( Ctr_Branch       : in  STD_LOGIC;
               Funct3           : in  STD_LOGIC_VECTOR(2 downto 0);
               Rs1_Data_Read    : in  STD_LOGIC_VECTOR(31 downto 0);
               Rs2_Data_Read    : in  STD_LOGIC_VECTOR(31 downto 0);
               Branch_Taken     : out STD_LOGIC);
    END COMPONENT;
    
    --DECLARACION DE COMPONENTE PARA "Unidad_de_Control"
    COMPONENT Unidad_de_Control
        Port ( Opcode        : in  STD_LOGIC_VECTOR(6 downto 0);
               Control_Out2  : out STD_LOGIC;
               Control_Out1  : out STD_LOGIC_VECTOR(6 downto 0));  
    END COMPONENT;   

    --DECLARACION DE COMPONENTE PARA "Memoria_de_Instrucciones"
    COMPONENT Memoria_de_Instrucciones
        Port ( Address   : in  STD_LOGIC_VECTOR(6  downto 0);
               Count_V   : in  integer;
               Instr_Out : out STD_LOGIC_VECTOR(31 downto 0);
               MemI_Out  : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT;

    --DECLARACION DE COMPONENTE PARA "Banco_de_Registros"
    COMPONENT Banco_de_Registros
        Port ( CLK        : in  STD_LOGIC; 
               RST        : in  STD_LOGIC;              
               Addr1_Read : in  STD_LOGIC_VECTOR(3  downto 0);
               Addr2_Read : in  STD_LOGIC_VECTOR(3  downto 0);
               Reg_Write  : in  STD_LOGIC;
               Addr_Write : in  STD_LOGIC_VECTOR(3  downto 0);
               Data_In    : in  STD_LOGIC_VECTOR(31 downto 0);
               Count_V    : in  integer;
               Data1_Out  : out STD_LOGIC_VECTOR(31 downto 0);
               Data2_Out  : out STD_LOGIC_VECTOR(31 downto 0); 
               Reg_Left   : out STD_LOGIC_VECTOR(31 downto 0);
               Reg_Right  : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT;  
    
    --DECLARACION DE COMPONENTE PARA "Memoria_de_Datos"
    COMPONENT Memoria_de_Datos
        Port ( CLK       : in  std_logic;
               RST       : in  STD_LOGIC;
               Mem_Write : in  STD_LOGIC;
               Mem_Read  : in  STD_LOGIC;
               Address   : in  STD_LOGIC_VECTOR(5  downto 0);
               Data_In   : in  STD_LOGIC_VECTOR(31 downto 0);               
               Count_V   : in  integer;
               Data_Out  : out STD_LOGIC_VECTOR(31 downto 0);
               MemD_Out  : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT;   

    --DECLARACION DE COMPONENTE PARA "MUX_2_a_1"
    COMPONENT MUX_2_a_1
        Generic(N    : Integer);
        Port ( In_A  : in  STD_LOGIC_VECTOR(N - 1 downto 0);
               In_B  : in  STD_LOGIC_VECTOR(N - 1 downto 0);
               Sel   : in  STD_LOGIC;
               Out_C : out STD_LOGIC_VECTOR(N - 1 downto 0));
    END COMPONENT;   
 
     --DECLARACION DE COMPONENTE PARA "Sumador"
    COMPONENT Sumador
        Port ( In_A  : in  STD_LOGIC_VECTOR (6 downto 0);
               In_B  : in  STD_LOGIC_VECTOR (5 downto 0);
               Out_C : out STD_LOGIC_VECTOR (6 downto 0));
    END COMPONENT;     
    
    --DECLARACION DE COMPONENTE PARA "Generador_de_Inmediatos"
    COMPONENT Generador_de_Inmediatos
        Port ( Instr  : in  STD_LOGIC_VECTOR (31 downto 0);
               Inm    : out STD_LOGIC_VECTOR (31 downto 0));        
    END COMPONENT;
    
    --DECLARACION DE COMPONENTE PARA "Controlador_ALU"
    COMPONENT Controlador_ALU
        Port ( ALU_Op  : in  STD_LOGIC_VECTOR (1 downto 0);
               Funct3  : in  STD_LOGIC_VECTOR (2 downto 0);
               Funct7  : in  STD_LOGIC_VECTOR (6 downto 0);
               ALU_Ctr : out STD_LOGIC_VECTOR (3 downto 0));        
    END COMPONENT;    
    
    --DECLARACION DE COMPONENTE PARA "Desplazador_de_1bit_hacia_la_Izquierda"
    COMPONENT Desplazador_de_1bit_hacia_la_Izquierda
        Port ( Inp  : in  STD_LOGIC_VECTOR (31 downto 0);
               Outp : out STD_LOGIC_VECTOR (31 downto 0));     
    END COMPONENT;   

    --DECLARACION DE COMPONENTE PARA "MUX_3_a_1"
    COMPONENT MUX_3_a_1
        Port ( In_A  : in  STD_LOGIC_VECTOR(31 downto 0);
               In_B  : in  STD_LOGIC_VECTOR(31 downto 0);
               In_C  : in  STD_LOGIC_VECTOR(31 downto 0);
               Sel   : in  STD_LOGIC_VECTOR(1  downto 0);
               Out_D : out STD_LOGIC_VECTOR(31 downto 0));     
    END COMPONENT;  
    
    --DECLARACION DE COMPONENTE PARA "Unidad_de_Anticipacion"
    COMPONENT Unidad_de_Anticipacion
        Port ( IDEX_Reg_Rs1      : in  STD_LOGIC_VECTOR(3 downto 0);
               IDEX_Reg_Rs2      : in  STD_LOGIC_VECTOR(3 downto 0);         
               EXMEM_Ctr_WB_W    : in  STD_LOGIC;
               MEMWB_Ctr_WB_W    : in  STD_LOGIC;
               EXMEM_Ctr_MEM_R   : in  STD_LOGIC;
               IDEX_Ctr_MEM_W    : in  STD_LOGIC;
               EXMEM_Reg_Rd      : in  STD_LOGIC_VECTOR(3 downto 0);
               MEMWB_Reg_Rd      : in  STD_LOGIC_VECTOR(3 downto 0);
               MUX_Anti_ALU_A_Sel : out STD_LOGIC_VECTOR(1 downto 0);
               MUX_Anti_ALU_B_Sel : out STD_LOGIC_VECTOR(1 downto 0);
               MUX_Anti_MemD_Sel  : out STD_LOGIC);   
    END COMPONENT;     

    --DECLARACION DE COMPONENTE PARA "Unidad_de_Deteccion_de_Riesgos"
    COMPONENT Unidad_de_Deteccion_de_Riesgos
        Port ( IDEX_Ctr_MEM_R : in  STD_LOGIC;
               ID_Ctr_MEM_W   : in  STD_LOGIC;
               IFID_Reg_Rs1   : in  STD_LOGIC_VECTOR(3 downto 0);
               IFID_Reg_Rs2   : in  STD_LOGIC_VECTOR(3 downto 0);
               IDEX_Reg_Rd    : in  STD_LOGIC_VECTOR(3 downto 0);
               MUX_Ctr_Sel    : out STD_LOGIC;
               PC_E           : out STD_LOGIC;
               IFID_E         : out STD_LOGIC);  
    END COMPONENT; 

    --DECLARACION DE COMPONENTE PARA "Multi_MUX"
    COMPONENT Multi_MUX
        Port ( In_1A  : in  STD_LOGIC_VECTOR(31 downto 0);
               In_2A  : in  STD_LOGIC_VECTOR(31 downto 0);
               In_1B  : in  STD_LOGIC_VECTOR(31 downto 0);
               In_2B  : in  STD_LOGIC_VECTOR(31 downto 0);
               Sel1   : in  STD_LOGIC;
               Sel2   : in  STD_LOGIC;
               Out_1C : out STD_LOGIC_VECTOR(31 downto 0);
               Out_2C : out STD_LOGIC_VECTOR(31 downto 0));
    END COMPONENT; 


    --DECLARACION DE COMPONENTE PARA "Detecction_Grafica_de_Riesgos"
    COMPONENT Deteccion_Grafica_de_Riesgos
        Port ( MUX_Anti_Sel_In_A        : in  STD_LOGIC_VECTOR(1 downto 0);
               MUX_Anti_Sel_In_B        : in  STD_LOGIC_VECTOR(1 downto 0);
               EXMEM_MUX_Anti_MemD_Sel  : in  STD_LOGIC;
               IFID_H_NOP               : in  STD_LOGIC;
               IDEX_Reg_Rs1             : in  STD_LOGIC_VECTOR(3  downto 0);
               IDEX_Reg_Rs2             : in  STD_LOGIC_VECTOR(3  downto 0);
               EXMEM_Reg_Rs2            : in  STD_LOGIC_VECTOR(3  downto 0);
               IDEX_Reg_Rd              : in  STD_LOGIC_VECTOR(3  downto 0);
               Branch_Taken             : in  STD_LOGIC;
               IFID_E                   : in  STD_LOGIC;
               NOP_ID                   : out STD_LOGIC;
               NOP_EX                   : out STD_LOGIC;
               Hazard_Cases             : out STD_LOGIC_VECTOR(4  downto 0);
               Hazard_Registers         : out STD_LOGIC_VECTOR(15 downto 0)); 
    END COMPONENT;

    --DECLARACION DE COMPONENTE PARA "Registro_de_Segmentacion_IFID"
    COMPONENT Registro_de_Segmentacion_IFID
        Port ( CLK          : in  STD_LOGIC;
               RST          : in  STD_LOGIC;
               IFID_E       : in  STD_LOGIC;
               IFID_L_Instr : in  STD_LOGIC_VECTOR(31 downto 0);
               IFID_L_PC    : in  STD_LOGIC_VECTOR(6  downto 0);
               Branch_Taken : in  STD_LOGIC;
               IFID_L_NOP   : in  STD_LOGIC;
               IFID_H_Instr : out STD_LOGIC_VECTOR(31 downto 0);
               IFID_H_PC    : out STD_LOGIC_VECTOR(6  downto 0);
               IFID_H_NOP   : out STD_LOGIC);
    END COMPONENT;

    --DECLARACION DE COMPONENTE PARA "Registro_de_Segmentacion_IDEX"
    COMPONENT Registro_de_Segmentacion_IDEX
        Port ( CLK                  : in  STD_LOGIC;
               RST                  : in  STD_LOGIC;      
               IDEX_L_Ctr_WB        : in  STD_LOGIC_VECTOR(1  downto 0); 
               IDEX_L_Ctr_MEM       : in  STD_LOGIC_VECTOR(1  downto 0);
               IDEX_L_Ctr_EX        : in  STD_LOGIC_VECTOR(2  downto 0);    
               IDEX_L_Funct3        : in  STD_LOGIC_VECTOR(2  downto 0);
               IDEX_L_Funct7        : in  STD_LOGIC_VECTOR(6  downto 0);
               IDEX_L_Data1         : in  STD_LOGIC_VECTOR(31 downto 0);
               IDEX_L_Data2         : in  STD_LOGIC_VECTOR(31 downto 0);           
               IDEX_L_Gen_Inm       : in  STD_LOGIC_VECTOR(31 downto 0);
               IDEX_L_Reg_Rs1       : in  STD_LOGIC_VECTOR(3  downto 0);
               IDEX_L_Reg_Rs2       : in  STD_LOGIC_VECTOR(3  downto 0);
               IDEX_L_Reg_Rd        : in  STD_LOGIC_VECTOR(3  downto 0);  
               IDEX_L_PC            : in  STD_LOGIC_VECTOR(6  downto 0);
               IDEX_L_NOP           : in  STD_LOGIC;  
               IDEX_H_Ctr_WB        : out STD_LOGIC_VECTOR(1  downto 0);
               IDEX_H_Ctr_MEM       : out STD_LOGIC_VECTOR(1  downto 0);
               IDEX_H_ALU_Op        : out STD_LOGIC_VECTOR(1  downto 0);
               IDEX_H_MUX_ALU_Sel   : out STD_LOGIC;
               IDEX_H_Funct3        : out STD_LOGIC_VECTOR(2  downto 0);
               IDEX_H_Funct7        : out STD_LOGIC_VECTOR(6  downto 0);
               IDEX_H_Data1         : out STD_LOGIC_VECTOR(31 downto 0);
               IDEX_H_Data2         : out STD_LOGIC_VECTOR(31 downto 0);
               IDEX_H_Gen_Inm       : out STD_LOGIC_VECTOR(31 downto 0);
               IDEX_H_Reg_Rs1       : out STD_LOGIC_VECTOR(3  downto 0);
               IDEX_H_Reg_Rs2       : out STD_LOGIC_VECTOR(3  downto 0);
               IDEX_H_Reg_Rd        : out STD_LOGIC_VECTOR(3  downto 0);         
               IDEX_H_PC            : out STD_LOGIC_VECTOR(6  downto 0);
               IDEX_H_NOP           : out STD_LOGIC);
    END COMPONENT;

    --DECLARACION DE COMPONENTE PARA "Registro_de_Segmentacion_EXMEM"
    COMPONENT Registro_de_Segmentacion_EXMEM
        Port ( CLK                           : in  STD_LOGIC;
               RST                           : in  STD_LOGIC;   
               EXMEM_L_Ctr_WB                : in  STD_LOGIC_VECTOR(1  downto 0);
               EXMEM_L_Ctr_MEM               : in  STD_LOGIC_VECTOR(1  downto 0);
               EXMEM_L_ALU_Out               : in  STD_LOGIC_VECTOR(31 downto 0);
               EXMEM_L_Anti_In_B             : in  STD_LOGIC_VECTOR(31 downto 0);
               EXMEM_L_MUX_Anti_MemD_Sel     : in  STD_LOGIC;
               EXMEM_L_Reg_Rd                : in  STD_LOGIC_VECTOR(3  downto 0);
               EXMEM_L_PC                    : in  STD_LOGIC_VECTOR(6  downto 0);
               EXMEM_L_NOP                   : in  STD_LOGIC;
               EXMEM_L_Reg_Rs2               : in  STD_LOGIC_VECTOR(3  downto 0);
               EXMEM_H_Ctr_WB                : out STD_LOGIC_VECTOR(1  downto 0);
               EXMEM_H_Ctr_MEM_R             : out STD_LOGIC;
               EXMEM_L_Ctr_MEM_R_Aux         : out STD_LOGIC;
               EXMEM_L_Ctr_MEM_W_Aux         : out STD_LOGIC;
               EXMEM_L_ALU_Out_Aux           : out STD_LOGIC_VECTOR(31 downto 0);
               EXMEM_H_ALU_Out               : out STD_LOGIC_VECTOR(31 downto 0);
               EXMEM_L_Anti_In_B_Aux         : out STD_LOGIC_VECTOR(31 downto 0);           
               EXMEM_L_MUX_Anti_MemD_Sel_Aux : out STD_LOGIC;
               EXMEM_H_Reg_Rd                : out STD_LOGIC_VECTOR(3  downto 0);
               EXMEM_H_PC                    : out STD_LOGIC_VECTOR(6  downto 0);
               EXMEM_H_NOP                   : out STD_LOGIC;
               EXMEM_H_MUX_Anti_MemD_Sel     : out STD_LOGIC;           
               EXMEM_H_Reg_Rs2               : out STD_LOGIC_VECTOR(3  downto 0));
    END COMPONENT;

    --DECLARACION DE COMPONENTE PARA "Registro_de_Segmentacion_MEMWB"
    COMPONENT Registro_de_Segmentacion_MEMWB
        Port ( CLK                          : in  STD_LOGIC;
               RST                          : in  STD_LOGIC;
               MEMWB_L_Ctr_WB               : in  STD_LOGIC_VECTOR(1  downto 0);
               MEMWB_L_MemD_Data_Out        : in  STD_LOGIC_VECTOR(31 downto 0);  
               MEMWB_L_ALU_Out              : in  STD_LOGIC_VECTOR(31 downto 0);         
               MEMWB_L_Reg_Rd               : in  STD_LOGIC_VECTOR(3  downto 0);  
               MEMWB_L_PC                   : in  STD_LOGIC_VECTOR(6  downto 0);
               MEMWB_L_NOP                  : in  STD_LOGIC;          
               MEMWB_L_Ctr_WB_W_Aux         : out STD_LOGIC;
               MEMWB_H_Ctr_WB_W             : out STD_LOGIC;
               MEMWB_L_Multi_MUX_WB_Sel_Aux : out STD_LOGIC;
               MEMWB_H_Multi_MUX_WB_Sel     : out STD_LOGIC;
               MEMWB_L_MemD_Data_Out_Aux    : out STD_LOGIC_VECTOR(31 downto 0);
               MEMWB_H_MemD_Data_Out        : out STD_LOGIC_VECTOR(31 downto 0);
               MEMWB_L_ALU_Out_Aux          : out STD_LOGIC_VECTOR(31 downto 0);
               MEMWB_H_ALU_Out              : out STD_LOGIC_VECTOR(31 downto 0);
               MEMWB_L_Reg_Rd_Aux           : out STD_LOGIC_VECTOR(3  downto 0);
               MEMWB_H_Reg_Rd               : out STD_LOGIC_VECTOR(3  downto 0);
               MEMWB_H_PC                   : out STD_LOGIC_VECTOR(6  downto 0);
               MEMWB_H_NOP                  : out STD_LOGIC);
    END COMPONENT;
    
    ------------------------------------------------------------------------------------ 
    ----------                     Declaracion de Signals                 --------------
    ------------------------------------------------------------------------------------ 
    signal Instr                     : std_logic_vector(31 downto 0);
    signal PC_In                     : std_logic_vector(6  downto 0);
    signal PC_Out                    : std_logic_vector(6  downto 0);  
    signal Sumador_de_Salto_Out      : std_logic_vector(6  downto 0);
    signal Sumador_PC_Out            : std_logic_vector(6  downto 0);
    signal IFID_L_NOP                : std_logic;
    signal IFID_H_NOP                : std_logic;
    signal IFID_H_Instr              : std_logic_vector(31 downto 0);
    signal IFID_H_PC                 : std_logic_vector(6  downto 0);
    signal Control_Out1              : std_logic_vector(6  downto 0);
    signal Control_Out2              : std_logic;
    signal Ctr                       : std_logic_vector(6  downto 0);
    signal MUX_Ctr_Sel               : std_logic;
    signal PC_E                      : std_logic;
    signal IFID_E                    : std_logic;
    signal Branch_Taken              : std_logic;  
    signal Desp_1_Izq_Out            : std_logic_vector(31 downto 0);
    signal Reg_Data1_Out             : std_logic_vector(31 downto 0);
    signal Reg_Data2_Out             : std_logic_vector(31 downto 0);
    signal Gen_Inm_Out               : std_logic_vector(31 downto 0);
    signal IDEX_H_PC                 : std_logic_vector(6  downto 0);
    signal IDEX_L_NOP                : std_logic;
    signal IDEX_H_NOP                : std_logic;
    signal IDEX_H_MUX_ALU_Sel        : std_logic;
    signal IDEX_H_ALU_Op             : std_logic_vector(1  downto 0);
    signal IDEX_H_Funct3             : std_logic_vector(2  downto 0);
    signal IDEX_H_Funct7             : std_logic_vector(6  downto 0);
    signal IDEX_H_Ctr_MEM            : std_logic_vector(1  downto 0);
    signal IDEX_H_Ctr_WB             : std_logic_vector(1  downto 0);
    signal IDEX_H_Gen_Inm            : std_logic_vector(31 downto 0);
    signal IDEX_H_Data1              : std_logic_vector(31 downto 0);
    signal IDEX_H_Data2              : std_logic_vector(31 downto 0);
    signal IDEX_H_Reg_Rs1            : std_logic_vector(3  downto 0);
    signal IDEX_H_Reg_Rs2            : std_logic_vector(3  downto 0);
    signal IDEX_H_Reg_Rd             : std_logic_vector(3  downto 0);
    signal Anti_In_B                 : std_logic_vector(31 downto 0);
    signal ALU_In_A                  : std_logic_vector(31 downto 0);
    signal ALU_IN_B                  : std_logic_vector(31 downto 0);   
    signal MUX_Anti_Sel_In_A         : std_logic_vector(1  downto 0);
    signal MUX_Anti_Sel_In_B         : std_logic_vector(1  downto 0);
    signal MUX_Anti_MemD_Sel         : std_logic;
    signal ALU_Ctr                   : std_logic_vector(3  downto 0);
    signal ALU_Out                   : std_logic_vector(31 downto 0);     
    signal EXMEM_L_Ctr_MEM_R         : std_logic;
    signal EXMEM_H_Ctr_MEM_R         : std_logic;
    signal EXMEM_L_Ctr_MEM_W         : std_logic;
    signal EXMEM_H_Ctr_WB            : std_logic_vector(1  downto 0);
    signal EXMEM_H_PC                : std_logic_vector(6  downto 0); 
    signal EXMEM_H_NOP               : std_logic;
    signal EXMEM_L_ALU_Out           : std_logic_vector(31 downto 0);
    signal EXMEM_H_ALU_Out           : std_logic_vector(31 downto 0);
    signal MemD_Data_In              : std_logic_vector(31 downto 0);
    signal MemD_Data_Out             : std_logic_vector(31 downto 0);
    signal EXMEM_L_MUX_Anti_MemD_Sel : std_logic;
    signal EXMEM_H_MUX_Anti_MemD_Sel : std_logic;
    signal EXMEM_H_Reg_Rd            : std_logic_vector(3  downto 0);
    signal EXMEM_L_Anti_In_B         : std_logic_vector(31 downto 0); 
    signal EXMEM_H_Reg_Rs2           : std_logic_vector(3  downto 0);
    signal MEMWB_H_PC                : std_logic_vector(6  downto 0);
    signal MEMWB_H_NOP               : std_logic;
    signal MEMWB_L_MemD_Data_Out     : std_logic_vector(31 downto 0);
    signal MEMWB_H_MemD_Data_Out     : std_logic_vector(31 downto 0);
    signal MEMWB_L_ALU_Out           : std_logic_vector(31 downto 0);
    signal MEMWB_H_ALU_Out           : std_logic_vector(31 downto 0);
    signal MEMWB_L_Reg_Rd            : std_logic_vector(3  downto 0);
    signal MEMWB_H_Reg_Rd            : std_logic_vector(3  downto 0);
    signal MEMWB_L_Ctr_WB_W          : std_logic;
    signal MEMWB_H_Ctr_WB_W          : std_logic;
    signal MEMWB_L_Multi_MUX_WB_Sel  : std_logic;
    signal MEMWB_H_Multi_MUX_WB_Sel  : std_logic;
    signal WB_L_Reg_Data_In          : std_logic_vector(31 downto 0);
    signal WB_H_Reg_Data_In          : std_logic_vector(31 downto 0);

    
begin

    -------------------------------------------------------------------
    ----------       Conexiones a Puertos de Salida      --------------
    ------------------------------------------------------------------- 
    Inter_IF  <= PC_Out;
    Inter_ID  <= IFID_H_NOP  & IFID_H_PC;
    Inter_EX  <= IDEX_H_NOP  & IDEX_H_PC;
    Inter_MEM <= EXMEM_H_NOP & EXMEM_H_PC;
    Inter_WB  <= MEMWB_H_NOP & MEMWB_H_PC;    

    -------------------------------------------------------------------
    ----------       Instancias de los Componentes       --------------
    ------------------------------------------------------------------- 

    --INSTANCIACION DEL "Contador_de_Programa"
    PC : Contador_de_Programa
    PORT MAP (
        CLK                  => CLK,
        RST                  => RST,
        PC_E                 => PC_E,
        PC_In                => PC_In,
        PC_Out               => PC_Out
    );

    --INSTANCIACION DE LA "Memoria_de_Instrucciones"
    Memoria_d_Instrucciones : Memoria_de_Instrucciones
    PORT MAP (
        Address              => PC_Out,
        Count_V              => Count_V,          
        Instr_Out            => Instr,
        MemI_Out             => Inter_MemI
    );

    --INSTANCIACION DEL "MUX_2_a_1"
    MUX_PC : MUX_2_a_1 
    GENERIC MAP (N => 7)
    PORT MAP (
        In_A                 => Sumador_de_Salto_Out,
        In_B                 => Sumador_PC_Out,
        Sel                  => Branch_Taken,
        Out_C                => PC_In
    );     
    
    --INSTANCIACION DE LA "Unidad_de_Salto_Condicional"
    USC : Unidad_de_Salto_Condicional
    PORT MAP (
        Ctr_Branch           => Control_Out2,
        Funct3               => IFID_H_Instr(14 downto 12),
        Rs1_Data_Read        => Reg_Data1_Out,
        Rs2_Data_Read        => Reg_Data2_Out, 
        Branch_Taken         => Branch_Taken
    );   

    --INSTANCIACION DEL "Sumador"
    Sumador_PC : Sumador 
    PORT MAP (
        In_A                 => PC_Out,
        In_B                 => "000100",      --Incremento en 4
        Out_C                => Sumador_PC_Out     
    ); 

    --INSTANCIACION DE LA "Unidad_Aritmetico_Logica"
    ALU : Unidad_Aritmetico_Logica 
    PORT MAP (
        In_A                 => ALU_In_A,
        In_B                 => ALU_IN_B,
        ALU_Ctr              => ALU_Ctr,
        ALU_Out              => ALU_Out    
    );

    --INSTANCIACION DE LA "Unidad_de_Control"
    Unidad_d_Control : Unidad_de_Control
    PORT MAP (
        Opcode               => IFID_H_Instr(6 downto 0),        
        Control_Out2         => Control_Out2,
        Control_Out1         => Control_Out1 
    );

    --INSTANCIACION DEL "MUX_2_a_1"
    MUX_Control : MUX_2_a_1 
    GENERIC MAP (N => 7)
    PORT MAP (
        In_A                 => Control_Out1,
        In_B                 => (others => '0'),
        Sel                  => MUX_Ctr_Sel, 
        Out_C                => Ctr
    ); 

    --INSTANCIACION DEL "Banco_de_Registros"
    Banco_d_Registros : Banco_de_Registros
    PORT MAP (
        CLK                  => CLK,
        RST                  => RST,
        Addr1_Read           => IFID_H_Instr(18 downto 15),
        Addr2_Read           => IFID_H_Instr(23 downto 20),
        Reg_Write            => MEMWB_L_Ctr_WB_W,
        Addr_Write           => MEMWB_L_Reg_Rd,
        Data_In              => WB_L_Reg_Data_In,        
        Count_V              => Count_V,
        Data1_Out            => Reg_Data1_Out,
        Data2_Out            => Reg_Data2_Out,
        Reg_Left             => Inter_Reg_Left,
        Reg_Right            => Inter_Reg_Right
    );

    --INSTANCIACION DE LA "Memoria_de_Datos"
    Memoria_d_Datos : Memoria_de_Datos
    PORT MAP (
        CLK                  => CLK,        
        RST                  => RST,
        Mem_Write            => EXMEM_L_Ctr_MEM_W,
        Mem_Read             => EXMEM_L_Ctr_MEM_R,
        Address              => EXMEM_L_ALU_Out(5 downto 0),
        Data_In              => MemD_Data_In,
        Count_V              => Count_V,
        Data_Out             => MemD_Data_Out,
        MemD_Out             => Inter_MemD
    );

    --INSTANCIACION DEL "MUX_2_a_1"
    MUX_Anticipacion_MemD : MUX_2_a_1 
    GENERIC MAP (N => 32) 
    PORT MAP (
        In_A                 => MEMWB_L_MemD_Data_Out,
        In_B                 => EXMEM_L_Anti_In_B, 
        Sel                  => EXMEM_L_MUX_Anti_MemD_Sel,
        Out_C                => MemD_Data_In
    );    
    
    --INSTANCIACION DEL "MUX_2_a_1"
    MUX_ALU : MUX_2_a_1 
    GENERIC MAP (N => 32)
    PORT MAP (
        In_A                 => Anti_In_B,
        In_B                 => IDEX_H_Gen_Inm,
        Sel                  => IDEX_H_MUX_ALU_Sel,
        Out_C                => ALU_IN_B
    );    

    --INSTANCIACION DEL "Generador_de_Inmediatos"
    Gen_Inm : Generador_de_Inmediatos
    PORT MAP (
        Instr                => IFID_H_Instr,
        Inm                  => Gen_Inm_Out    
    );
    
    --INSTANCIACION DEL "Desplazador_de_1bit_hacia_la_Izquierda"
    Desp_1_Izq : Desplazador_de_1bit_hacia_la_Izquierda
    PORT MAP (
        Inp                  => Gen_Inm_Out,
        Outp                 => Desp_1_Izq_Out     
    );
    
    --INSTANCIACION DEL "Sumador"
    Sumador_de_Salto : Sumador 
    PORT MAP (
        In_A                 => IFID_H_PC,
        In_B                 => Desp_1_Izq_Out(5 downto 0),
        Out_C                => Sumador_de_Salto_Out
    );
    
    --INSTANCIACION DEL "Controlador_ALU"
    Control_ALU : Controlador_ALU
    PORT MAP (
        ALU_Op               => IDEX_H_ALU_Op,
        Funct3               => IDEX_H_Funct3,
        Funct7               => IDEX_H_Funct7,
        ALU_Ctr              => ALU_Ctr      
    );
    
    --INSTANCIACION DEL "MUX_3_a_1"
    MUX_Anticipacion_ALU_A : MUX_3_a_1 
    PORT MAP (
        In_A                 => IDEX_H_Data1,
        In_B                 => WB_H_Reg_Data_In, 
        In_C                 => EXMEM_H_ALU_Out,
        Sel                  => MUX_Anti_Sel_In_A,
        Out_D                => ALU_In_A     
    );    
    
    --INSTANCIACION DEL "MUX_3_a_1"
    MUX_Anticipacion_ALU_B : MUX_3_a_1 
    PORT MAP (
        In_A                 => IDEX_H_Data2,
        In_B                 => WB_H_Reg_Data_In,
        In_C                 => EXMEM_H_ALU_Out,
        Sel                  => MUX_Anti_Sel_In_B,
        Out_D                => Anti_In_B     
    );      
    
    --INSTANCIACION DE LA "Unidad_de_Anticipacion"
    Unidad_d_Anticipacion : Unidad_de_Anticipacion
    PORT MAP (
        IDEX_Reg_Rs1         => IDEX_H_Reg_Rs1,
        IDEX_Reg_Rs2         => IDEX_H_Reg_Rs2,        
        EXMEM_Ctr_WB_W       => EXMEM_H_Ctr_WB(1),
        MEMWB_Ctr_WB_W       => MEMWB_H_Ctr_WB_W,
        EXMEM_Ctr_MEM_R      => EXMEM_H_Ctr_MEM_R,
        IDEX_Ctr_MEM_W       => IDEX_H_Ctr_MEM(1),
        EXMEM_Reg_Rd         => EXMEM_H_Reg_Rd,
        MEMWB_Reg_Rd         => MEMWB_H_Reg_Rd,
        MUX_Anti_ALU_A_Sel   => MUX_Anti_Sel_In_A,
        MUX_Anti_ALU_B_Sel   => MUX_Anti_Sel_In_B,
        MUX_Anti_MemD_Sel    => MUX_Anti_MemD_Sel
    );   
    
    --INSTANCIACION DE LA "Unidad_de_Deteccion_de_Riesgos"
    UDR : Unidad_de_Deteccion_de_Riesgos
    PORT MAP (
        IDEX_Ctr_MEM_R       => IDEX_H_Ctr_MEM(0),
        ID_Ctr_MEM_W         => Control_Out1(4),
        IFID_Reg_Rs1         => IFID_H_Instr(18 downto 15),
        IFID_Reg_Rs2         => IFID_H_Instr(23 downto 20),
        IDEX_Reg_Rd          => IDEX_H_Reg_Rd,
        MUX_Ctr_Sel          => MUX_Ctr_Sel,
        PC_E                 => PC_E,
        IFID_E               => IFID_E 
    );  

    --INSTANCIACION DE LA "Multi_MUX"
    Multi_MUX_WB : Multi_MUX
    PORT MAP (
        In_1A       => MEMWB_L_MemD_Data_Out,
        In_2A       => MEMWB_H_MemD_Data_Out,
        In_1B       => MEMWB_L_ALU_Out,
        In_2B       => MEMWB_H_ALU_Out,
        Sel1        => MEMWB_L_Multi_MUX_WB_Sel,
        Sel2        => MEMWB_H_Multi_MUX_WB_Sel,
        Out_1C      => WB_L_Reg_Data_In,
        Out_2C      => WB_H_Reg_Data_In
    );

    --INSTANCIACION DE "Detecction_Grafica_de_Riesgos"
    Deteccion_Grafica_d_Riesgos : Deteccion_Grafica_de_Riesgos
    PORT MAP (
        MUX_Anti_Sel_In_A       => MUX_Anti_Sel_In_A,
        MUX_Anti_Sel_In_B       => MUX_Anti_Sel_In_B,
        EXMEM_MUX_Anti_MemD_Sel => EXMEM_H_MUX_Anti_MemD_Sel,
        IFID_H_NOP              => IFID_H_NOP,
        IDEX_Reg_Rs1            => IDEX_H_Reg_Rs1,
        IDEX_Reg_Rs2            => IDEX_H_Reg_Rs2,
        EXMEM_Reg_Rs2           => EXMEM_H_Reg_Rs2,
        IDEX_Reg_Rd             => IDEX_H_Reg_Rd,
        Branch_Taken            => Branch_Taken,
        IFID_E                  => IFID_E,
        NOP_ID                  => IFID_L_NOP,
        NOP_EX                  => IDEX_L_NOP,
        Hazard_Cases            => Inter_Hazard_Cases,
        Hazard_Registers        => Inter_Hazard_Registers
    );      

    --INSTANCIACION DEL "Registro_de_Segmentacion_IFID"
    Registro_IFID : Registro_de_Segmentacion_IFID
    PORT MAP (
        CLK             => CLK,
        RST             => RST,
        IFID_E          => IFID_E,
        IFID_L_Instr    => Instr,
        IFID_L_PC       => PC_Out,
        Branch_Taken    => Branch_Taken,
        IFID_L_NOP      => IFID_L_NOP,
        IFID_H_Instr    => IFID_H_Instr,
        IFID_H_PC       => IFID_H_PC,
        IFID_H_NOP      => IFID_H_NOP
    );  

    --INSTANCIACION DEL "Registro_de_Segmentacion_IDEX"
    Registro_IDEX : Registro_de_Segmentacion_IDEX
    PORT MAP (
        CLK                 => CLK,
        RST                 => RST,
        IDEX_L_Ctr_WB       => Ctr(6 downto 5),
        IDEX_L_Ctr_MEM      => Ctr(4 downto 3),
        IDEX_L_Ctr_EX       => Ctr(2 downto 0),
        IDEX_L_Funct3       => IFID_H_Instr(14 downto 12),
        IDEX_L_Funct7       => IFID_H_Instr(31 downto 25),
        IDEX_L_Data1        => Reg_Data1_Out,
        IDEX_L_Data2        => Reg_Data2_Out,
        IDEX_L_Gen_Inm      => Gen_Inm_Out,
        IDEX_L_Reg_Rs1      => IFID_H_Instr(18 downto 15),
        IDEX_L_Reg_Rs2      => IFID_H_Instr(23 downto 20),
        IDEX_L_Reg_Rd       => IFID_H_Instr(10 downto 7),
        IDEX_L_PC           => IFID_H_PC,
        IDEX_L_NOP          => IDEX_L_NOP,        
        IDEX_H_Ctr_WB       => IDEX_H_Ctr_WB,
        IDEX_H_Ctr_MEM      => IDEX_H_Ctr_MEM,
        IDEX_H_ALU_Op       => IDEX_H_ALU_Op,
        IDEX_H_MUX_ALU_Sel  => IDEX_H_MUX_ALU_Sel,
        IDEX_H_Funct3       => IDEX_H_Funct3,
        IDEX_H_Funct7       => IDEX_H_Funct7,
        IDEX_H_Data1        => IDEX_H_Data1,
        IDEX_H_Data2        => IDEX_H_Data2,
        IDEX_H_Gen_Inm      => IDEX_H_Gen_Inm,
        IDEX_H_Reg_Rs1      => IDEX_H_Reg_Rs1,
        IDEX_H_Reg_Rs2      => IDEX_H_Reg_Rs2,
        IDEX_H_Reg_Rd       => IDEX_H_Reg_Rd,
        IDEX_H_PC           => IDEX_H_PC,
        IDEX_H_NOP          => IDEX_H_NOP
    );
    
    --INSTANCIACION DEL "Registro_de_Segmentacion_EXMEM"
    Registro_EXMEM : Registro_de_Segmentacion_EXMEM
    PORT MAP (
        CLK                           => CLK,
        RST                           => RST,
        EXMEM_L_Ctr_WB                => IDEX_H_Ctr_WB,
        EXMEM_L_Ctr_MEM               => IDEX_H_Ctr_MEM,
        EXMEM_L_ALU_Out               => ALU_Out,
        EXMEM_L_Anti_In_B             => Anti_In_B,
        EXMEM_L_MUX_Anti_MemD_Sel     => MUX_Anti_MemD_Sel,
        EXMEM_L_Reg_Rd                => IDEX_H_Reg_Rd,
        EXMEM_L_PC                    => IDEX_H_PC,
        EXMEM_L_NOP                   => IDEX_H_NOP,
        EXMEM_L_Reg_Rs2               => IDEX_H_Reg_Rs2,
        EXMEM_H_Ctr_WB                => EXMEM_H_Ctr_WB,
        EXMEM_H_Ctr_MEM_R             => EXMEM_H_Ctr_MEM_R,
        EXMEM_L_Ctr_MEM_R_Aux         => EXMEM_L_Ctr_MEM_R,
        EXMEM_L_Ctr_MEM_W_Aux         => EXMEM_L_Ctr_MEM_W,
        EXMEM_L_ALU_Out_Aux           => EXMEM_L_ALU_Out,
        EXMEM_H_ALU_Out               => EXMEM_H_ALU_Out,
        EXMEM_L_Anti_In_B_Aux         => EXMEM_L_Anti_In_B, 
        EXMEM_L_MUX_Anti_MemD_Sel_Aux => EXMEM_L_MUX_Anti_MemD_Sel,       
        EXMEM_H_Reg_Rd                => EXMEM_H_Reg_Rd,
        EXMEM_H_PC                    => EXMEM_H_PC,
        EXMEM_H_NOP                   => EXMEM_H_NOP,
        EXMEM_H_MUX_Anti_MemD_Sel     => EXMEM_H_MUX_Anti_MemD_Sel,
        EXMEM_H_Reg_Rs2               => EXMEM_H_Reg_Rs2
    );        

    --INSTANCIACION DEL "Registro_de_Segmentacion_MEMWB"
    Registro_MEMWB : Registro_de_Segmentacion_MEMWB
    PORT MAP (
        CLK                          => CLK,
        RST                          => RST,     
        MEMWB_L_Ctr_WB               => EXMEM_H_Ctr_WB,
        MEMWB_L_MemD_Data_Out        => MemD_Data_Out,
        MEMWB_L_ALU_Out              => EXMEM_H_ALU_Out,
        MEMWB_L_Reg_Rd               => EXMEM_H_Reg_Rd,
        MEMWB_L_PC                   => EXMEM_H_PC,
        MEMWB_L_NOP                  => EXMEM_H_NOP,
        MEMWB_L_Ctr_WB_W_Aux         => MEMWB_L_Ctr_WB_W,
        MEMWB_H_Ctr_WB_W             => MEMWB_H_Ctr_WB_W,
        MEMWB_L_Multi_MUX_WB_Sel_Aux => MEMWB_L_Multi_MUX_WB_Sel,
        MEMWB_H_Multi_MUX_WB_Sel     => MEMWB_H_Multi_MUX_WB_Sel,
        MEMWB_L_MemD_Data_Out_Aux    => MEMWB_L_MemD_Data_Out,
        MEMWB_H_MemD_Data_Out        => MEMWB_H_MemD_Data_Out,
        MEMWB_L_ALU_Out_Aux          => MEMWB_L_ALU_Out,
        MEMWB_H_ALU_Out              => MEMWB_H_ALU_Out,
        MEMWB_L_Reg_Rd_Aux           => MEMWB_L_Reg_Rd,
        MEMWB_H_Reg_Rd               => MEMWB_H_Reg_Rd,
        MEMWB_H_PC                   => MEMWB_H_PC,
        MEMWB_H_NOP                  => MEMWB_H_NOP
    );
    
end Behavioral;

