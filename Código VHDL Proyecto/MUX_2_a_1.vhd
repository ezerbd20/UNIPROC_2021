----------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : MUX_2_a_1
--  Descripcion del Modulo : 
--      En este modulo se encuentra el multiplexor "MUX 2 a 1", su trabajo 
--      consiste en seleccionar uno de los multiples puertos de entrada que 
--      posee, y transferir a su unico puerto de salida, la informacion 
--      presente en el puerto seleccionado.
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MUX_2_a_1 is
    --Puerto Generico
    Generic (N   : Integer);
    --Puertos Normales
    Port ( ---------------------------------------------
           ------        Puertos de Entrada       ------
           ---------------------------------------------
           In_A  : in  STD_LOGIC_VECTOR(N - 1 downto 0);
           In_B  : in  STD_LOGIC_VECTOR(N - 1 downto 0);
           Sel   : in  STD_LOGIC;
           ---------------------------------------------
           ------        Puerto de Salida         ------
           ---------------------------------------------
           Out_C : out STD_LOGIC_VECTOR(N - 1 downto 0));
end MUX_2_a_1;


architecture Behavioral of MUX_2_a_1 is

begin

   Out_C  <= In_A WHEN Sel ='1' ELSE In_B;

end Behavioral;