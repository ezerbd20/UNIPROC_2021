--------------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Clock_Divider
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "Divisor de Frecuencia" (Clock Divider),
--      que provee el reloj para la interfaz grafica y el reloj automatico para
--      el microprocesador 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity Clock_Divider is
    Port ( ----------------------------------
           ---     Puertos de Entrada     ---   
           ----------------------------------
           CLK              : in  STD_LOGIC;
           RST              : in  STD_LOGIC;
           ----------------------------------
           ---      Puerto de Salida      ---   
           ----------------------------------
           CLK_VGA          : out STD_LOGIC;
           Micro_Auto_CLK   : out STD_LOGIC);
end Clock_Divider;

architecture Behavioral of Clock_Divider is

    signal counter : STD_LOGIC_VECTOR(27 downto 0);

begin

    --Divisor de Frecuencia
    process(CLK, RST)
    begin
        if RST = '1' then
            counter <= (others => '0');
        elsif (CLK'event and CLK = '1') then
            counter <= counter + 1;
        end if;    
    end process;  
    
    --CLK para la Interfaz Grafica
    CLK_VGA <= counter(1);

    --CLK Automatico para el Microprocesador
    Micro_Auto_CLK <= counter(27);

end Behavioral;
