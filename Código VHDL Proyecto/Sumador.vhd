------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria
--  Facultad               : FEC 
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Sumador
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "Sumador", su trabajo 
--      consiste en sumar los valores presentes en sus puertos de 
--      entrada, y entregar el resuldado en el puerto de salida.
------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Sumador is
    Port ( -------------------------------------------------
           --------       Puertos de Entrada        --------
           -------------------------------------------------
           In_A  : in  STD_LOGIC_VECTOR (6 downto 0);
           In_B  : in  STD_LOGIC_VECTOR (5 downto 0);
           -------------------------------------------------
           --------         Puerto de Salida        --------
           -------------------------------------------------
           Out_C : out STD_LOGIC_VECTOR (6 downto 0));
end Sumador;

architecture Behavioral of Sumador is

begin

    --Retornar el bit de conteo sin modificaciones
    Out_C(6)          <= In_A(6); 
    
    --Retornar la suma de los valores de "In_A" e "In_B"
    Out_C(5 downto 0) <= In_A(5 downto 0) + In_B;
    
end Behavioral;
