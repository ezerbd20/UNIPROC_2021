--------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Detecction_Grafica_de_Riesgos
--  Descripcion del Modulo : 
--      Modulo necesario para el correcto funcionamiento de la interfaz
--      grafica. Este modulo le indica a la interfaz grafica cuando se
--      se han introducido burbujas al camino de datos, asi como,
--      informacion ligada a los riesgos producidos, como los registro
--      del banco de registros asociados a los riesgos, los registros de
--      segmentacion de los que se extrae informacion, etc.
--------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Deteccion_Grafica_de_Riesgos is
    Port ( -------------------------------------------------------------
           --------              Puertos de Entrada             --------
           -------------------------------------------------------------
           MUX_Anti_Sel_In_A        : in  STD_LOGIC_VECTOR(1  downto 0);
           MUX_Anti_Sel_In_B        : in  STD_LOGIC_VECTOR(1  downto 0);
           EXMEM_MUX_Anti_MemD_Sel  : in  STD_LOGIC;
           IFID_H_NOP               : in  STD_LOGIC;
           IDEX_Reg_Rs1             : in  STD_LOGIC_VECTOR(3  downto 0);
           IDEX_Reg_Rs2             : in  STD_LOGIC_VECTOR(3  downto 0);
           EXMEM_Reg_Rs2            : in  STD_LOGIC_VECTOR(3  downto 0);
           IDEX_Reg_Rd              : in  STD_LOGIC_VECTOR(3  downto 0);
           Branch_Taken             : in  STD_LOGIC;
           IFID_E                   : in  STD_LOGIC;
           -------------------------------------------------------------
           --------              Puertos de Salida              --------
           -------------------------------------------------------------
           NOP_ID                   : out STD_LOGIC;
           NOP_EX                   : out STD_LOGIC;
           Hazard_Cases             : out STD_LOGIC_VECTOR(4  downto 0);
           Hazard_Registers         : out STD_LOGIC_VECTOR(15 downto 0)
    );
end Deteccion_Grafica_de_Riesgos;

architecture Behavioral of Deteccion_Grafica_de_Riesgos is

begin

    -----------------------------------------------------------------------------------------------
    ----------      Insercion de Burbujas ("NOP"), en la Etapa ID y la Etapa EX      --------------
    -----------------------------------------------------------------------------------------------

    --Insertar Burbuja ("NOP") en la etapa de segmentacion ID, para casos de saltos
    NOP_ID <= '1' when (Branch_Taken = '1') else '0';
    
    --Insertar Burbuja ("NOP") en la etapa de segmentacion EX, para casos de estancamientos
    --o mostrar en la etapa de segmentacion EX, la burbuja que fue insertada en la etapa ID 
    NOP_EX <= '1' when (IFID_H_NOP = '1') or (IFID_E = '1') else '0';      

    -----------------------------------------------------------------------------------------------
    ----------                        Deteccion de Casos de Riesgos                  --------------
    -----------------------------------------------------------------------------------------------       
            
    --Caso 0: Riesgos de Datos en la Etapa de Segmentacion EX
    --Anticipacion en el Registro de Segmentacion EXMEM
    Hazard_Cases(0) <= '1' when (MUX_Anti_Sel_In_A = "10" or MUX_Anti_Sel_In_B = "10") else '0';            
        
    --Caso 1: Riesgos de Datos en la Etapa de Segmentacion EX
    --Anticipacion en el Registro de Segmentacion MEMWB
    Hazard_Cases(1) <= '1' when (MUX_Anti_Sel_In_A = "01" or MUX_Anti_Sel_In_B = "01") else '0';   
        
    --Caso 2: Riesgos de Datos en la Etapa de Segmentacion MEM
    --Anticipacion en el Registro de Segmentacion MEMWB
    Hazard_Cases(2) <= '1' when (EXMEM_MUX_Anti_MemD_Sel = '1') else '0';

    --Caso 3: Riesgos de Datos en la Etapa de Segmentacion ID       
    --Estancamiento en el Registro de Segmentacion IFID
    Hazard_Cases(3) <= '1' when (IFID_E = '1') else '0';
        
    --Caso 4: Riesgos de Control en la Etapa de Segmentacion ID
    --Desecho de Instruccion en el Registro de Segmentacion IFID  
    Hazard_Cases(4) <= '1' when (Branch_Taken = '1') else '0';

    -----------------------------------------------------------------------------------------------
    ----------                        Registros Ligados a Riesgos                    --------------
    ----------------------------------------------------------------------------------------------- 
        
    --Riesgo de Datos Ligado al Registro Fuente 1 (Anticipacion)
    --Procedente del Registro de Segmentacion IDEX
    Hazard_Registers(3 downto 0)   <= IDEX_Reg_Rs1 when (MUX_Anti_Sel_In_A /= "00") else x"0";
            
    --Riesgo de Datos Ligado al Registro Fuente 2 (Anticipacion)
    --Procedente del Registro de Segmentacion IDEX    
    Hazard_Registers(7 downto 4)   <= IDEX_Reg_Rs2 when (MUX_Anti_Sel_In_B /= "00") else x"0";
            
    --Riesgo de Datos Ligado al Registro Destino (Estancamiento)
    --Procedente del Registro de Segmentacion IDEX
    Hazard_Registers(11 downto 8)  <= IDEX_Reg_Rd when (IFID_E = '1') else x"0";

    --Riesgo de Datos Ligado al Registro Fuente 2 (Anticipacion)
    --Procedente del Registro de Segmentacion EXMEM
    Hazard_Registers(15 downto 12) <= EXMEM_Reg_Rs2 when (EXMEM_MUX_Anti_MemD_Sel = '1') else x"0";

end Behavioral;
