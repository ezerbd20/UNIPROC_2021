--------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Memoria_de_Instrucciones
--  Descripcion del Modulo : 
--      En este modulo se encuentra la "Memoria de Instrucciones", 
--      su trabajo consiste en almacenar las instrucciones que el 
--      microprocesador procesara, y su principal caracteristica
--      es ser una memoria de solo lectura (ROM).
--------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.ALL;
use std.textio.all;


entity Memoria_de_Instrucciones is
    Port ( ----------------------------------------------
           ------        Puerto de Entrada         ------
           ------           - Modulo -             ------
           ----------------------------------------------
           Address   : in  STD_LOGIC_VECTOR(6  downto 0);
           ----------------------------------------------
           ------     Puerto Especial de Entrada   ------
           ------        - Interfaz Grafica -      ------
           ----------------------------------------------
           Count_V   : in  integer;
           ----------------------------------------------
           ------        Puerto de Salida          ------
           ------           - Modulo -             ------
           ----------------------------------------------
           Instr_Out : out STD_LOGIC_VECTOR(31 downto 0);
           ----------------------------------------------
           ------     Puerto Especial de Salida    ------
           ------        - Interfaz Grafica -      ------
           ----------------------------------------------
           MemI_Out  : out STD_LOGIC_VECTOR(31 downto 0)
           );
end Memoria_de_Instrucciones;

   architecture Behavioral of Memoria_de_Instrucciones is

    -------------------------------------------------------------------------
    ------------      Declaracion del Array "MemI_Array"     ----------------
    -------------------------------------------------------------------------
    type   MemI_Array is array (0 to 9)  of  std_logic_vector(31 downto 0);
      
    -------------------------------------------------------------------------
    ------------         Funcion para Cargar Programa        ----------------
    -------------------------------------------------------------------------
    impure function load_program (program_name : string) return MemI_Array is
    FILE     ProgramFile     : text open read_mode is program_name;
    variable ProgramFileLine : line;
    variable temp            : bit_vector(31 downto 0);
    variable program         : MemI_Array := (others => (others => '0'));
    begin
        for i in MemI_Array'range loop
            readline(ProgramFile, ProgramFileLine);
            read(ProgramFileLine, temp);
            program(i) := to_stdlogicvector(temp);
        end loop; 
        return program;
    end load_program;
    
    -------------------------------------------------------------------------
    ----------------       Declaracion e Inicializacion      ----------------
    ----------------      de la Memoria de Instrucciones     ----------------
    -------------------------------------------------------------------------
    constant Instr : MemI_Array := load_program("programa.bin");
    
begin

    process(Address)
    begin
        --Verificar Bit de Conteo Activo
        if Address(6) = '1' then
        
            --"Address" es menor o igual a 36
            if Address(5 downto 0) <= "100100" then
                --Retornar la instrucción que indica la dirección "Address"
                Instr_Out <= Instr(to_integer(shift_right(unsigned(Address(5 downto 0)),2)));
            
            --"Address" es mayor a 36
            else
                --Retornar la instruccion: add x0, x0, x0
                Instr_Out <= x"00000033";
            end if;    
        
        --Bit de Conteo No Activo
        else
            --Retornar la instruccion: add x0, x0, x0
            Instr_Out <= x"00000000";
        end if;
    end process;
    
    -----------------------------------------------------
    --------     Segmento de Codigo para      -----------
    --------       la Interfaz Grafica        -----------
    -----------------------------------------------------
    MemI_Out <= Instr(Count_V - 3); 

end Behavioral;