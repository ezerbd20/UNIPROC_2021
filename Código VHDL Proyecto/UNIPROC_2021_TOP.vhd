----------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : UNIPROC_2021_TOP
--  Descripcion del Modulo : 
--      Archivo "Top" de todo el proyecto "UNIPROC 2021". En este modulo se 
--      instancia el modulo "Microarquitectura", el modulo "VGA_Controller",
--      el  modulo  "Interfaz",  el  modulo  "Clock_Divider"  y  el  modulo 
--      "Multi_Debounce"
----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity UNIPROC_2021_Top is
    Port ( ----------------------------------------------------------
           ------               Puertos de Entrada             ------   
           ----------------------------------------------------------
           CLK_Inp                : in  STD_LOGIC;                          
           Push_Button_Inp        : in  STD_LOGIC;                      
           RST_VGA_Inp            : in  STD_LOGIC;                     
           RST_Micro_Inp          : in  STD_LOGIC;                      
           Micro_CLK_Src_Sel_Inp  : in  STD_LOGIC;                      
           ---------------------------------------------------------- 
           ------               Puertos de Salida              ------
           ----------------------------------------------------------
           Hsync_Outp             : out STD_LOGIC;                     
           Vsync_Outp             : out STD_LOGIC;                    
           Data_R_Outp            : out STD_LOGIC_VECTOR(4 downto 0);  
           Data_G_Outp            : out STD_LOGIC_VECTOR(5 downto 0);
           Data_B_Outp            : out STD_LOGIC_VECTOR(4 downto 0)
    );
end UNIPROC_2021_Top;

architecture Behavioral of UNIPROC_2021_Top is
 
    -------------------------------------------------------------------
    ----------       Declaracion de los Componentes      --------------
    ------------------------------------------------------------------- 

    --DECLARACION DE COMPONENTE PARA "Clock_Divider"
    COMPONENT Clock_Divider
        Port ( CLK              : in  STD_LOGIC;
               RST              : in  STD_LOGIC;
               CLK_VGA          : out STD_LOGIC;
               Micro_Auto_CLK   : out STD_LOGIC
               );
    END COMPONENT;
 
    --DECLARACION DE COMPONENTE PARA "Multi_Debounce"
    COMPONENT Multi_Debounce
        Port ( CLK                : in  STD_LOGIC;
               Micro_CLK_Src_Sel  : in  STD_LOGIC;
               RST_VGA_In         : in  STD_LOGIC;
               RST_Micro_In       : in  STD_LOGIC;
               Push_Button_In     : in  STD_LOGIC;
               Micro_CLK_Sel      : out STD_LOGIC;
               RST_VGA_Controller : out STD_LOGIC;
               RST_Micro          : out STD_LOGIC;
               Micro_Manual_CLK   : out STD_LOGIC
               );
    END COMPONENT; 
    
    --DECLARACION DE COMPONENTE PARA "VGA_Controller"
    COMPONENT VGA_Controller
        PORT( CLK          : in  STD_LOGIC;
              RST          : in  STD_LOGIC;
              Hsync        : out STD_LOGIC;
              Vsync        : out STD_LOGIC;
              Video_Enable : out STD_LOGIC;
              X_Position   : out integer;
              Y_Position   : out integer;
              Count_V      : out integer range 0 to 40;
              Count_H      : out integer range 0 to 53
              );
    END COMPONENT;
    
    --DECLARACION DE COMPONENTE PARA "Interfaz"
    COMPONENT Interfaz
        PORT( CLK               : in  STD_LOGIC;
              X_Position        : in  integer;
              Y_Position        : in  integer;
              Count_V           : in  integer range 0 to 40;
              Count_H           : in  integer range 0 to 53;
              Video_Enable      : in  STD_LOGIC;
              Hazard_Cases      : in  STD_LOGIC_VECTOR(4  downto 0);
              Hazard_Registers  : in  STD_LOGIC_VECTOR(15 downto 0);
              IF_In             : in  STD_LOGIC_VECTOR(6  downto 0);
              ID_In             : in  STD_LOGIC_VECTOR(7  downto 0);
              EX_In             : in  STD_LOGIC_VECTOR(7  downto 0);
              MEM_In            : in  STD_LOGIC_VECTOR(7  downto 0);
              WB_In             : in  STD_LOGIC_VECTOR(7  downto 0);
              MemI_In           : in  STD_LOGIC_VECTOR(31 downto 0);
              MemD_In           : in  STD_LOGIC_VECTOR(31 downto 0);
              Reg_In1           : in  STD_LOGIC_VECTOR(31 downto 0);
              Reg_In2           : in  STD_LOGIC_VECTOR(31 downto 0);
              Red               : out STD_LOGIC_VECTOR(4  downto 0);
              Green             : out STD_LOGIC_VECTOR(5  downto 0);
              Blue              : out STD_LOGIC_VECTOR(4  downto 0)            
        );    
    END COMPONENT;
    
    --DECLARACION DE COMPONENTE PARA "Microarquitectura"
    COMPONENT Microarquitectura
        Port ( CLK                    : in  STD_LOGIC;
               RST                    : in  STD_LOGIC;
               Count_V                : in  integer;
               Inter_IF               : out STD_LOGIC_VECTOR(6  downto 0);
               Inter_ID               : out STD_LOGIC_VECTOR(7  downto 0);
               Inter_EX               : out STD_LOGIC_VECTOR(7  downto 0);
               Inter_MEM              : out STD_LOGIC_VECTOR(7  downto 0);
               Inter_WB               : out STD_LOGIC_VECTOR(7  downto 0);
               Inter_MemI             : out STD_LOGIC_VECTOR(31 downto 0);
               Inter_MemD             : out STD_LOGIC_VECTOR(31 downto 0);
               Inter_Reg_Left         : out STD_LOGIC_VECTOR(31 downto 0);
               Inter_Reg_Right        : out STD_LOGIC_VECTOR(31 downto 0);
               Inter_Hazard_Cases     : out STD_LOGIC_VECTOR(4  downto 0);
               Inter_Hazard_Registers : out STD_LOGIC_VECTOR(15 downto 0));
    END COMPONENT;

    -------------------------------------------------------------------------- 
    ----------                Declaracion de Signals            --------------
    -------------------------------------------------------------------------- 
    
    --Signals que Controlan el Funcionamiento de la Interfaz Grafica
    --en Condiciones Normales
    signal CLK_VGA              : std_logic;    
    signal RST_VGA_Controller   : std_logic;
    signal Video_Enable         : std_logic;
    signal Count_V              : integer range 0 to 40;
    signal Count_H              : integer range 0 to 53;
    signal X_Position           : integer;
    signal Y_Position           : integer;
    
    --Signals que Controlan Parte del Comportamiento de la Interfaz 
    --Grafica, Cuando se Detectan Riesgos (Hazards)
    signal Hazard_Cases         : std_logic_vector(4  downto 0);
    signal Hazard_Registers     : std_logic_vector(15 downto 0);
    
    --Signals que Muestran el Comportamiento del Microprocesador,
    --a Traves de la Interfaz Grafica
    signal IF_Sig               : std_logic_vector(6  downto 0);
    signal ID_Sig               : std_logic_vector(7  downto 0);
    signal EX_Sig               : std_logic_vector(7  downto 0);
    signal MEM_Sig              : std_logic_vector(7  downto 0);
    signal WB_Sig               : std_logic_vector(7  downto 0);
    signal MemI_Sig             : std_logic_vector(31 downto 0);
    signal MemD_Sig             : std_logic_vector(31 downto 0);
    signal Reg_Sig1             : std_logic_vector(31 downto 0);
    signal Reg_Sig2             : std_logic_vector(31 downto 0);
    
    --Signals de los Distintos Clocks y Reset del Microprocesador
    signal CLK_Micro            : std_logic;  
    signal Micro_CLK_Sel        : std_logic;
    signal Micro_Manual_CLK     : std_logic;
    signal Micro_Auto_CLK       : std_logic;
    signal RST_Micro            : std_logic; 

begin
    
    --Seleccion del Tipo de Clock que se Usara el Microprocesador
    CLK_Micro   <= Micro_Auto_CLK when Micro_CLK_Sel = '1' else Micro_Manual_CLK;  

    -------------------------------------------------------------------
    ----------     Instanciaciones de los Componentes    --------------
    ------------------------------------------------------------------- 

    --INSTANCIACION DE "Clock_Divider"
    CLK_Divider : Clock_Divider 
    PORT MAP (
        CLK                     => CLK_Inp,
        RST                     => RST_VGA_Inp,
        CLK_VGA                 => CLK_VGA,
        Micro_Auto_CLK          => Micro_Auto_CLK
    );

    --INSTANCIACION DE "Multi_Debounce"
    Multiple_Debounce : Multi_Debounce
    PORT MAP (
        CLK                     => CLK_VGA,              
        Micro_CLK_Src_Sel       => Micro_CLK_Src_Sel_Inp,
        RST_VGA_In              => RST_VGA_Inp,
        RST_Micro_In            => RST_Micro_Inp,
        Push_Button_In          => Push_Button_Inp,
        Micro_CLK_Sel           => Micro_CLK_Sel,    
        RST_VGA_Controller      => RST_VGA_Controller,
        RST_Micro               => RST_Micro,         
        Micro_Manual_CLK        => Micro_Manual_CLK
    );
                   
    --INSTANCIACION DE "VGA_Controller"
    Controlador_VGA : VGA_Controller 
    PORT MAP (
        CLK                     => CLK_VGA,
        RST                     => RST_VGA_Controller,
        Hsync                   => HSYNC_Outp,
        Vsync                   => VSYNC_Outp,
        Video_Enable            => Video_Enable,
        X_Position              => X_Position,
        Y_Position              => Y_Position, 
        Count_V                 => Count_V,
        Count_H                 => Count_H
    );

    --INSTANCIACION DE "Interfaz"
    Interfaz_Grafica : Interfaz 
    PORT MAP (
        CLK                     => CLK_VGA,     
        X_Position              => X_Position,
        Y_Position              => Y_Position,
        Count_V                 => Count_V,
        Count_H                 => Count_H,
        Video_Enable            => Video_Enable,
        Hazard_Cases            => Hazard_Cases,
        Hazard_Registers        => Hazard_Registers,
        IF_In                   => IF_Sig,
        ID_In                   => ID_Sig,
        EX_In                   => EX_Sig,
        MEM_In                  => MEM_Sig,
        WB_In                   => WB_Sig,
        MemI_In                 => MemI_Sig,
        MemD_In                 => MemD_Sig,
        Reg_In1                 => Reg_Sig1,
        Reg_In2                 => Reg_Sig2,
        Red                     => Data_R_Outp,
        Green                   => Data_G_Outp,
        Blue                    => Data_B_Outp       
    );
    
    --INSTANCIACION DE "Microarquitectura"
    Microprocesador : Microarquitectura
    PORT MAP ( 
        CLK                     => CLK_Micro,
        RST                     => RST_Micro,
        Count_V                 => Count_V,
        Inter_IF                => IF_Sig,
        Inter_ID                => ID_Sig,
        Inter_EX                => EX_Sig,
        Inter_MEM               => MEM_Sig,
        Inter_WB                => WB_Sig,
        Inter_MemI              => MemI_Sig,
        Inter_MemD              => MemD_Sig,
        Inter_Reg_Left          => Reg_Sig1, 
        Inter_Reg_Right         => Reg_Sig2,        
        Inter_Hazard_Cases      => Hazard_Cases,
        Inter_Hazard_Registers  => Hazard_Registers
    );
           
end Behavioral;