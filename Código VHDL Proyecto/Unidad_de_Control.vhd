-------------------------------------------------------------------- 
--  Nombre del Proyecto    : UNIPROC 2021                           
--  Institucion            : Universidad Nacional de Ingenieria     
--  Facultad               : FEC      
--  Carrera                : Ing. Electronica                              
--  Pais                   : Nicaragua                                   
--  Nombre del Autor       : Jason Ortiz                            
--  Nombre del Modulo      : Unidad_de_Control                               
--  Descripcion del Modulo :                                            
--      En este modulo se encuentra la "Unidad de Control", su
--      trabajo consiste en ser el elemento de control principal 
--      del microprocesador, por tal motivo, decodifica las
--      instrucciones y genera se�ales para los elementos del 
--      camino de datos que estan bajo su dominio, de igual manera,
--      indica a los elementos de control secundario, las se�ales 
--      de control a generar para los elementos del camino de datos 
--      que estos controlan
--------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Unidad_de_Control is
    Port ( ---------------------------------------------------
           -------          Puerto de Entrada          -------
           ---------------------------------------------------
           Opcode        : in  STD_LOGIC_VECTOR(6 downto 0);
           ---------------------------------------------------
           -------          Puertos de Salida          -------
           ---------------------------------------------------           
           Control_Out2  : out STD_LOGIC;
           Control_Out1  : out STD_LOGIC_VECTOR(6 downto 0));           
end Unidad_de_Control;

architecture Behavioral of Unidad_de_Control is

begin

    process(Opcode)
    begin
        case Opcode is
            --Instruccion lw
            when "0000011" =>
                --Ctr_Branch
                Control_Out2     <= '0';

                --Reg_W
                Control_Out1(6)  <= '1';
                
                --Multi_MUX_WB_Sel   
                Control_Out1(5)  <= '1';
                
                --MemD_W   
                Control_Out1(4)  <= '0';
                
                --MemD_R  
                Control_Out1(3)  <= '1';
                
                --MUX_ALU_Sel  
                Control_Out1(2)  <= '0';
                
                --ALU_Op  
                Control_Out1(1 downto 0)  <= "01";        
            
            --Instruccion sw
             when "0100011" =>
                --Ctr_Branch
                Control_Out2     <= '0';

                --Reg_W
                Control_Out1(6)  <= '0';
                
                --MUX_WB_Sel   
                Control_Out1(5)  <= '0';
                
                --MemD_W   
                Control_Out1(4)  <= '1';
                
                --MemD_R  
                Control_Out1(3)  <= '0';
                
                --MUX_ALU_Sel  
                Control_Out1(2)  <= '0';
                
                --ALU_Op  
                Control_Out1(1 downto 0)  <= "01";
                
            --Instrucciones Logicas y Arimeticas con Inmediatos 
            when "0010011" =>
                --Ctr_Branch
                Control_Out2     <= '0';

                --Reg_W
                Control_Out1(6)  <= '1';
                
                --MUX_WB_Sel   
                Control_Out1(5)  <= '0';
                
                --MemD_W   
                Control_Out1(4)  <= '0';
                
                --MemD_R  
                Control_Out1(3)  <= '0';
                
                --MUX_ALU_Sel  
                Control_Out1(2)  <= '0';
                
                --ALU_Op  
                Control_Out1(1 downto 0)  <= "10";
                
            --Instrucciones Logicas y Arimeticas
            when "0110011" =>
                --Ctr_Branch
                Control_Out2     <= '0';

                --Reg_W
                Control_Out1(6)  <= '1';
                
                --MUX_WB_Sel   
                Control_Out1(5)  <= '0';
                
                --MemD_W   
                Control_Out1(4)  <= '0';
                
                --MemD_R  
                Control_Out1(3)  <= '0';
                
                --MUX_ALU_Sel  
                Control_Out1(2)  <= '1';
                
                --ALU_Op  
                Control_Out1(1 downto 0)  <= "10";
            
            --Instrucciones de Salto Condicional
            when "1100011" =>
                --Ctr_Branch
                Control_Out2     <= '1';

                --Reg_W
                Control_Out1(6)  <= '0';
                
                --MUX_WB_Sel   
                Control_Out1(5)  <= '0';
                
                --MemD_W   
                Control_Out1(4)  <= '0';
                
                --MemD_R  
                Control_Out1(3)  <= '0';
                
                --MUX_ALU_Sel  
                Control_Out1(2)  <= '0';
                
                --ALU_Op  
                Control_Out1(1 downto 0)  <= "00";
            
            --Casos no Estimados
            when others =>
                --Ctr_Branch
                Control_Out2     <= '0';

                --Reg_W
                Control_Out1(6)  <= '0';
                
                --MUX_WB_Sel   
                Control_Out1(5)  <= '0';
                
                --MemD_W   
                Control_Out1(4)  <= '0';
                
                --MemD_R  
                Control_Out1(3)  <= '0';
                
                --MUX_ALU_Sel  
                Control_Out1(2)  <= '0';
                
                --ALU_Op  
                Control_Out1(1 downto 0)  <= "00"; 
        end case;

    end process;

end Behavioral;

