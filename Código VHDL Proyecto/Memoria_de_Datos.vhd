----------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Memoria_de_Datos
--  Descripcion del Modulo : 
--      En este modulo se encuentra la "Memoria de Datos", su
--      trabajo consiste en almacenar aquellos datos, que el 
--      banco de registros (por su muy limitada capacidad de
--      almacenamiento), no requiere tener a disposicion, pero
--      podira necesita en un futuro.
----------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Memoria_de_Datos is
    Port ( ----------------------------------------------
           ------       Puertos de Entrada         ------
           ------           - Modulo -             ------
           ----------------------------------------------
           CLK       : in  STD_LOGIC;
           RST       : in  STD_LOGIC; 
           Mem_Write : in  STD_LOGIC;
           Mem_Read  : in  STD_LOGIC;
           Address   : in  STD_LOGIC_VECTOR(5  downto 0);
           Data_In   : in  STD_LOGIC_VECTOR(31 downto 0);
           ----------------------------------------------
           ------    Puerto Especial de Entrada    ------
           ------       - Interfaz Grafica -       ------
           ----------------------------------------------
           Count_V   : in  integer;
           ----------------------------------------------
           ------         Puerto de Salida         ------
           ------            - Modulo -            ------
           ----------------------------------------------
           Data_Out  : out STD_LOGIC_VECTOR(31 downto 0);
           ----------------------------------------------
           ------    Puerto Especial de Salida     ------
           ------       - Interfaz Grafica -       ------
           ----------------------------------------------
           MemD_Out  : out STD_LOGIC_VECTOR(31 downto 0)
           );
end Memoria_de_Datos;

architecture Behavioral of Memoria_de_Datos is

    --------------------------------------------------------------------
    -----------       Declaracion del Array "MemD_Array"       ---------
    --------------------------------------------------------------------
    type MemD_Array is array (0 to 9) of std_logic_vector (31 downto 0);
 
 
    --------------------------------------------------------------------
    -----------         Declaracion e Inicializacion         -----------
    -----------            de la Memoria de Datos            -----------
    --------------------------------------------------------------------
    signal Data             : MemD_Array :=(    --Direcciones:
        x"AAAAAAAA",     --0
        x"AAAAAAAA",     --4
        x"AAAAAAAA",     --8       
        x"AAAAAAAA",     --12
        x"AAAAAAAA",     --16  
        x"AAAAAAAA",     --20 
        x"CCCCCCCC",     --24
        x"AAAAAAAA",     --28
        x"99999999",     --32
        x"AAAAAAAA");    --36

    --Signal para almacenar el ultimo dato leido
    signal Last_Data        : std_logic_vector(31 downto 0);

begin

    PROCESS(CLK, RST)
    begin
        --Establecer nuevamente los valores iniciales de la memoria de datos
        if RST = '1' then 
            Data(0) <= x"AAAAAAAA";
            Data(1) <= x"AAAAAAAA";
            Data(2) <= x"AAAAAAAA";
            Data(3) <= x"AAAAAAAA";
            Data(4) <= x"AAAAAAAA";
            Data(5) <= x"AAAAAAAA";
            Data(6) <= x"CCCCCCCC";
            Data(7) <= x"AAAAAAAA";
            Data(8) <= x"99999999";
            Data(9) <= x"AAAAAAAA";
            
        elsif (rising_edge(CLK)) then
            
            --Verificar que "Address" es menor o igual a 36
            if (Address <= "100100") then 
            
                --Verificar que el puerto de escritura, ha sido establecido en '1' logico
                if (Mem_Write = '1') then
                
                    --Escribir el dato "Data_In", en la direccion "Address"
                    Data(to_integer(shift_right(unsigned(Address),2))) <= Data_In;
            
                --Verificar que el puerto de lectura, ha sido establecido en '1' logico
                elsif (Mem_Read = '1') then 
                
                    --Retornar el dato almacenado en la direccion "Address"
                    Data_Out  <= Data(to_integer(shift_right(unsigned(Address),2)));
                    
                    --Almacer en "Last_Data", el dato leido
                    Last_Data <= Data(to_integer(shift_right(unsigned(Address),2)));
                
                --Retornar el ultimo dato leido    
                else
                    Data_Out <= Last_Data;    
                end if; 
                  
            --Cuando "Address" es mayor a 36, retornar el ultimo dato leido
            else
                Data_Out <= Last_Data;    
            end if;    
        end if;
    end process;      
    
    --------------------------------------------
    -----     Segmento de Codigo para      -----
    -----       la Interfaz Grafica        -----
    --------------------------------------------    
--    MemD_Out <= DATA(6);
    MemD_Out <= DATA(Count_V - 3); 

end Behavioral;

