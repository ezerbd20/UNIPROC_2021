---------------------------------------------------------------------------  
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : VGA_Controller
--  Descripcion del Modulo : 
--      En este modulo se encuentra el "Controlador VGA" (VGA Controller), 
--      cuya funcion consiste en permitir la sincronizacion vertical y
--      horizontal de la interfaz grafica, asi como, activar el video 
--      cuando se este dentro del area de dibujado de la pantalla.   
---------------------------------------------------------------------------  

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity VGA_Controller is
    Port ( ------------------------------------------------
           ------         Puertos de Entrada         ------   
           ------------------------------------------------
           CLK            : in  STD_LOGIC;
           RST            : in  STD_LOGIC;
           ------------------------------------------------
           ------         Puertos de Salida          ------
           ------------------------------------------------
           Hsync          : out STD_LOGIC;
           Vsync          : out STD_LOGIC;
           Video_Enable   : out STD_LOGIC;
           X_Position     : out integer;
           Y_Position     : out integer;           
           Count_V        : out integer range 0 to 40;
           Count_H        : out integer range 0 to 53
        );
end VGA_Controller;

architecture Behavioral of VGA_Controller is
    
    -------------------------------------------------------------------------- 
    ----------              Declaracion de Constantes           --------------
    --------------------------------------------------------------------------    

    --Especificaciones de Tiempo para la Resolucion VGA 640x480
    constant H_Video         : INTEGER   := 640;    --Lineas
    constant H_Front_Porch   : INTEGER   := 16;  
    constant H_Sync_Pulse    : INTEGER   := 96;  
    constant H_Back_Porch    : INTEGER   := 48;  
    constant V_Video         : INTEGER   := 480;    --Pixeles
    constant V_Front_Porch   : INTEGER   := 10;  
    constant V_Sync_Pulse    : INTEGER   := 2;  
    constant V_Back_Porch    : INTEGER   := 33; 
    
    --Numero Total de pixels-clock por Linea 
    constant H_Total_Pixels  : INTEGER := H_Video + H_Front_Porch + H_Sync_Pulse + H_Back_Porch;
    
    --Numero Total de Lineas por Cuadro
    constant V_Total_Lines   : INTEGER := V_Video + V_Front_Porch + V_Sync_Pulse + V_Back_Porch;
    
    -------------------------------------------------------------------------- 
    ----------                Declaracion de Signals            --------------
    --------------------------------------------------------------------------   

    signal Pixel_Count       : INTEGER RANGE 0 TO H_Total_Pixels - 1 := 0;
    signal Line_Count        : INTEGER RANGE 0 TO V_Total_Lines  - 1 := 0;

begin   
        
    -------------------------------------------------------------------
    ----------     Instanciaciones de los Componentes    --------------
    ------------------------------------------------------------------- 

    Sync_Process : process(CLK)
    begin
        if (rising_edge(CLK)) then
            if(RST = '1') then
                --Establecer en 0, las Senales de Sincronizacion
                Hsync          <= '0';
                Vsync          <= '0';
                
                --Establecer en 0, la Senal Habilitacion de Video
                Video_Enable   <= '0';
            else

                --Habilitar Video, en el Area que si se Muestra en Pantalla
                if((Line_Count < V_Video) and (Pixel_Count < H_Video)) then
                    Video_Enable   <= '1';
                    
                --Deshabilitar Video, en las Areas que no se Muestran en Pantalla
                else
                    Video_Enable   <= '0';
                end if;

                --Inicio del Pulso de Sincronizacion Horizontal
                if(Pixel_Count = H_Video + H_Front_Porch - 1) then
                    Hsync <= '0';
                    
                --Fin del Pulso de Sincronizacion Horizontal
                elsif(Pixel_Count = H_Video + H_Front_Porch + H_Sync_Pulse - 1) then
                    Hsync <= '1';
                end if;
                
                --Inicio del Pulso de Sincronizacion Vertical
                if(Pixel_Count = H_Total_Pixels - 1) and (Line_Count = V_Video + V_Front_Porch - 1) then
                    Vsync <= '1';
                    
                --Fin del Pulso de Sincronizacion Vertical
                elsif(Pixel_Count = H_Total_Pixels - 1) and (Line_Count = V_Video + V_Front_Porch + V_Sync_Pulse - 1) then
                    Vsync  <= '0';
                end if;
                
            end if;
        end if;
    end process;

    --Puerto de Salida para el Contador de Pixels
    X_Position     <= Pixel_Count;
    
    --Puerto de Salida para el Contador de Lineas
    Y_Position     <= Line_Count;
    
    Counters_Process : process(CLK)
    variable count_v_var : INTEGER range 0 to 40 := 0;
    variable count_h_var : INTEGER range 0 to 53 := 0;
    begin
        if (rising_edge(CLK)) then
            if(RST = '1') then
                --Establecer en 0, todos los Contadores
                Pixel_Count <= 0;
                Line_Count  <= 0;
                count_h_var := 0; 
                count_v_var := 0;
            else
                --Contador de Pixels                
                if(Pixel_Count < H_Total_Pixels - 1) then
                    --Incrementar en 1, el Contador de Pixels
                    Pixel_Count   <= Pixel_Count + 1;
                    
                else
                    --Establecer en 0, el Contador de Pixels
                    Pixel_Count <= 0;
                    --Establecer en 0, el Contador Horizontal de Cuadros
                    count_h_var := 0;
                    
                    --Contador de Lineas
                    if(Line_Count < V_Total_Lines - 1)then                        
                        --Incrementar en 1, el Contador de Lineas
                        Line_Count    <= Line_Count + 1;					  
                    else
                        --Establecer en 0, el Contador de Lineas 
                        Line_Count  <= 0;
                        --Establecer en 0, el Contador Verticual de Cuadros
                        count_v_var := 0;	    
                    end if;
                    
                    --Contador Vertical de Cuadros de la Interfaz Grafica	
                    if Line_Count < (12 *(count_v_var + 1)) then
                        --No Incrementar el Contador Vertical de Cuadros
                        count_v_var := count_v_var;
                    
                    else
                        --Incrementar en 1, el Contador Verticual de Cuadros
                        count_v_var := count_v_var + 1;
                    end if;
                end if;
                   
                --Contador Horizontal de Cuadros de la Interfaz Grafica
                if Pixel_Count < (12 * (count_h_var + 1)) then
                    --No Incrementar el Contador Horizontal de Cuadros
                    count_h_var := count_h_var;
                
                else
                    --Incrementar en 1, el Contador Horizontal de Cuadros
                    count_h_var := count_h_var + 1;	
                end if;
            end if;
        end if;
     
        --Puerto de Salida para el Contador Horizontal de Cuadros 
        Count_H <= count_h_var;
        
        --Puerto de Salida para el Contador Vertical de Cuadros 
        Count_V <= count_v_var;
        
    end process;

end Behavioral;