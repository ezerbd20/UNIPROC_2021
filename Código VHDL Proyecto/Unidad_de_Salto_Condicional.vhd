----------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Unidad_de_Salto_Condicional
--  Descripcion del Modulo : 
--      En este modulo se encuentra la "Unidad de Salto Condicional" o "USC",
--      su trabajo consiste en evaluar e informar, si se ha cumplido la
--      condicion de salto de la instruccion beq, la instruccion bne o la 
--      instruccion blt.
----------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Unidad_de_Salto_Condicional is
    Port ( Ctr_Branch       : in  STD_LOGIC;
           Funct3           : in  STD_LOGIC_VECTOR(2 downto 0);
           Rs1_Data_Read    : in  STD_LOGIC_VECTOR(31 downto 0);
           Rs2_Data_Read    : in  STD_LOGIC_VECTOR(31 downto 0);
           Branch_Taken     : out STD_LOGIC);
end Unidad_de_Salto_Condicional;

architecture Behavioral of Unidad_de_Salto_Condicional is

begin

    process(Rs1_Data_Read, Rs2_Data_Read, Funct3, Ctr_Branch)
    begin
        --Verficar si se cumplen las condiciones de salto de la instruccion beq
        if (Rs1_Data_Read  = Rs2_Data_Read AND Funct3 = "000" AND Ctr_Branch = '1') then
            --Saltar
            Branch_Taken <= '1';
        
        --Verficar si se cumplen las condiciones de salto de la instruccion bne
        elsif (Rs1_Data_Read /= Rs2_Data_Read AND Funct3 = "001" AND Ctr_Branch = '1') then          
            --Saltar
            Branch_Taken <= '1';
        
        --Verficar si se cumplen las condiciones de salto de la instruccion blt
        elsif (Rs1_Data_Read  < Rs2_Data_Read AND Funct3 = "100" AND Ctr_Branch = '1') then 
            --Saltar
            Branch_Taken <= '1';
            
        --No se cumple ninguna condicion de salto
        else
            --No Saltar
            Branch_Taken <= '0';
        end if;
    end process;

end Behavioral;
