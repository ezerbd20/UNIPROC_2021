------------------------------------------------------------------------------
--  Nombre del Proyecto    : UNIPROC 2021
--  Institucion            : Universidad Nacional de Ingenieria 
--  Facultad               : FEC
--  Carrera                : Ing. Electronica
--  Pais                   : Nicaragua
--  Nombre del Autor       : Jason Ortiz
--  Nombre del Modulo      : Interfaz
--  Descripcion del Modulo : 
--      En este modulo se manda a llamar al paquete "Interfaz_PKG", el cual
--      cuenta con las funciones y elementos necesarios, para recrear en 
--      pantalla, la interfaz grafica que permite la interacci�n con el 
--      microprocesador
------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.Interfaz_PKG.all;

entity Interfaz is
    Port ( -------------------------------------------------
           ------          Puertos de Entrada         ------   
           -------------------------------------------------
           CLK              : in  STD_LOGIC;
           X_Position       : in  integer;
           Y_Position       : in  integer;
           Count_V   	    : in  integer range 0 to 40;
           Count_H   	    : in  integer range 0 to 53;
           Video_Enable     : in  STD_LOGIC;
           Hazard_Cases     : in  STD_LOGIC_VECTOR(4  downto 0);
           Hazard_Registers : in  STD_LOGIC_VECTOR(15 downto 0);
           IF_In            : in  STD_LOGIC_VECTOR(6  downto 0);
           ID_In            : in  STD_LOGIC_VECTOR(7  downto 0);
           EX_In            : in  STD_LOGIC_VECTOR(7  downto 0);
           MEM_In           : in  STD_LOGIC_VECTOR(7  downto 0);
           WB_In            : in  STD_LOGIC_VECTOR(7  downto 0);
           MemI_In          : in  STD_LOGIC_VECTOR(31 downto 0);
           MemD_In          : in  STD_LOGIC_VECTOR(31 downto 0);
           Reg_In1          : in  STD_LOGIC_VECTOR(31 downto 0);
           Reg_In2          : in  STD_LOGIC_VECTOR(31 downto 0);           
           -------------------------------------------------
           ------          Puertos de Salida          ------
           -------------------------------------------------
           Red              : out STD_LOGIC_VECTOR(4  downto 0);
           Green            : out STD_LOGIC_VECTOR(5  downto 0);
           Blue             : out STD_LOGIC_VECTOR(4  downto 0));
end Interfaz;

architecture Behavioral of Interfaz is      

    --------------------------------------------------------
    ------            Interfaz Grafica              --------
    ------              -- Dise�o --                -------- 
    --------------------------------------------------------
    constant Interfaz                          : Screen := (
    "*****************************************************",  
    "*MEMORIA DE INSTRUCCIONES*|*****MEMORIA DE DATOS*****",
    "**************************|**************************",
    "******00.*^^^^_^^^^*******|******00.*~~~~_~~~~*******",
    "******04.*^^^^_^^^^*******|******04.*~~~~_~~~~*******",
    "******08.*^^^^_^^^^*******|******08.*~~~~_~~~~*******",
    "******12.*^^^^_^^^^*******|******12.*~~~~_~~~~*******",
    "******16.*^^^^_^^^^*******|******16.*~~~~_~~~~*******",
    "******20.*^^^^_^^^^*******|******20.*~~~~_~~~~*******",
    "******24.*^^^^_^^^^*******|******24.*~~~~_~~~~*******",
    "******28.*^^^^_^^^^*******|******28.*~~~~_~~~~*******",
    "******32.*^^^^_^^^^*******|******32.*~~~~_~~~~*******",
    "******36.*^^^^_^^^^*******|******36.*~~~~_~~~~*******",
    "**************************|**************************",  
    "*|||||||||||||||||||||||||||||||||||||||||||||||||||*",  
    "*****************|\\\\\\\\\\\\\\\\\\\|***************",  
    "*****************|||||||||||||||||||||***************",
    "*****************************************************",
    "****************ETAPAS DE SEGMENTACION***************",
    "*****************************************************",
    "******IF********ID********EX*******MEM********WB*****",
    "***********/*********/*********/*********/***********",
    "*****&&&***/***$$$***/***%%%***/***###***/***+++*****",
    "***********/*********/*********/*********/***********",
    "*****************************************************",
    "*****************************************************",
    "*|||||||||||||||||||||||||||||||||||||||||||||||||||*",
    "*****************************************************",
    "******************BANCO DE REGISTROS*****************",
    "*****************************************************",
    "******00.*<<<<_<<<<*******|******08.*>>>>_>>>>*******",
    "******01.*<<<<_<<<<*******|******09.*>>>>_>>>>*******",
    "******02.*<<<<_<<<<*******|******10.*>>>>_>>>>*******",
    "******03.*<<<<_<<<<*******|******11.*>>>>_>>>>*******",
    "******04.*<<<<_<<<<*******|******12.*>>>>_>>>>*******",
    "******05.*<<<<_<<<<*******|******13.*>>>>_>>>>*******",
    "******06.*<<<<_<<<<*******|******14.*>>>>_>>>>*******",
    "******07.*<<<<_<<<<*******|******15.*>>>>_>>>>*******",
    "*****************************************************",
    "*****************************************************"); 

begin

    process(CLK)
    variable Inter_Charac : Inter_Anim;
    variable Color        : VGA_Color;
    begin
        if (rising_edge(CLK)) then
			Inter_Charac := Interface_Animation(Interfaz, X_Position, Y_Position, Hazard_Cases, Hazard_Registers, IF_In, ID_In, EX_In, MEM_In, WB_In, MemI_In, MemD_In, Reg_In1, Reg_In2, Count_V, Count_H);
            Color        := Charac_Print(X_Position, Y_Position, Count_V, Count_H, Inter_Charac.Charac, Inter_Charac.Change_Color, Video_Enable);
            Red          <= Color.Red;
            Green        <= Color.Green;
            Blue         <= Color.Blue;
       end if;
    end process;    

end Behavioral;