# UNIPROC 2021: Microprocesador RISC-V con Interfaz Gráfica

<img src="doc/Interfaz_Grafica.gif" height="379">

**An English version of this README, is provided as** [`README.EN.md`](README.EN.md)


## Introducción

El código contenido en este repositorio, ha sido elaborado para el proyecto
descrito en el trabajo monográfico: ["Implementación de un Microprocesador 
Tipo RISC, Utilizando el Lenguaje de Descripción de Hardware VHDL y el 
Software Xilinx Vivado"].

["Implementación de un Microprocesador Tipo RISC, Utilizando 
el Lenguaje de Descripción de Hardware VHDL y el Software Xilinx Vivado"]: https://drive.google.com/file/d/1cRLjrTpBWcSlqRgZv3Zw3FxXzD7avTfz/view?usp=sharing
## Descripción del Proyecto
<img src="doc/Microarquitectura_del_Microprocesador_-_Con_Nombre.png" height="379">

El microprocesador `UNIPROC 2021`, es un microprocesador [RISC-V] segmentado de 5 etapas de 32 bits, que implementa un subconjunto de instrucciones del ISA base "RV32E". Junto al microprocesador, tambien se provee una interfaz gráfica que permite visualizar muchos aspectos del comportamiento del microprocesador, ademas que permite la interacción con el. El microprocesador y la interfaz gráfica se han descritos utilizando el lenguaje de descripción de hardware VHDL.

El código en este repositorio esta organizado en 3 carpetas, la carpeta [`Código VHDL Proyecto`] que contiene todo el código del microprocesador y la interfaz gráfica, la carpeta [`Código VHDL Simulaciones`], que contiene subcarpetas con el código VHDL de cada módulo de la microarquitectura del microprocesador, asi como, el código VHDL con el que se simula cada módulo, y por último la carpeta [`Archivo XDC`], que contiene el archivo `XDC` con el cual se mapea los puertos de entrada y salida del proyecto, con los puertos físicos de entrada y salida de la FPGA.

Para el desarrollo de este proyecto se ha utilizado el software Xilinx [Vivado]®v19.1, y en su implementación se ha utilizado la tarjeta FPGA de desarrollo [Zybo].

[RISC-V]: https://riscv.org/about/
[Zybo]: https://reference.digilentinc.com/reference/programmable-logic/zybo/start
[`Código VHDL Proyecto`]: https://gitlab.com/ezerbd20/UNIPROC_2021/-/tree/main/Código%20VHDL%20Proyecto
[`Código VHDL Simulaciones`]: https://gitlab.com/ezerbd20/UNIPROC_2021/-/tree/main/Código%20VHDL%20Simulaciones
[`Archivo XDC`]: https://gitlab.com/ezerbd20/UNIPROC_2021/-/tree/main/Archivo%20XDC
[Vivado]: https://www.xilinx.com/products/design-tools/vivado.html

## ¡Importante!

La interfaz gráfica desarrollada para este proyecto, trabaja a una resolución de 640x480 píxeles, muchos monitores reescalan esta resolución automáticamente a la resolución con la que ellos trabajan, por lo cual, en muchos casos no habrá ningún inconveniente con la resolución de la interfaz gŕafica, sin embargo, en algunos monitores, el reescaldo automático no trabaja bien con la resolución de la interfaz gráfica, en dichos casos, la interfaz gráfica no funcionara, por lo que se recomienda utilizar otro monitor que no presente dicho problema.