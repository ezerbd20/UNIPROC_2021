# UNIPROC 2021: RISC-V Microprocessor with Graphic Interface

<img src="doc/Interfaz_Grafica.gif" height="379">

## Introduction

The code provided in this repository, has been developed for the project
described in the Bachelor Thesis: ["Implementación de un Microprocesador 
Tipo RISC, Utilizando el Lenguaje de Descripción de Hardware VHDL y el 
Software Xilinx Vivado"].


["Implementación de un Microprocesador Tipo RISC, Utilizando 
el Lenguaje de Descripción de Hardware VHDL y el Software Xilinx Vivado"]: https://drive.google.com/file/d/1cRLjrTpBWcSlqRgZv3Zw3FxXzD7avTfz/view?usp=sharing


## Project Description
<img src="doc/Microarquitectura_del_Microprocesador_-_With_Name.png" height="379">

The `UNIPROC 2021` microprocessor, is a pipelined 32-bit [RISC-V] microprocessor, that implements a subset of instructions from the base ISA "RV32E". Along with the microprocessor, a graphical interface is also provided, that allows the visualization of many aspects of the behavior of the microprocessor, in addition to allowing interaction with it. The microprocessor and the graphic interface, have been described using the VHDL hardware description languaje.

The code in this repository is organized in 3 folders, the [`Código VHDL Proyecto`] folder, which contains all the microprocessor and graphical interface code, the [`Código VHDL Simulaciones`] folder, which contains subfolders with the code of each module of the microprocessor microarchitecture, as well as the VHDL code to simulate each module, and finally the [`Archivo XDC`] folder, which contains the `XDC` file to map the inputs and outputs ports of the project, with the physical inputs and outputs ports of the FPGA. 

The software Xilinx [Vivado]®v19.1, has been used in the development of this project, and the [Zybo] development board, has been used on its implementation.

[RISC-V]: https://riscv.org/about/
[Zybo]: https://reference.digilentinc.com/reference/programmable-logic/zybo/start
[`Código VHDL Proyecto`]: https://gitlab.com/ezerbd20/UNIPROC_2021/-/tree/main/Código%20VHDL%20Proyecto
[`Código VHDL Simulaciones`]: https://gitlab.com/ezerbd20/UNIPROC_2021/-/tree/main/Código%20VHDL%20Simulaciones
[`Archivo XDC`]: https://gitlab.com/ezerbd20/UNIPROC_2021/-/tree/main/Archivo%20XDC
[Vivado]: https://www.xilinx.com/products/design-tools/vivado.html

## Important!

The graphical interface developed for this project works at a resolution of 640x480 pixels, many monitors automatically rescale this resolution to the resolution they work, so in many cases there will be no problems with the resolution of the graphical interface, however, in some monitors, the automatic rescaling does not work well with the resolution fo the graphical interface, in such cases, the graphic interface will no work, so it's recommended to use another monitor that doesn't present this problem.